classdef analysis < hgsetget
    properties
        Label                                    % label for analysis
        paradigmType                             % Type of paradigm  
        analysisCallback = 'nseoae_callback';    % callback for analysis
        buffersOn                                % using buffers or not?
        nBuffer                                  % total number of buffers
        conditionsOn                             % are we using conditions?
        nCondition                               % total number of conditions
        Buffer = 1                               % current buffer
        Condition = 1                            % current condition
        bufferLabels                             % labels for each buffer
        conditionLabels                          % labels for each condition
        avg                                      % averaged file from controller that analysis is to be performed on
        nf                                       % noise floor file
        date_created                             % save the date/time when analysis was performed
        artifactrejectionLabel                   % AR method
        
        %patient info                           --------------------------------------------
        patientID                               % Not currently used
        ear                                     % patient ear {L R}
        DOB                                     % Date of birth
        sex                                     % {M F} 
        notes                                   % Notes
        
        % analysis properties
        analysisType                             % type of analysis such as finestructure, sepearated components, etc.
        nfilt = 22050                            % size of the LSF filter
        nstep = 551                              % size of the Nstep filter
        nskip = 0;                               % Nskip
        fc_l                                     % Low-frequency filter cutoff
        fc_h                                     % High-frequency filter cutoff
        calcDifference = false;                  % used for MOCR, calculate difference between NO-CAS and CAS conditions
        sensitivity                              % sensitivity of the system (mv/Pa)
        gain                                     % gain of the system (dB)
        fs                                       % sampling frequency (Hz)
        fstart                                   % starting frequencies for the sweep (Hz)
        fend                                     % ending frequencies for the sweep (Hz)
        L                                        % stimulus level (dB)
        T                                        % int, total time of the sweep (seconds)
        phaseAccumFlag = 1                       % bool, phase accumulation flag 1 = use phase accumulation, 0 = do not use phase accumulation for phase calculations - this is helpful to determine if your looking at an OAE or just noise where noise has random phase with no phase accumulation
        navg                                     % int, number of samples for temporal averaging (e.g. for disctete OAEs)
        fftavg                                   % bool, perform fft averaging
         

    end
    properties (Hidden = true)
        % results, hidden so that they don't get copied around
        f                                        % frequency vectors for each component
        pa                                       % pascal values for each component
        pa_std                                   % standard deviation (pa)
        db                                       % dB values for each component
        db_std                                   % standard deviation (db)
        phi                                      % wrapped phase values for each component
        phiUnwrap                                % unwrapped phase values for each component
        residual                                 % filtered residuals for each coponent
        
        labels                                   % labels for each line on the plot
        labelsOn                                 % logical vector for which components will be on after analysis
        nComponents = 4                          % number of components that are to be/were analyzed
        
        Ylimits = [];                            % Y axis limits
        Xlimits = [];                            % X axis limits
        Xscale = 'log';                          % 'log' or 'linear' x scale
        Yscale = 'linear';                       % same as above
        phaseWrap = 'unwrapped';                 % {'unwrapped' or 'wrapped' }
    end
    
    
    methods
        % constructor
        function obj = analysis()
            uid = oae_getuid();
            file = [pwd filesep 'functions' filesep 'defaults' filesep uid '_analysisObj.mat'];
            obj = obj.loadSettings(file);
        end
        % load settings
        function obj = loadSettings(obj, file)
            if exist(file, 'file'),
                load(file);
                fields = fieldnames(props);
                for i=1:length(fields),
                    set(obj, fields{i}, eval(['props.' fields{i}]));
                end
            end
        end
        function obj = setProp(obj, propertyname, value)
            if isempty(strcmp(propertyname, properties(obj))),
                disp('property does not exist.');
            else
                eval(['obj.' propertyname '=' 'value' ';']);
            end
        end
        function S = saveoptions(obj)
            % Save property values in struct
            % Return struct for save function to write to MAT-file
            S.Xlimits = obj.Xlimits;
            S.Ylimits = obj.Ylimits;
            S.Xscale = obj.Xscale;
            S.Yscale = obj.Yscale;
        end
        function S = export(obj)
            S = struct();
            
            S.date_saved = datestr(now);
            
            % patient info
            S.patient_info.patientID = obj.patientID;
            S.patient_info.ear       = obj.ear;                              
            S.patient_info.DOB       = obj.DOB;                              
            S.patient_info.sex       = obj.sex;                              
            S.patient_info.notes     = obj.notes;                              
            
            % analysis details
            S.analysis_info.analysisType = obj.analysisType;
            S.analysis_info.analysis_callback = obj.analysisCallback;
            S.analysis_info.artifact_rejection = obj.artifactrejectionLabel;
            S.analysis_info.nfilt = obj.nfilt;
            S.analysis_info.nstep = obj.nstep;
            S.analysis_info.Label = obj.Label;
            S.analysis_info.nCondition = obj.nCondition;
            S.analysis_info.conditionLabels = obj.conditionLabels;
            S.analysis_info.sensitivity = obj.sensitivity;
            S.analysis_info.gain = obj.gain;
            
            % results
            S.labels = obj.labels;
            S.f = obj.f;
            S.db = obj.db;
            S.pa = obj.pa;
            S.phi = obj.phi;
            S.phiUnwrap = obj.phiUnwrap;
            S.noise_floor_pa = obj.residual;
            S.noise_floor_db = 20*log10(obj.residual);
            S.fs = obj.fs;
            S.fstart = obj.fstart;
            S.L = obj.L;
            S.fend = obj.fend;
            S.T = obj.T;
            S.avg = obj.avg;
            S.nf = obj.nf;
            
            
            
        end
        
        % Analysis methods 
        function obj = analyze(obj, oae)
            % perform analysis for each buffer & condition
            h = waitbar(0, 'Analyzing...'); counter = 1; total = (obj.nBuffer+obj.nCondition);
            for i=1:obj.nBuffer,
                % At times, number of analysis buffers may be dynamically reduced (e.g. double-evoked TEOAEs reduces 3 buffers to 1 analysis buffer), 
                % therefore, check if we still need to perform analysis
                % (e.g. were analysis object buffers updated)
                if i > obj.nBuffer,
                    continue;
                end
                obj.Buffer = i; oae.Buffer = i;
                for j=1:obj.nCondition,
                    if j > obj.nCondition,
                        continue;
                    end
                    obj.Condition = j; oae.Condition = j;
                    obj.date_created = datestr(now);
                    obj = feval(obj.analysisCallback, obj, oae);
                    counter = counter+1;
                    waitbar(counter/total, h);
                end
            end
            obj.buffersOn = logical([1:obj.nBuffer]);
            obj.conditionsOn = logical([1:obj.nCondition]);
            close(h);
            
            % make sure analyis has a "Label". Labels are used to validate
            % analysis objects
            if isempty(obj.Label),
                obj.Label = sprintf('%s', obj.analysisCallback);
            end
        end
        
        function avg = getCurrentAverage(obj)
           avg = obj.avg(:, obj.Buffer, obj.Condition); 
        end
        
        function nf = getCurrentNoiseFloor(obj)
            nf = [];
            if  ~isempty(obj.nf),
                nf = obj.nf(:, obj.Buffer, obj.Condition);
            end
        end
        
        function obj = saveAnalysis(obj, f, pa, pa_std, db, db_std, phi, residual)
            % TODO: change the variable name "residual" to "noise_floor" to
            % be more generic
            if size(f,1)<size(f,2),
                %f = f';
            end
            obj.f(:,:,obj.Buffer, obj.Condition) = f;
            obj.pa(:,:,obj.Buffer, obj.Condition) = pa;
            obj.pa_std(:,:,obj.Buffer, obj.Condition) = pa_std;
            obj.db(:,:,obj.Buffer, obj.Condition) = db;
            obj.db_std(:,:,obj.Buffer, obj.Condition) = db_std;
            obj.phi(:,:,obj.Buffer, obj.Condition) = phi; %NOTE: phi should be in degrees
            obj.phiUnwrap(:,:,obj.Buffer, obj.Condition) = unwrap(phi, 150);
            obj.residual(:,:,obj.Buffer, obj.Condition) = residual;
            
            
            
            % calculate differences at end of the analysis
            % --------------------------------------------
            if ((obj.calcDifference) && (obj.Condition == obj.nCondition) && (obj.Buffer == obj.nBuffer) ),
                % try to calculate difference after last condition & buffer
                % Add a new Condition to hold these values and give it a
                % label (e.g. "Label1 - Label2")
                obj.nCondition = obj.nCondition + 1;
                obj.Condition = obj.Condition + 1;
                obj.conditionLabels{obj.Condition} = sprintf('%s - %s', obj.conditionLabels{obj.Condition-2}, obj.conditionLabels{obj.Condition-1});
                obj.conditionsOn = [obj.conditionsOn 0];
                for b=1:obj.nBuffer,
                    obj.f(:,:,b, obj.Condition) = obj.f(:,:,b, obj.Condition-2);
                    obj.pa(:,:,b, obj.Condition) = obj.pa(:,:,b, obj.Condition-2) - obj.pa(:,:,b, obj.Condition-1);
                    obj.db(:,:,b, obj.Condition) = obj.db(:,:,b, obj.Condition-2) - obj.db(:,:,b, obj.Condition-1);
                    obj.phi(:,:,b, obj.Condition) = obj.phi(:,:,b, obj.Condition-2) - obj.phi(:,:,b, obj.Condition-1);
                    obj.phiUnwrap(:,:,b, obj.Condition) = oae_unwrapper(obj.phi(:,:,b,obj.Condition)*(180/pi), 150); 
                    obj.residual(:,:,b, obj.Condition) = (obj.residual(:,:,b, obj.Condition-2) + obj.residual(:,:,b, obj.Condition-1))./2;
                end
                
            end
            
        end
    end
end