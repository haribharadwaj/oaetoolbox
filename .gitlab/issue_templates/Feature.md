`FEATURE` Proposal
<!---
* NOTE: This form is for submitting feature proposals.
  (If you are reporting a Bug, please use the Bug template instead)
* Please provide a concise description of the issue in the Title above.
--->
--------------------------------------------------------------------------------
### Description

(Include problem, use cases, benefits, and/or goals)

### Proposal

### Links / references

/label ~"feature proposal"
