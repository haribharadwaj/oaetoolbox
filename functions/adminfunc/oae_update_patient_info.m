function OAE = oae_update_patient_info(OAE, SEQUENCE)
% bug fix, update patient got stuck in the while loop as N was never
% updated. Fixed - Joshua J. Hajicek - 3/1/2017 GC CUNY.

% update patient info
ID    = oae_getvalue('pt_info', 'pt_ID', 'string');
ear   = oae_getvalue('pt_info', 'pt_ear', 'string');
DOB   = oae_getvalue('pt_info', 'pt_DOB', 'string');
sex   = oae_getvalue('pt_info', 'pt_sex', 'string');
notes = oae_getvalue('pt_info', 'pt_notes', 'string');

N = 1;
while (N <= SEQUENCE)
    OAE(N).patientID = ID;
    OAE(N).ear       = ear;
    OAE(N).DOB       = DOB;
    OAE(N).sex       = sex;
    OAE(N).notes     = strtrim(notes);
    N = N+1;
end
