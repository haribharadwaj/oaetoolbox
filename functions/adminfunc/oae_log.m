function oae_log(msg, varargin)

if ~isempty(msg),
    if nargin > 1,
        fprintf(1, 'OTB: %s %s\n', msg, varargin{:});
    else
        fprintf(1, 'OTB: %s \n', msg);
    end
end


end