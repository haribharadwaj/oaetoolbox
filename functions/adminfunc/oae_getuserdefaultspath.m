function pth = oae_getuserdefaultspath()


sep = filesep;
if isdeployed,
    p = explode(ctfroot, sep)
    pth = [implode(p(1:end-1), sep) sep 'SwOAE' sep]
else
    pth = [pwd sep 'functions' sep 'defaults' sep];
end
if ~exist(pth, 'dir'), mkdir(pth); end

end

