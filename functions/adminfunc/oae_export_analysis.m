function OAE = oae_export_analysis(OAE, SEQUENCE)

% TODO: check if analyzed data
anal = OAE(SEQUENCE).datasets(OAE(SEQUENCE).currentIndex(1)).analysisObj(OAE(SEQUENCE).currentIndex(2)).export();
% try to get analysis label, if it exists,
if ~isempty(anal.analysis_info.Label),
    possible_filename = ['otb_analysis_' regexprep(lower(anal.analysis_info.Label), {' ' '[' ']'},{'_' '' ''}) '_' datestr(now, 'dd-mm-yyyy-HHMMSS')];
else
    possible_filename = ['otb_analysis_' datestr(now, 'dd-mm-yyyy-HHMMSS')];
end
[filename, pathname] = uiputfile('*.mat', 'Pick a Mat-file', possible_filename);
if isequal(filename,0) || isequal(pathname,0)
    
else
    save(fullfile(pathname, filename), '-struct', 'anal');
end


end