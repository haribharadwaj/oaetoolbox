function oae_savedefaults(hObject, eventdata)
% set defaults for current block
% ------------------------------
%
% 7/31/12   fixed userdefaults paths
% 07/15/16  updated to new defaults system - SH

% % set default module
% % ------------------
% block = get(get(hObject, 'parent'), 'tag');
% child_module = findobj('tag', [block '.modules']);
% string = get(child_module, 'string');
% current_module = string{get(child_module, 'value')};
% 
% sep = filesep;
% 
% pth = oae_getuserdefaultspath;
% 
% defaults_file = [pth oae_getuid '_defaults.mat'];
% 
% if exist(defaults_file, 'file')
%     load(defaults_file);
%     blockset = find(strcmp({defaults.block}, block));
%     if ~isempty(blockset),
%         defaults(blockset).default = current_module;
%     else
%         nextEntry = length(defaults)+1;
%         defaults(nextEntry).block = block;
%         defaults(nextEntry).default = current_module;
%     end
% else
%     defaults.block = block;
%     defaults.default = current_module;
% end
% 
% save(defaults_file, 'defaults');
% evalin('base', 'OAE(SEQUENCE).saveController();');
% 
% % set module defaults for all visible modules
% % -------------------------------------------
% %modules = findobj('-regexp', 'tag', '[a-z].modules');
% 
% % TODO: Check for protocols module, to override
% 
% % set module defaults
% % -------------------------------------------
% block_module_value = get(findobj('tag', [block '.modules']), 'value');
% names = get(findobj('tag', [block '.modules']), 'string');
% block_module_name = names{block_module_value};
% 
% module = findobj('tag', [ block '.parameters']);
% parameters = flipud(get(module, 'children'));    % REMARK: Need to flip the paramters u/d to resort them in desceinding order.
% defaults = [];
% defaults.module = block_module_name; counter = 1;
% for i=1:length(parameters),
%     tag = get(parameters(i), 'tag');
%     % check if figure object.
%     % if so, skip
%     if (0),
%         continue;
%     end
%     if ~isempty(tag) && isempty(regexp(tag, '.protocols')),
%         val = oae_getvalue(module, tag, 'string'); %%%%
%         userdata = get(findobj('tag', tag), 'userdata');
%         defaults.parameters(counter).tag = tag;
%         defaults.parameters(counter).value = val;
%         defaults.parameters(counter).userdata = userdata;
%         counter = counter +1;
%     end
% end
% module_defaults_file = [pth oae_getuid '.' block '.parameters_defaults.mat'];
% save(module_defaults_file, 'defaults');


% set default module
% ------------------
block = get(get(hObject, 'parent'), 'tag');
child_module = findobj('tag', [block '.modules']);
string = get(child_module, 'string');
current_module = string{get(child_module, 'value')};

% save last used settings
module = findobj('tag', [ block '.parameters']);
params = flipud(get(module, 'children'));    % REMARK: Need to flip the paramters u/d to resort them in desceinding order.
parameters = [];
counter = 1;
for i=1:length(params),
    tag = get(params(i), 'tag');
    style = [];
    try, 
        style = get(params(i), 'style');
    catch
    end
    % check if figure object.
    % if so, skip
    if (0),
        continue;
    end
    if ~isempty(tag) && isempty(regexp(tag, 'protocols')) && ~strcmp(style, 'pushbutton'),
%         if strcmp(style, 'checkbox'),
%             val = oae_getvalue(module, tag, 'value');
%         else
%             val = oae_getvalue(module, tag, 'string'); %%%%
%         end
        
        userdata = get(findobj('tag', tag), 'userdata');
        parameters(counter).tag = tag;
        parameters(counter).value = get(findobj('tag', tag), 'value'); %oae_getvalue(module, tag, 'value');
        parameters(counter).string = get(findobj('tag', tag), 'string'); %oae_getvalue(module, tag, 'string');
        parameters(counter).userdata = userdata;
        counter = counter +1;
    end
end

pth = oae_getuserdefaultspath;

defaults_file = [pth oae_getuid '.defaults.mat'];

if exist(defaults_file, 'file')
    load(defaults_file); % loads in 'modules'
    blockset = find(strcmp({modules.block}, block));
    module_idx = find(strcmp({modules.label}, current_module));
    if ~isempty(blockset),
        [modules(blockset).is_default] = deal(0);   % set all modules in this block to 0
        
% SH (04/17/17): these lines will blank out any paramters used in other modules (e.g. between calibration modules). I'm not sure why this was done in the first place, but for now will remove it        
%         for i=1:length(blockset),
%             modules(blockset(i)).parameters   = [];   % set all modules in this block to 0
%         end

        modules(module_idx).is_default = 1;         % set current as default
        modules(module_idx).parameters = parameters;
    end
end
save(defaults_file, 'modules');
evalin('base', 'OAE(SEQUENCE).saveController();');
assignin('base', 'modules', modules);

% if daq block, let's update the main display
if strcmp(block, 'daq'),
    evalin('base', 'OAE = oae_update_io_config(OAE, SEQUENCE);');
end

end