function ret = oae_validate(varargin)

ret = false;
if all(varargin{:}) && (sum(isnan(varargin{:})) == 0)
    ret = true;
end

end