function OAE = oae_savesequence(OAE, SEQUENCE)


if ( (SEQUENCE > 1) || (~isempty(OAE(SEQUENCE).paradigmLabel))),
    [filename, pathname] = uiputfile('*.seq','Save file');
    if isequal(filename,0) || isequal(pathname,0)
       oae_log('User canceled "save sequence" operation...');
    else
       %disp(['User selected',fullfile(pathname,filename)])
       % need to remove 
       if SEQUENCE > 1,
            OAE = OAE(1:SEQUENCE-1);
       end
       save(fullfile(pathname,filename), 'OAE', '-mat');
    end
else
    oae_error('No saved sequences.');
end
