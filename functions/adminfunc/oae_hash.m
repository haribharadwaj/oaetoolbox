function hash = oae_hash(str)
% create a simple 'sdbm' hash value from a string
%
% 07/17/16 created function. (SH) Modified from D.Kroon University of Twente
%
hash = zeros(size(str,1),1);
for i=1:size(str,2),
    hash = mod(hash * 65599 + str(:,i), 2^32-1);
end