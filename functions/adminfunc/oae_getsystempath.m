function pth = oae_getsystempath()

sep = filesep; 
defaults = [];
if isdeployed
    pth = [ctfroot sep 'functions' sep 'defaults' sep];
else
    pth = [pwd sep 'functions' sep 'defaults' sep];
end
if ~exist(pth, 'dir'), mkdir(pth);
    
end
