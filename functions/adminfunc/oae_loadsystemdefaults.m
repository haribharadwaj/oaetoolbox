function defaults = oae_loadsystemdefaults()
%
% * Load system defaults
% ----------------------------------------
% If first run, this function will copy default files into the
% appropriate folder.
%
% 7/31/12 - updated paths
% 2/24/15 - added probe mic calibration defaults - jjh

sep = filesep; defaults = [];
userpth = oae_getuserdefaultspath;
systempth = oae_getsystempath;
icons_path = [systempth 'icons'];


defaults_file = [userpth oae_getuid '_defaults.mat'];
defaults_obj = [systempth oae_getuid '_obj.mat'];
defaults_cal = [userpth oae_getuid '.calibration.parameters_defaults.mat'];
defaults_ar = [userpth oae_getuid '.artifactrejection.parameters_defaults.mat'];
defaults_pcal = [userpth oae_getuid '.probemicCalibration.parameters_defaults.mat'];


if exist(defaults_file, 'file')
    load(defaults_file);
else
    % copy default object + defaults
    if isdeployed,
        % copy files in function\adminfunc
        pth = [ctfroot sep 'functions' sep 'adminfunc' sep];   %C:\HearID\5_1_SwOAE\libswoae_mcr\functions\adminfunc\
    else
        pth = [pwd sep 'functions' sep 'adminfunc' sep];
    end
    
    
    system_defaults = [pth 'system_defaults.mat'];
    copyfile(system_defaults, defaults_file, 'f');
    
    system_obj = [pth 'system_obj.mat'];
    copyfile(system_obj, defaults_obj, 'f');
    
    system_cal = [pth 'system.calibration.parameters_defaults.mat'];
    copyfile(system_cal, defaults_cal, 'f');
    
    system_ar = [pth 'system.artifactrejection.parameters_defaults.mat'];
    copyfile(system_ar, defaults_ar, 'f');
    
    system_pcal = [pth 'system.probemicCalibration.parameters_defaults.mat'];
    copyfile(system_pcal, defaults_pcal,'f');
    
    system_icon = [pth 'icons'];   
    copyfile(system_icon, icons_path, 'f');
    load(defaults_file);
end

end