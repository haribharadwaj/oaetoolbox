function OAE = oae_exportcurrent_tavg(OAE, SEQUENCE)
%% OAE_exportcurrent_tavg, Exports the time domain average
% This was primarily written to export .snd or .wav files for further
% analysis such as in Unix or NSEOAE
% Note: This may not yet work with TEOAE or Discrete DPOAEs (untested)
% Also, note that in order to get the right scaling back, you must divide
% multiply by the sensitivity listed in the comments which should be attached to each file.
%
% Joshua J. Hajicek, November 18, 2017, CUNY Graduate Center

% TODO: check if average exists
try
    anal = OAE(SEQUENCE).analysisObj.getCurrentAverage();
    if isempty(anal),
        oae_error('No dataset average available');
        return;
    end
catch
    oae_error('No dataset average available');
    return;
end

opts = []; fnbase = [];
try,
    if size(anal.L,1) == 2  % DPOAEs which use L2 and f2 as file references
        fnbase = [anal.patientID anal.ear(1)  '.L' num2str(anal.L(2)) '.f' num2str(anal.fstart(2)) '-' num2str(anal.fend(2)) '.' num2str(anal.T) '.avg'];
        opts = ['F1s=' num2str(anal.fstart(1)) ' F1e=' num2str(anal.fend(1)) ' F2s=' num2str(anal.fstart(2)) ' F2e=' num2str(anal.fend(2)) ' L1=' num2str(anal.L(1)) ' L2=' num2str(anal.L(2)) ' T=' num2str(anal.T) ' gain=1' ];
    else   % without suppressor SFOAE - may need to expand for suppressors
        fnbase = [anal.patientID anal.ear(1)  '.L' num2str(anal.L(1))  '.L0.f' num2str(anal.fstart(1)) '-' num2str(anal.fend(1)) '.' num2str(anal.T) '.avg'];
        opts = ['F1s=' num2str(anal.fstart(1)) ' F1e=' num2str(anal.fend(1)) ' L1=' num2str(anal.L(1)) ' T=' num2str(anal.T) ' gain=1' ];
    end
catch,
end
% Suggest a format for the filename


[filename, pathname, filteridx] = uiputfile({'*.wav'; '*.snd'}, 'Choose a filename and file format', fnbase);
tmp = explode(filename, '.');
tmp(end+1) = tmp(end);
tmp{end-1} = 'floor';
fn2 = implode(tmp,'.');
% normalize file by peak amp
mx = max(abs(anal.avg));
ynorm = anal.avg/mx;
ynf = anal.nf/mx;
opts = [opts ' sens=' num2str(mx)];
if isequal(filename,0) || isequal(pathname,0)
    
else
    switch filteridx
        case 1    % .wav
            audiowrite(fullfile(pathname, [filename]), ynorm, anal.fs, 'BitsPerSample',  32, 'Comment', opts);
            audiowrite(fullfile(pathname, [fn2]), ynf, anal.fs, 'BitsPerSample',  32, 'Comment', opts);
        case 2    % .snd
            oae_sndwrite(ynorm, anal.fs, 32, 'floatingpoint', opts, fullfile(pathname, filename));
            oae_sndwrite(ynf, anal.fs, 32, 'floatingpoint', opts, fullfile(pathname,  fn2));
        otherwise
            
    end
end
    
