function uid = oae_getuid()

uid = '';
if isunix,
    ni = java.net.NetworkInterface.getNetworkInterfaces;
    while ni.hasMoreElements
        try,
            % only works for > 7.7.0
            addr = ni.nextElement.getHardwareAddress;
        catch 
            % use ethernet mac address instead
            [tmp,a]=system('ifconfig');
            c=strfind(a,'en0');if ~isempty(c),a=a(c:end);end
            c=strfind(a,'en1');if ~isempty(c),a=a(1:c-1);end
            % find the mac address
            b=strfind(a,'ether');
            addr=regexprep(a(1,b(1)+6:b(1)+22), ':', '');
        end
        if ~isempty(addr)
            addrStr = dec2hex(int16(addr)+128);
            uid = [uid '.' reshape(addrStr,1,2*length(addr))];
        end
    end
else
    uid = [uid '.' get(com.sun.security.auth.module.NTSystem,'DomainSID')];
end

end