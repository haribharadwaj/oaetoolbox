function [OAE SEQUENCE] = oae_start(OAE, SEQUENCE)
%
% 8/4/16 Fixed bug where additional unused sequence (e.g. "in the chamber"
% sequence) persisted through until analysis. Simon H.

% main start routine
global iIsAnalysisGuiOpen

%remove unused sequences...
% and assign back into base.
valid_idx = find([OAE.validSequence]);
OAE = OAE(valid_idx);
SEQUENCE = length(valid_idx);
% if (SEQUENCE > 1), OAE(SEQUENCE) = []; SEQUENCE = SEQUENCE-1; end
assignin('base', 'OAE', OAE);
assignin('base', 'SEQUENCE', SEQUENCE); 
% TODO: see if workaround for assigning back to base workspace, this is
% kind of hacky. But maybe it's ok? (SH)


% validate OAE configuration (e.g. devices, v2pa, etc...)
% This only needs to be done once
errors = OAE(1).validateController();
if errors,
    return;
end

% validate sequences before starting
N = 1; ok = 1;
while (N <= SEQUENCE)
    obj = OAE(N); 
    errors = obj.validateSequence(N);
    if errors,
        ok = 0;
    end
    N = N+1;
end

if ok,

    N = 1;
    while (N <= SEQUENCE)
        obj = OAE(N);
        obj.rep = 1;
        OAE(N) = obj.startRecording();
        N = N+1;
    end
    
% 9-27-16, SH: This has been moved to the analysis controller   
%     %% run default artifact rejection module
%     N = 1;
%     h = waitbar(0,'Please wait...');
%     while (N <= SEQUENCE)
%         obj = OAE(N);
%         OAE(N) = obj.runArtifactRejection();
%         N = N+1;
%         waitbar(N/SEQUENCE,h);
%     end
%     close(h);

% TODO: remove this. The object is saved down below
%     %autosave
%     if OAE(1).autosave,
%         save(OAE(1).filename, 'OAE');
%     end
    
    
    % Perform final probe check, only if other calibration method not set
    if isempty(OAE(SEQUENCE).calibrationCallback) || strcmp(OAE(SEQUENCE).calibrationCallback, ''),
        % if no calibration routine. Ask if we should perform a probe check in lieu
        response = oae_questdlg('Perform final probe check?', {'Yes', 'No'});
        if response == 1,
            OAE(SEQUENCE) = OAE(SEQUENCE).probeCheck();
        end
    else
        % 03/18/17 - SH, option to run final calibration should be available to any calibration callback.    
        response = oae_questdlg('Perform final calibration?', {'Yes', 'No'});
        if response == 1,
            OAE(SEQUENCE) = OAE(SEQUENCE).runCalibration();
        end
    end
            
    % autosave the session to the data folder, but prompt to "Continue to
    % analysis", "Discard", or "Quit"
    [OAE, filename] = oae_saveanalysis(OAE, SEQUENCE, 1);
    
    options.Interpreter = 'none';
    options.Default = 'Continue to Analysis';
    set(0,'DefaultUicontrolUnits','Pixels'); %Bug fix: questdlg does not show properly when default units are 'normalized'. So override temporarily.
    btn = questdlg({sprintf('Your session has been saved to %s.', filename); 'What would you like to do?'}, 'Next...', 'Continue to Analysis', 'Discard Session', 'Close & Exit', options);
    set(0,'DefaultUicontrolUnits','Normalized');
    switch btn,
        case 'Continue to Analysis',
            close(findobj('tag', 'OAEToolbox'));
            evalin('base', 'oae_analysis_setup;');
            SEQUENCE = 1;
        case 'Discard Session',
            delete([OAE(end).datapath filesep filename]);
            close(findobj('tag', 'OAEToolbox'));
        otherwise,
            %do nothing but close
            close(findobj('tag', 'OAEToolbox'));
    end
    
     %if iIsAnalysisGuiOpen = 1 then don't notify VB of completion
    %   iIsAnalysisGuiOpen = 0 notify VB of completion    
    iIsAnalysisGuiOpen = 0;

end

end