function [y] = oae_tone(varargin)
%  OAE_TONE create tone
%
%   y = oae_tone(F, T, fs);

%
%


% if nargin ~= 3 | mod(nargin,2) == 0,   % not sure what the logic is here
%     error('myApp:argChk', 'Wrong number of input arguments.');
% end
if length(varargin{1}) ~= length(varargin{2}),
    error('myApp:argChk', 'Vectors must be the same lengths.');
end

for i=1:nargin,
    if strcmp(varargin{i},'fs'),
        fs = varargin{i+1};
    end
end

F = varargin{1};
T = varargin{2};
fs = varargin{3};
% time vector
t = 0:1/fs:T;

%allocate stimulus
y = zeros(length(F), length(t));

for i=1:length(F),
    f = F(i);
    w = 2*pi*f;
    phi1 = w .* t;
    y(i,:) = cos(phi1);
end


