function [f coef residual rms_error est F] = oae_lsf_feval(PhiF, Phi, delay, nfilt, t, y, Wvec)
%
% Simple lsf

% transform data that needs it
if size(y,1) > size(y,2),
    y = y';
end
if size(delay,1) > size(delay,2),
    delay = delay';
end

% Build the model including each component
nPhi = length(Phi);
F = zeros(nPhi*2, nfilt);
f = zeros(nPhi,1);
for i=1:nPhi,
    pf = feval(Phi{i}, (t))+delay(i,:); % evaluate phase function
    f(i) = feval(PhiF{i}, median(t) );
    F((2*i)-1, :) = cos(pf);
    F((2*i), :) = sin(pf);
end
FT = F'; % Create transpose once for speed

% LSF
A = zeros(nPhi*2, nPhi*2);
tmp = zeros(1,nPhi*2);
for i = 1:nPhi*2
    tmp = Wvec .* F(i, :);
    for j = 1:nPhi*2
        A(i,j) = tmp * FT(:,j);
    end
end
b =  F * (Wvec .* y)';

% solve the matrix equation A*x = b
coef = A \ b;

est = F' * coef;
residual = (y - est') .* Wvec ;
rms_error = sqrt(mean(residual.^2));


end