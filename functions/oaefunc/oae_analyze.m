function OAE = oae_analyze(OAE, SEQUENCE)
% Analyze button callback
% run an analysis
OAE(SEQUENCE).currentIndex = [OAE(SEQUENCE).current_dataset,OAE(SEQUENCE).datasets(OAE(SEQUENCE).current_dataset).analysisIndex];
OAE(SEQUENCE) = OAE(SEQUENCE).runAnalysis();

%autosave
if OAE(SEQUENCE).autosave,
    save(OAE(SEQUENCE).filename, 'OAE');
end

%oae_refresh(OAE, SEQUENCE);
    
%oae_plot(OAE, SEQUENCE);

% finished, so let's make a new analysis object, in case another analysis is
% performed
% OAE(SEQUENCE) = OAE(SEQUENCE).newAnalysis( OAE(SEQUENCE).analysisObj( OAE(SEQUENCE).analysisIndex) );
OAE(SEQUENCE) = OAE(SEQUENCE).newAnalysis(); % create another analysis object for the next analysis
oae_update_analyses(OAE, SEQUENCE);

% call the cmd to reset variables
module = findobj('tag', 'analysis.parameters');
edit = findobj(module, 'style', 'edit');
cmd = get(edit(1), 'callback');
eval('base', cmd);

end