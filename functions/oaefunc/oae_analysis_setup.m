% oaetoolbox analysis setup
oae_log('running analysis setup...');

%%MAIN GUI
handle = oae_makeanalysisgui();

% add help menu
menu_help_modules = findobj(handle, 'tag', 'help');
for i=1:length(modules),
    folder = modules(i).path;
    funcname = dir([folder filesep 'oaemodule_*.m']);
    if ~isempty(funcname),
        funcname = funcname.name(1:end-2);
    else
        continue;
    end
    % add help menu
    uimenu(menu_help_modules,'Label', strrep(funcname, 'oaemodule_',''), 'Callback',sprintf('doc %s', funcname));
end
    

%% Initialize an analysis object for each sequence
% after each analysis this should be called again.
if exist('OAE', 'var'),
    for i=1:size(OAE,2),
        
        % TODO: this is old method for analysis object. Consider removing.
        
        % Remove any invalid analysis objects (e.g. empty label, Can occur when
        % analysis is set, but not run)
        index = OAE(i).analysisIndex;
        for j=1:index,
            if isempty(OAE(i).analysisObj(j).Label),
                OAE(i).analysisObj(j) = [];
            end
        end
        
        % create a fresh analysis object
        % OAE(i) = OAE(i).newAnalysis();
    end
    
    %load modules for visible elements
    tags = {'analysis', 'artifactrejection'};
    oae_loadmodules(handle, modules, tags);
    
    %Reset sequence counter
    SEQUENCE = 1;
    
    % Load the OAE objects
    oae_loadobject(OAE, SEQUENCE, handle);

    % update the dropdowns
    oae_update_datasets(OAE, SEQUENCE);
    oae_update_analyses(OAE, SEQUENCE);
end


