function [OAE, filename] = oae_saveanalysis(OAE, SEQUENCE, autosave)
%
% Save OAE object to file
%

filename = [];

% remove any empty analyses
for i=1:SEQUENCE,
    %obj = OAE(i);
    pop = [];
    for j=1:length(OAE(i).datasets),
        for k=1:length(OAE(i).datasets(j)),
            if isempty(OAE(i).datasets(j).analysisObj(k).db),
                pop = [pop k];
            end
        end
        OAE(i).datasets(j).analysisObj(pop);
    end
    %OAE(i).currentIndex = [1 1];
    % remove any graphics handles
    OAE(i).fig = [];
    OAE(i).runtimeFig = [];
end

if isdeployed,
    % TODO: need to grab database string and save to database folder
else
    if exist('autosave', 'var') && autosave,
        pathname = sprintf('%s%s', OAE(1).datapath, filesep);
        filename = sprintf('otb_%s.oae', datestr(now, 'DD-mm-yy_HH.MM.SS'));
    else
        [filename, pathname] = uiputfile({'*.oae'},'Save OAE Session...', sprintf('otb_%s.oae', datestr(now, 'DD-mm-yy_HH.MM.SS')));
    end
    
    if isequal(filename,0) || isequal(pathname,0)
        %disp('User selected Cancel')
    else
        h = oae_waitbar('string', 'Saving...');
        modules = evalin('base', 'modules');
        % remove any existing graphics handles
        save('-v7.3', fullfile(pathname,filename), 'OAE', 'modules');
        oae_log('analysis object saved.');
        close(h);
    end
    
end

end


