function y = oae_ramp(y, duration, fs, side)

if nargin < 4,
    side = 'both';
end

transform = 0;
if size(y,1) > size(y,2),
    y = y';
    transform = 1;
end
if isempty(side),
    side = 'both';
end

% prepare ramp for stimulus
n = length(y);                          % number of samples
dr = duration;                          % ramp time
nr = floor(fs * dr);
ramp = sin(linspace(0, pi/2, nr)).^2;
nr = length(ramp);
switch side,
    case {'both'}
        ramp = [ramp, ones(1, n - (nr * 2)), fliplr(ramp)]';
    case 'onset'
        ramp = [ramp, ones(1, n - (nr * 2)), ones(1,nr)]';
    case 'offset'
        ramp = [ones(1,nr), ones(1, n - (nr * 2)), fliplr(ramp)]';
end

ramp = repmat(ramp', size(y,1),1);
y = y.*ramp;

if (transform),
   y = y'; 
end