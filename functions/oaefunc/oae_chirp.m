function y = oae_chirp(T, fs)

% Generate the chirp (based on Shawn's code)
nfft = round(T * fs);
f = [50,20000]; % frequency minimum and maximum (Hz)
stepSize = (f(2)-f(1))/(nfft-1); % frequency step size (Hz)
F = (f(1):stepSize:f(end))';
rampN = round(nfft * T);
phi = 0;
for jj = 1:nfft
    p(jj,1) = cos(phi);
    phi = phi + ((2*pi) / (1/F(jj) * fs));
end
rampOn = hann(rampN*2);
rampOn = rampOn(1:rampN);
rampOff = hann(rampN*2);
rampOff = rampOff(1:rampN);
p(1:rampN) = p(1:rampN) .* rampOn;
p = flipud(p);
p(1:rampN) = p(1:rampN) .* rampOff;
p = flipud(p);
segmt = p * .95;
padLen = 0.001;
padN = round(padLen * fs);
pad = zeros(padN,1);
y = [pad;segmt;pad];


