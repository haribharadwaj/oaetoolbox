function [y] = oae_linsweep(varargin)
%  LINSWEEP create linearsweep
%
%   y = oae_linsweep(F1s, F1e, T, fs);
%       F1s, F1e are starting and ending frequency vectors, respectively.
%       T is total sweep duration
%       
%   e.g.
%       y = oae_linsweep(1000,2000,2, 44100);               % linear sweep
% Simon Henin and Joshua Hajicek - 6-10-2017

if length(varargin{1}) ~= length(varargin{2}),
    error('myApp:argChk', 'Vectors must be the same lengths.');
end


for i=1:nargin
    if strcmp(varargin{i},'fs'),
        fs = varargin{i+1};
    end
end

F1s = varargin{1};
F1e = varargin{2};
T = varargin{3};
fs = varargin{4};
% time vector
t = 0:1/fs:T;

%create linear sweep
y = zeros(length(F1s), length(t));

for i=1:length(F1s)
    f1 = F1s(i);
    f2 = F1e(i);
    w1 = 2*pi*f1;
    wdot = pi*(f2-f1)/T;
    phi = (w1 + wdot .* t) .* t;
    y(i,:) = cos(phi);
end

     
           
