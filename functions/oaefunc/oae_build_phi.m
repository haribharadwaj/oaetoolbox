function [Phi PhiF] = oae_build_phi(f1, f2, T, fs, varargin)
%
% extra paramters 'type'
% Modifications
%   Added model for linear sweep, 6-10-2017 JJH
type = 'logsweep';
delay = [];
for i=1:2:length(varargin),
    switch varargin{i}
        case 'type'
            type = varargin{i+1};
        case 'delay'
            delay = varargin{i+1};
    end
end



% general paramters
dt = 1/fs;
t = 0:dt:T;

switch type,
    case {'logsweep'},
        gamma = log(f2/f1)/T;
        %Phi0(i) = 2*pi*f1/gamma;
        %Phi{i} = @(t) Phi0(i) .* (exp(gamma.*t)-1);
        Phi0 = (2*pi) * f1 * T/log(f2/f1);
        Phi = @(t) Phi0 * (f2/f1).^(t/T);
        PhiF = @(t) f1.*exp(gamma .* t);
        
        % Optional delay function (e.g. to extract reflection source
        % component from DPOAE). Delay function is specified as a
        % function of frequency.
        if ~isempty(delay),
            x = (f1)*(exp(gamma .* (t) )-1) + f1; % sweep frequency
            delayfunc = feval(@(x) eval(delay), x) ./ 1000; % evaluate delay function at sweep frequency (in ms)
            delayfunc = computePhiTau(delayfunc, x);
            %% TODO: figure out equation with delay
            Phi = @(t) Phi0 * (f2/f1).^(t/T);
        end
        
    case {'linsweep'}  % linear sweep
        w = 2*pi*f1;
        wdot = pi*(f2-f1)/T;
        Beta = (f2-f1)/T;
        Phi = @(t) (w + wdot .* t) .* t;
        PhiF = @(t) f1 + Beta .* t;
        % Optional delay function (e.g. to extract reflection source
        % component from DPOAE). Delay function is specified as a
        % function of frequency.
        if ~isempty(delay),
            x = f1+Beta.*t; % sweep frequency
            delayfunc = feval(@(x) eval(delay), x) ./ 1000; % evaluate delay function at sweep frequency (in ms)
            delayfunc = computePhiTau(delayfunc, x);
            %% TODO: figure out equation with delay
        end
           
    case {'tone'},
        f = f1;
        w = 2*pi*f;
        Phi = @(t) w .* t;
        PhiF = @(t) f;           % does not change with time, t, but used to keep format the same.
end

function Phi_tau = computePhiTau(delayfunc, x)
    %% computePhiTau
    sam = 0;
    ndata = length(delay);
    ndatap = ndata -1;
    Phi_tau = zeros(1, ndata);
    Phi_tau(1) = 0;
    sam = (-pi/2) * delay(1) * (freq(2)-freq(1));
    Phi_tau(2) = sam;
    for n = 2:ndatap
        sam = sam - pi/2 * (delay(n) + delay(n-1)) * (freq(n+1)-freq(n-1));
        Phi_tau(n) = sam;
    end
    Phi_tau(1) = 2*Phi_tau(2) - Phi_tau(3);
    dphi = Phi_tau(1);
    for n = 1:ndata-1
        Phi_tau(n) = Phi_tau(n) - dphi;
    end
    Phi_tau(ndata) = 2*Phi_tau(ndata-1) - Phi_tau(ndata-2);
end

end