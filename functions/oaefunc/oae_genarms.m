function [armsx armsy] = oae_genarms(x,y,varargin)
%
% GENARMS    generate wieghted rms value
%
% Optional arguments
%   fwind - window width in octaves (default: 0.333)
%   fstep - window step size in octaves (default 0.111);
%   power (boolean) - use power instead of amplitude; possible values: 0 (false) 1 (true) [default 1]
%
% Example usage:
% 
% using default values:
%   [armsx armsy] = genarms(x, y);
%  
% ocatve band window with 1/2 octave step size
%   [armsx armsy] = genarmsx, y, 'fwind', 1, 'fstep', 0.5);
%
% use power instead of amplitude
%   [armsx armsy] = genarmsx, y, 'power', 1);
%
%
% % ported to MATLAB from orignal shell script
% % Simon Henin <shenin@gc.cuny.edu>
% %
% % Rev. 30/12/09 Simon Henin
% % Rev. 08/05/10 Simon Henin; added optional variables 


%% default variables
dref = 10;
units =2e-5;
d_oct =0.001;
fwind = 0.333;
fstep = 0.111;
power = 1;

optargin = size(varargin,2);
for k= 1:optargin,
    switch varargin{k}
        case 'fwind'
            fwind = varargin{k+1};
        case 'fstep'
            fstep = varargin{k+1};
        case 'power'
            power = varargin{k+1};
    end
end

%set constants
if power,
    scale = units*units;
    dref = 10;
else
    scale = units;
    dref = 20;
end

ptmp =  [log(x)./log(2) 10.^(y./dref) .* units .* units];

range = [d_oct*floor(min(ptmp(:,1))/d_oct) d_oct*floor(max(ptmp(:,1))/d_oct)];
gstep = range(1):d_oct:range(2);
pspl = [gstep; spline(ptmp(:,1), ptmp(:,2), gstep)]';


% windowing & stepping initial values
nwind = floor(fwind/d_oct+0.5);
nstep = floor(fstep/d_oct+0.5);
ntotal = size(pspl,1);

nstart = 1;
nend = nstart + nwind;

t = 1:1:nwind+1;
pweight = 0.5 * (1 - cos(2*pi*(t-0.5)/(nwind+1)));

% where the work gets done
arms = [];
while (nend < ntotal)
    
    pwin = pspl(nstart:nend, :);
    
    xsum = sum(pwin(:,1));
    nsum = size(pwin,1);
    ksum = sum(pwin(:,2).*pweight');
    wsum = sum(pweight);
    
    m = [2^(xsum/nsum) dref*log10(ksum/wsum/scale)];
    arms = [arms; m];
    
    nstart = nstart + nstep;
    nend = nend + nstep;
end
%return the results
armsx = arms(:,1);
armsy = arms(:,2);

