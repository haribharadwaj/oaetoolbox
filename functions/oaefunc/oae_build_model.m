function F = oae_build_model(Phi, nfilt, t)
% Builds the model matrx F from phase functions defined in func_handle and
% evaluated at time, t

% Build the model including each component
nPhi = length(Phi);
F = zeros(nPhi*2, nfilt);
for i=1:nPhi,
    pf = feval(Phi{i}, t); % evaluate phase function
    F((2*i)-1, :) = cos(pf);
    F((2*i), :) = sin(pf);
end


end