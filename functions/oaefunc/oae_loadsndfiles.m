function [DATA CMD STATE] = oae_loadsndfiles(STATE)

[filename, pathname, filterindex] = uigetfile( ...
       {'*.snd','SUN-Audio (*.snd)'; ...
        '*.*',  'All Files (*.*)'}, ...
        'Pick a file', ...
        'MultiSelect', 'on');

DATA = []; sep = filesep;   
for i=1:length(filename),
    [y fs bits opts] = sndread([pathname sep filename{i}]);
    Data(i).y = y;
    Data(i).fs = fs;
    
    % get snd info from header
    sndinfo = extractsndinfo(opts);
    names = fieldnames(sndinfo);
    for j=1:length(names),
        eval(['Data(i).' names{j} '= sndinfo.' names{j} ';']);
    end
    
end
    

CMD = sprintf('[DATA CMD] = %s;', mfilename);    
    