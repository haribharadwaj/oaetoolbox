function [filename id level sweep freq dB phi] = oae_loadfile(filename)
% load file and parse info
%
% Copyright (c) 2009, Simon Henin
if nargin ~= 1,
    error('myApp:argChk', 'Wrong number of input arguments')
end

if ~ischar(filename)
    error('myApp:argChk', 'input argument must be a string')
end

tokens = explode(filename);
id = tokens(1);

for i=2:size(tokens,2),
    if regexp(tokens{i}, 'L??'), % level match
        level = tokens{i}(2:end);
    elseif regexp(tokens{3}, '[0-9]{2,}-[0-9]{2,}'); %sweep match
        s = explode(tokens{i}(2:end), '-');
        sweep = [str2num(s{1}) str2num(s{2})];
    end
end

[pathstr, name, ext, versn] = fileparts(filename)

dbfile = [pathstr filesep name '.phi'];
if exists(dbfile)
    data = load(dbfile);
    freq = data(:,1);
    dB = data(:,2);
else
    warning('DB file not found');
    freq = [];
    dB = [];
end

phifile = [pathstr filesep name '.phi'];
if exists(phifile)
    data = load(phifile);
    phi = data(:,2);
    if empty(freq)
        freq = data(:,1);
    end
else
    warning('PHI file not found.')
    phi = [];
end