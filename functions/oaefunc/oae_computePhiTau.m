function Phi_tau = oae_computePhiTau(delay, freq)


%% computePhiTau
% TODO: figure out way to include in feval
% USE only for LSF of SFOAE
sam = 0;
ndata = length(delay);
ndatap = ndata -1;
Phi_tau = zeros(1, ndata);
Phi_tau(1) = 0;
sam = (-pi/2) * delay(1) * (freq(2)-freq(1));
%Phi_tau(2) = sam;
for (n = 2:ndatap),
    sam = sam - pi/2 * (delay(n) + delay(n-1)) * (freq(n+1)-freq(n-1));
    Phi_tau(n) = sam;
end
% does not seem necessary for this problem...double check and remove
% -------------------------------------------------------
Phi_tau(1) = 2*Phi_tau(2) - Phi_tau(3);
dphi = Phi_tau(1);
for (n = 1:ndata-1),
    Phi_tau(n) = Phi_tau(n) - dphi;
end
Phi_tau(ndata) = 2*Phi_tau(ndata-1) - Phi_tau(ndata-2);


end