function [coef residual rms_error] = oae_lsf(Phi, delay, nfilt, y, Wvec)
%
% Simple lsf

% transform data that needs it
if size(y,1) > size(y,2),
    y = y';
end

% Build the model including each component
nPhi = min(size(Phi));
F = zeros(nPhi*2, nfilt);

for i=1:nPhi,
    F((2*i)-1, :) = cos(Phi(:,i)+delay(:,i));
    F((2*i), :) = sin(Phi(:,i)+delay(:,i));
end
FT = F'; % Create transpose once for speed

% LSF
A = zeros(nPhi*2, nPhi*2);
for i = 1:nPhi*2
    tmp = Wvec .* F(i, :);
    for j = 1:nPhi*2
        A(i,j) = tmp * FT(:,j);
    end
end
b =  F * (Wvec .* y)';

% solve the matrix equation A*x = b
coef = A \ b;

est = FT * coef;
residual = (Wvec .* (y - est'))';
rms_error = sqrt(mean(residual.^2));

end