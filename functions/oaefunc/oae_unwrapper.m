function phase = oae_unwrapper(phase, tolerance)
% 
%   special phase unwrapper
%   Simon Henin
%   9/14/12 normalize phase

phaseOffset = 0;
ophase = 0;
for n=1:length(phase),
    if ((phase(n) > tolerance) & (ophase < -tolerance))
        phaseOffset = phaseOffset-360;
    elseif ((phase(n) < -tolerance) & (ophase > tolerance))
        phaseOffset = phaseOffset + 360;
    end
    phase(n) = phase(n) + phaseOffset;
    ophase = phase(n);
    
end

