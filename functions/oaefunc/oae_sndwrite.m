function oae_sndwrite(y,Fs,nbits,method,comment,sndfile)
%SNDWRITE Write NeXT/SUN (".snd") sound file.
%   SNDWRITE(Y,SNDFILE) writes a sound file specified by the
%   string SNDFILE.  The data should be arranged with one channel
%   per column.  Amplitude values outside the range [-1,+1] are
%   clipped prior to writing. 
%
%   Supports multi-channel data for 8-bit mu-law, and 8- and
%   16-bit linear formats:
%
%   SNDWRITE(Y,Fs,SNDFILE) specifies the sample rate of the data
%   in Hertz.
%
%   SNDWRITE(Y,Fs,BITS,SNDFILE) selects the number of bits in
%   the encoder.  Allowable settings are BITS=8 and BITS=16.
%
%   SNDWRITE(Y,Fs,BITS,METHOD,SNDFILE) allows selection of the
%   encoding method, which can be either 'mu', 'linear' or 'floatingpoint'
%   Note that mu-law files must be 8-bit. By default, method='mu'.
%   Floating-point files must either be 32-bit or 64-bit floats.
%   Internally, the floating point numbers are always written big-endian.
%
%   SNDWRITE(Y,Fs,BITS,METHOD,COMMENT, SNDFILE) same as 
%   SNDWRITE(Y,Fs,BITS,METHOD,SNDFILE), but inserts the comment string
%   into the comment field.
% 
%   See also SNDREAD, WAVWRITE.

%   Copyright 1984-2004 The MathWorks, Inc. 
%   $Revision: 1.1.6.2 $  $Date: 2004/03/05 18:10:34 $

%   D. Orofino, 10/95

% Get user default preferences:
Fs_pref = 8000; nbits_pref = 8; method_pref='mu';

% Parse inputs:
if nargin==1,
  % Compatibility with old sndwrite:
  warning(['This syntax for sndwrite is obsolete and should ' ...
           'be replaced by sound(y).']);
  sound(y);
  return;
elseif nargin>6,
  error('Incorrect number of input arguments.');
elseif nargin==5,
  sndfile=comment;
  comment = '';
elseif nargin==4,
  sndfile=method;
  comment = '';
  method=method_pref;
elseif nargin==3,
  sndfile=nbits;
  comment = '';
  method=method_pref;
  nbits=nbits_pref;
elseif nargin==2,
  sndfile=Fs;
  comment = '';
  method=method_pref;
  nbits=nbits_pref;
  Fs=Fs_pref;
end

% Open file for output:
if ~ischar(sndfile),
  error('Filename must be a string.');
end
if isempty(strfind(sndfile,'.')),
  sndfile=[sndfile '.snd'];
end
fid=fopen(sndfile,'wb','b'); % Big-endian
if (fid==-1),
  error('Cannot open sound file for output. File may be in use.');
end


% 
% force it to be a column vector
%
sz = size(y);
if sz(1) < sz(2)
   y = y';
end

% Note that we don't enforce floating point values with magnitudes less
% than one.
% if ~strncmp(method,'floatingpoint',length(method))
%     % Clip data to normalized range [-1,+1]:
%     i=find(abs(y)>1);
%     if ~isempty(i),
%         y(i)=sign(y(i));
%         warning('Data clipped during write to file.');
%     end
% end

% Write sound header:
snd = write_sndhdr(fid,Fs,nbits,method, size(y), comment);
if ~isstruct(snd), error(snd); end

% Write data:
if write_snddata(fid,snd,y),
  error('Error while writing sound file.');
end

fclose(fid);    % Close file
return;

% end of sndwrite()

% ------------------------------------------------------------------------
% Private functions:
% ------------------------------------------------------------------------

% WRITE_SNDHDR: Write sound file header structure
%   Assumes fid points to the start of an open file.
%
function snd = write_sndhdr(fid,Fs,nbits,method, sz, comment)
    %
    % Interpret format:
    %
    if strncmp(method,'mu',length(method)),
      if nbits~=8,
	snd=['Mu-law can only be used with 8 bit data.' ...
	     ' Use method=''linear'' instead.']; return;
      end
      snd.format=1;  % 8-bit mu-law
      snd.bits = 8;
    elseif strncmp(method,'linear',length(method)),
      if nbits==8,
	snd.format=2; % 8-bit linear
	snd.bits = 8;
      elseif nbits==16,
	snd.format=3; % 16-bit linear
	snd.bits = 16;
      else
	snd='Unrecognized data format selected.'; return;
      end
    elseif strncmp(method,'floatingpoint',length(method)),
	  if nbits == 32,% Single precision
	      snd.format = 6;
	      snd.bits = 32;
	  elseif nbits == 64,% Double-precision
	      snd.format = 7;
	      snd.bits = 64;
	  else
	      snd='Unrecognized data format selected.'; return;
	  end
    else
      snd='Unrecognized data format selected.'; return;
    end

    % Define sound header structure:
    snd.samples = sz(1);
    snd.chans = sz(2);
    total_samples = snd.samples*snd.chans;
    bytes_per_sample = ceil(snd.bits/8);
    snd.rate = Fs;
    snd.databytes = bytes_per_sample * total_samples;
    if strcmp(comment, '') == 1
	snd.offset = 28;
	snd.info = [fix('   ') 0];  % 4-character null terminated string
    else
     %   keyboard;
	len = length(comment);
	if len < 4
	    len = 4;
	    comment = sprintf('%4s', comment);
	end
	snd.offset = 28 + len - 4;
	snd.info = fix(comment);
    end

    fwrite(fid,'.snd','char');          % magic number
    fwrite(fid,snd.offset,'uint32');    % data location
    fwrite(fid,snd.databytes,'uint32'); % size in bytes
    fwrite(fid,snd.format,'uint32');    % data format
    fwrite(fid,snd.rate,'uint32');      % sample rate
    fwrite(fid,snd.chans,'uint32');     % channels
    fwrite(fid,snd.info,'char');        % info

return;


% WRITE_SNDATA: Write sound data
function status = write_snddata(fid,snd,data)
status = 0;

% Interpret format:
if snd.format==1,
  dtype='uchar'; % 8-bit mu-law
  data = lin2mu(data);
elseif snd.format==2,
  dtype='int8';
  data = round(data * (2^7-1));
elseif snd.format==3,
  dtype = 'int16';
  data  = round(data * (2^15-1));
elseif snd.format==5,
  dtype = 'int32';
  data  = round(data * (2^31-1));
elseif snd.format==6,
  dtype = 'float';
  data  = single(data);
elseif snd.format==7,
  dtype = 'double';
  data  = double(data);
else
  status=-1; return;
end

% Write data, one row at a time (one sample from each channel):
total_samples = snd.samples*snd.chans;
cnt = fwrite(fid, reshape(data',total_samples,1), dtype);
if cnt~=total_samples, status=-1; end

return;

% end of sndwrite.m
