function magdB = er10cMicCorrectionLookup(obj, f)
% returns correction value of a filter which is in dB and normalized to
% 1kHz

fs = obj.fs;
n = size(obj.rtFrqMicCalibrationFilterDB,1);
fvec = linspace(0,fs/2,n);

I = find(fvec > f);
idx = I(1);

magdB = obj.rtFrqMicCalibrationFilterDB(idx);

magdB
end


