function [OAE SEQUENCE] = oae_loadanalysisobject(datapath)

if ~exist('datapath', 'var')||isempty(datapath),
    datapath = '';
end
[filename, pathname] = uigetfile('*.oae', 'Pick an M-file', datapath);
OAE = []; SEQUENCE = [];
if isequal(filename,0)
    
else
    load(fullfile(pathname, filename), '-mat');
    SEQUENCE = 1;
    close();
    
    assignin('base', 'OAE', OAE);
%     assignin('base', 'modules', modules);
    assignin('base', 'SEQUENCE', SEQUENCE);
    
    evalin('base', 'oae_analysis_setup;oae_update_analyses(OAE, SEQUENCE);');
end