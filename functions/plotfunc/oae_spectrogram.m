function OAE = oae_spectrogram(OAE, SEQUENCE)
% This function is useful for assessing how clean (artifact free) the average is.
% updated to work with current analysis methods - JJH Jan 20-2017 
fs = OAE(SEQUENCE).fs;
try
data = OAE(SEQUENCE).analysisObj.getCurrentAverage();
% obj.avg(:,obj.Buffer, obj.Condition)
sz = size(data, 1); 
win = 300;
%NFFT = OAE(SEQUENCE).rtNfilt;
NFFT = 1024;
win = NFFT;
overlap = floor(win/2);
%block_len = frame_size/2;                                        % block length
%overlap = block_len - step_size;                                 % overlap (in samples)
%spectrogram_window = hann(block_len)';                           % Glenis uses a hann window to focus the LSF on the middle samples of a given data_frame so we use it here.
%spectrogram_window = spectrogram_window / sqrt( sum( spectrogram_window.^2 ) );
Inan = find(isnan(data)); % TODO, find out why there are NAN's I saw them with SF. 
data(Inan) = 0; ypos = 0;
h = findobj('tag', 'analysis.window');
% units = get(findobj('tag', 'analysis.window'), 'units');
set(h, 'units', 'normalized');
sz = get(h, 'position');
for p = 1: size(data,3)
    for n = 1: size(data,2)
        
        x = data(:, n, p);
        figure('Name','OTB Spectrogram','NumberTitle','off', 'units', 'normalized', 'position', [sz(1)+ypos sz(2)+(sz(4)/2)-ypos (sz(3)/2) (sz(3)/3)]);
        spectrogram(x,win,overlap,NFFT,fs, 'yaxis');
        axis xy
        hh = colorbar;
        title(['Spectrogram (dB SPL) of Buffer: ' int2str(n) ', Condition: ' int2str(p)])
        ylim([0 20]);
        set(gca, 'CLim', [-20 70]);
        ylabel(hh, 'dB SPL');
        ypos = ypos+0.1;
    end
end
catch
    oae_dialog('You must create at least 1 average before plotting the spectrogram.');

end