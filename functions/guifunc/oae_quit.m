function oae_quit
%
% quit oaetoolbox
% 8/7/2012 - added functionally to stop playback and clear OAE object

oae_log('User close request...attempting to shutdown');
try 
    OAE = evalin('base', 'OAE;');
    SEQUENCE = evalin('base', 'SEQUENCE;');
    modules = evalin('base', 'modules');
catch
    OAE = [];
    SEQUENCE = [];
end

if ~isempty(OAE),
    OAE(SEQUENCE).playback = false;
    % autosave defaults
end
try,
oae_loaddefaults(modules, 1);
catch
end

if isdeployed,
    global iIsAnalysisGuiOpen vbCommand
    
    if iIsAnalysisGuiOpen + vbCommand == 0
        %Let VB know that MATLAB has closed the window
        vb_notify_complete;
    end
else
    
end


%clear OAE*;
%close(gcf);  
    

delete(get(0,'Children'));

end