function W_MAIN = oae_makegui()

load oae_settings;

SIZE(2) = SIZE(2)-BORDERBOTTOM;
set(0, 'Units', 'Pixels');
scrsz = get(0,'ScreenSize');
%javax.swing.UIManager.setLookAndFeel('com.jgoodies.looks.plastic.Plastic3DLookAndFeel');

% BGCOLOR = get(0, 'defaultFigureColor');
% % set(0,'DefaultFigureColor',BGCOLOR);
set(0,'DefaultUipanelBackgroundColor',get(0,'DefaultFigureColor'));
set(0,'DefaultUicontrolBackgroundColor',get(0,'DefaultFigureColor'));
% % set(0,'DefaultUipanelHighlightColor',HIGHLIGHTCOLOR);
% % set(0,'DefaultUipanelShadowColor',SHADOWCOLOR);
set(0,'DefaultUicontrolUnits','Normalized');
set(0,'DefaultUicontrolHorizontalAlignment','Left');
% set(0,'DefaultUicontrolFontsize',11);


W_MAIN = figure('Units','pixels', ...
    'PaperPosition',paperposition, ...
    'PaperUnits','points', ...
    'name', [ 'OAEToolBox v' prgversion ], ...
    'numbertitle', 'off', ...
    'resize', 'on', ...
    'Units', 'Pixels', ...
    'Position', [(scrsz(3)-SIZE(1))/2 (scrsz(4)-SIZE(2))/2 SIZE(1) SIZE(2)], ...%'PaperPositionMode', 'auto',...
    'Tag','OAEToolbox', ...
    'Toolbar', 'none', ...
    'Menubar', 'none', ...
    'visible', 'on', ...
    'Userdata', {[] []},...
    'CloseRequestFcn', 'oae_quit');


try,
    set(W_MAIN, 'NextPlot','new');
catch, end;


% % add background image
% ha = axes('units','normalized','position',[0 0 0.3 0.3]);
% imshow(['misc' filesep 'CUNY-GC-logo.png'], 'parent', ha);
% %hi = imagesc(I);
% set(ha,'handlevisibility','off', 'visible','off');
% uistack(ha,'bottom');

% Menus
menu_file = uimenu('Label', 'File', 'tag', 'file');
%menu_edit = uimenu('Label', 'Edit', 'tag', 'edit');

menu_daq = uimenu('Label', 'Settings', 'tag', 'daq_menu');
% menu_analyze = uimenu('Label', 'Analysis', 'tag', 'analyze');
menu_calibration = uimenu('Label', 'Calibration', 'tag', 'calibration_menu');
menu_tools = uimenu('Label', 'Tools', 'tag', 'tools');
menu_help = uimenu('Label', 'Help', 'tag', 'help');



%menu_sequence = oae_addmenuitem(menu_file, 'Sequences', '', 'on');
oae_addmenuitem(menu_file, 'Load sequence...', 'oae_loadsequence(OAE, SEQUENCE);', 'on');
oae_addmenuitem(menu_file, 'Save sequence...', 'OAE = oae_savesequence(OAE, SEQUENCE);', 'on');
oae_addmenuitem(menu_help, 'Restore defaults', 'oae_deletedefaults();', 'on');
oae_addmenuitem(menu_tools, 'Extras', @oae_createmodulepopout, 'on', 'tag', 'extras');
if ~isdeployed,
    oae_addmenuitem(menu_file, 'Load analysis...', 'OAE = oae_loadanalysisobject();', 'on');
end
% oae_addmenuitem(menu_analyze, 'Artifact Rejection', @oae_createmodulepopout, 'on', 'tag', 'artifactrejection');
oae_addmenuitem(menu_calibration, 'Settings', @oae_createmodulepopout, 'on', 'tag', 'calibration');
%oae_addmenuitem(menu_calibration, 'FPL Cavity Calibration', @oae_createmodulepopout, 'on', 'tag', 'calibration');
%% Quit
oae_addmenuitem(menu_file, 'Quit', 'oae_quit', 'on', 'Accelerator', 'W');


oae_addmenuitem(menu_daq, 'Input/Ouput', @oae_createmodulepopout, 'on', 'tag', 'daq');
oae_addmenuitem(menu_daq, 'Configuration', 'OAE = oae_otb_options(OAE, SEQUENCE);', 'on', 'tag', 'otb_config');

vpad = 0.02; % TODO: make this part of oae_settings defaults

%% Panels Setup

% Panel to enable modification of current I/O configuration
IO = uipanel('title','Audio Configuration','units', 'normalized', 'position', [0.01 1-0.2+vpad 0.48 0.2-vpad], 'tag', 'daq_config');
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'string', 'Driver: ', 'fontweight', 'bold', 'position', [0.01 0.72 0.2 0.25]);
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'tag', 'daq_config.driver', 'string', '', 'position', [0.12 0.72 0.65 0.25]);
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'string', 'Device:', 'fontweight', 'bold', 'visible', 'off','position', [0.01 0.55 0.2 0.25]);
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'tag', 'daq_config.device_name', 'string', '', 'visible', 'off','position', [0.12 0.55 0.65 0.25]);
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'string', 'Input Channels: ', 'fontweight', 'bold', 'visible', 'off','position', [0.01 0.32 0.4 0.25]);
uicontrol(IO, 'style', 'popupmenu', 'units', 'normalized', 'tag', 'daq_config.input_channels', 'string', {''}, 'position', [0.4 0.33 0.2 0.25], 'visible', 'off','callback', 'OAE = oae_update_io_config(OAE, SEQUENCE, 1);');
uicontrol(IO, 'style', 'text', 'units', 'normalized', 'string', 'Output Channels: ', 'fontweight', 'bold', 'visible', 'off','position', [0.01 0.05 0.4 0.25]);
uicontrol(IO, 'style', 'popupmenu', 'units', 'normalized', 'tag', 'daq_config.output_channels', 'string', {''}, 'position', [0.4 0.05 0.2 0.25], 'visible', 'off','callback', 'OAE = oae_update_io_config(OAE, SEQUENCE, 1);');
uicontrol(IO, 'style', 'pushbutton', 'units', 'normalized', 'string', 'Configure Device', 'fontweight', 'normal', 'visible', 'on', 'position', [0.74 0.06 0.25 0.25], 'callback', @oae_createmodulepopout, 'tag', 'daq');

pos = get(IO, 'position');
pd = uipanel('title','Paradigm', 'units', 'normalized', 'position', [0.01 vpad 0.48 0.8-vpad], 'tag', 'paradigm');
pdd = oae_uicomponent(pd, 'Units', 'normalized',  'style', 'popupmenu', 'string', {'Select a paradigm'}, 'position', [0.02 0.85 0.65 0.1],'tag', 'paradigm.modules', 'callback', @oae_dropdown_callback);
oae_uicomponent(pd, 'Units', 'normalized',  'style', 'pushbutton', 'string', {'Save as Default'}, 'position', [0.71 0.905 0.25 0.05], 'fontsize', 10, 'tag', 'save_button', 'callback', @oae_savedefaults);
pdp = uipanel(pd, 'Title', 'Parameters', 'Position',[0.02 .01 0.96 .8], 'tag', 'paradigm.parameters', 'bordertype', 'none');

% sequence panel
pos = get(pd, 'position');
seq = uipanel('title','Sequence', 'units', 'normalized', 'position', [0.51 1-0.8+vpad 0.48 0.8-vpad], 'tag', 'sequence');
seq_textbox = oae_uicomponent(seq, 'style', 'text', 'units', 'normalized', 'position', [0.01 0.05 0.94 0.9], 'String', 'Saved Sequences:', 'horizontalalignment', 'left', 'tag', 'sequence.list');

%start button
cmd = '[OAE SEQUENCE] = oae_start(OAE,SEQUENCE);';
pos = get(seq, 'position');
oae_uicomponent(W_MAIN, 'style', 'pushbutton', 'String', 'Start', 'fontsize', 12, 'fontweight', 'bold', 'units', 'normalized', 'position', [0.6 0.08 0.3 0.05], 'callback', cmd);
set(W_MAIN, 'userdata', {[] []}, 'visible', 'on');