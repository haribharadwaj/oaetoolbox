function OAE = oae_analysis_settings(OAE, SEQUENCE)
% popup for analysis setting
%
% 04/02/18 - SH -fixed attribution to approriate analyisObj, added current object setting, added Xlimits setting
%

% get current setting
currentIndex = OAE(SEQUENCE).currentIndex;
if isempty(currentIndex),
    oae_dialog('Please analyze data to change settings');
    return;
end
xlims = OAE(SEQUENCE).datasets(currentIndex(1)).analysisObj(currentIndex(2)).Xlimits;
xscale = OAE(SEQUENCE).datasets(currentIndex(1)).analysisObj(currentIndex(2)).Xscale;
wrapping = OAE(SEQUENCE).datasets(currentIndex(1)).analysisObj(currentIndex(2)).phaseWrap;

h = findobj('tag', 'settings.window');
if isempty(h),
    f = oae_make_dialog('medium', 'tag', 'settings.window');
    cmd = 'OAE = oae_set_analysis(OAE, SEQUENCE);';
    %cmd2 = 'OAE = oae_set_phasewrapping(OAE, SEQUENCE);';
    left = 0.015; top = 0.87;
    oae_uicomponent(f, 'style', 'text', 'string', 'Analysis Setting', 'fontsize', 13, 'position', [left top 0.75 0.08]);
    top = top-0.12;
    oae_uicomponent(f, 'style', 'text', 'string', 'X-Axis Scale: ', 'position', [left top 0.3 0.08]);
    oae_uicomponent(f, 'style', 'popupmenu', 'string', {'linear', 'log'}, 'value', find(strcmp( xscale, {'linear', 'log'})), 'tag', 'Xscale', 'callback', cmd, 'position', [left+0.3 top 0.65 0.08]);
    
    top = top-0.12;
    oae_uicomponent(f, 'style', 'text', 'string', 'X-Axis Limits: ', 'position', [left top 0.3 0.08]);
    oae_uicomponent(f, 'style', 'edit', 'string', num2str(xlims(:)'), 'tag', 'Xlimits','position', [left+0.3 top 0.65 0.08], 'callback', cmd);
    % PHASE WRAPPING SETTINGS
    top = top-0.12;
    oae_uicomponent(f, 'style', 'text', 'string', 'Phase Wrapping: ', 'position',  [left top 0.3 0.08]);
    oae_uicomponent(f, 'style', 'popupmenu', 'string', {'wrapped', 'unwrapped'}, 'value', find(strcmp( wrapping, {'wrapped', 'unwrapped'})), 'tag', 'phaseWrap', 'callback', cmd, 'position', [left+0.3 top 0.65 0.08]);
    % top = top-0.09;
    % oae_uicomponent(f, 'style', 'text', 'string', 'Y-Axis Scale: ', 'position', [left top 0.2 0.08]);
    % oae_uicomponent(f, 'style', 'popupmenu', 'string', {'linear', 'log'}, 'tag', 'yscale', 'callback', cmd, 'position', [left+0.2 top 0.75 0.08]);
else
    figure(h);
end



end