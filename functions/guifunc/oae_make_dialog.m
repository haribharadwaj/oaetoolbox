function f = oae_make_dialog(size, varargin)

load oae_settings;
tag = 'OAEDialog';
for i=1:2:length(varargin),
    switch varargin{i}
        case 'tag'
            tag = varargin{i+1};
    end
end

switch size,
    case 'small'
        SIZE = [300 200];
    case 'medium'
        SIZE = [400 300];
    case 'large'
        SIZE = [600 400];
    otherwise
end

SIZE(2) = SIZE(2)-BORDERBOTTOM;
set(0, 'Units', 'Pixels');
scrsz = get(0,'ScreenSize');

f = figure('Units','pixels', ...
	'PaperPosition',paperposition, ...
	'PaperUnits','points', ...
	'name', [ 'OAEToolBox v' prgversion ], ... 
	'numbertitle', 'off', ...
	'resize', 'off', ...
	'Units', 'Pixels', ...
    'Position', [(scrsz(3)-SIZE(1))/2 ((scrsz(4)-SIZE(2))/2)+BORDERBOTTOM SIZE(1) SIZE(2)], ...
	'Tag',tag, ...
    'Toolbar', 'none', ...
    'Menubar', 'none', ...
	'Userdata', {[] []});