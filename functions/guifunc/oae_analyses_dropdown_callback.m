function oae_analyses_dropdown_callback(hObject, eventdata, handles)
% executes upon update of "cleaned" and "analyses" dropdowns

% if "cleaned" dropdown, update the current dataset and load analyses into
% dropdown
if strcmp(hObject.Tag, 'datasets'),
    cmd = sprintf('OAE(SEQUENCE).current_dataset = %i; OAE(SEQUENCE).currentIndex = [%i 1]; OAE(SEQUENCE).setActiveDataset(); oae_update_analyses(OAE, SEQUENCE);', get(hObject,'Value'), get(hObject,'Value'));
    evalin('base', cmd);
    
else % analyses was changed
    cmd = sprintf('OAE(SEQUENCE).currentIndex(2) = %i; oae_plot(OAE, SEQUENCE);', get(hObject,'Value'));
    evalin('base', cmd);
end