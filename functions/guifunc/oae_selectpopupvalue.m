function oae_selectpopupvalue(handle, value, varargin)


if ~ishandle(handle),
    handle = findobj('tag', handle);
end

userdata = [];
for i=1:2:length(varargin),
    switch varargin{i},
        case {'userdata'}
            userdata = varargin{i+1};
    end
end
if length(varargin) == 0,
    userdata = get(handle, 'userdata');
end

string = get(handle, 'string');
index = find(strcmp({string{:}}, value));
if ~isempty(index),
    set(handle, 'value', index, 'userdata', userdata);
else
    %error('index not valid');
end

end