function oae_addmodule(handle, cmd, label)

% add a module to menu or dropdown and assign callback
type = get(handle, 'type');
switch type
    case 'uicontrol',
        if isnumeric(cmd),
            cmd = num2str(cmd);
        end
        items = get(handle, 'String');
        if isempty(items),
            items = {label};
        else
            items = [items(:)' label];
        end
        set(handle, 'string', items);
        
        userdata = get(handle, 'UserData');
        if isempty(userdata),
            userdata = {cmd};
        else
            userdata = {userdata{:} cmd};
        end
        set(handle, 'Userdata', userdata);
        
    case 'uimenu'
        uimenu( handle, 'label', label, 'callback', cmd);
end

end