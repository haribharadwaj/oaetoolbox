function oae_setvalue(handle, value, varargin)
% oae_setvalue(handle, value, varargin)
%   set the value of a uicontrol object
%


userdata = [];
for i=1:2:length(varargin),
    switch varargin{i},
        case {'userdata'}
            userdata = varargin{i+1};
    end
end

if ~ishandle(handle),
   handle = findobj('tag', handle);
end
% if  isobject(panel) % strcmp(vv, '(R2015a)')
%    handle = panel.Children;
% else
%     handle = findobj('tag', panel);
% end
if length(varargin) == 0,
    userdata = get(handle, 'userdata');
end
if ~isempty(handle),
    switch get(handle, 'style'),
        case 'popupmenu'
            string = get(handle, 'string');
            if isempty(string),
                set(handle, 'string', value, 'userdata', userdata);
            elseif length(string) == 1,
                %if strcmp(string{:}, '')
                %if strcmp(string, '')
                    set(handle, 'string', value, 'userdata', userdata);
                %end
            elseif length(value) > 1
                % check if current value out of bounds
                cval = get(handle, 'value');
                if cval > length(value),
                    cval = 1;
                end
                set(handle, 'String', value, 'value', cval, 'userdata', userdata);
            elseif isnumeric(value),
                value = num2str(value);
                val = find(strcmp(get(handle, 'string'), value));
                if isempty(val), val = 1; end
                set(handle, 'value', val, 'userdata', userdata);
            end
        otherwise
            if isnumeric(value),
                value = num2str(value);
            end
            set(handle, 'string', value, 'userdata', userdata);
    end
end

end