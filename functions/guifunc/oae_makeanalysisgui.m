function figh = oae_makeanalysisgui()
% 10/2/12 V 1.7.0 removed load and save analysis when deployed. Joshua J. Hajicek
% 8/4/16    minor cosmetic updates. Simon H.

load oae_settings;

ANL_GUI_SIZE(2) = ANL_GUI_SIZE(2)-BORDERBOTTOM;
set(0, 'Units', 'Pixels');
scrsz = get(0,'ScreenSize');
scrsz = scrsz(1,:); % make sure it only appears on primary monitor

figh = figure('Units','pixels', ...
	'PaperPosition',paperposition, ...
	'PaperUnits','points', ...
	'name', [ 'OAEToolBox v' prgversion ], ... 
	'numbertitle', 'off', ...
	'resize', 'on', ...
	'Units', 'Pixels', ...
    'Position', [(scrsz(3)-ANL_GUI_SIZE(1))/2 (scrsz(4)-ANL_GUI_SIZE(2))/2 ANL_GUI_SIZE(1) ANL_GUI_SIZE(2)], ...
	'Tag','analysis.window', ...
    'Toolbar', 'none', ...
    'Menubar', 'none', ...
    'visible', 'off', ... 
	'Userdata', {[] []},...
    'CloseRequestFcn', 'oae_quit'); 

% set(0,'DefaultUipanelBackgroundColor',BGCOLOR);
% set(0,'DefaultUicontrolBackgroundColor',BGCOLOR);
set(0,'DefaultUicontrolUnits','Normalized');
set(0,'DefaultUicontrolHorizontalAlignment','Left');
set(0,'DefaultUicontrolFontsize',9);

try,
    set(figh, 'NextPlot','new');
catch, end;

menu_file = uimenu('Label', 'File', 'tag', 'file');
menu_edit = uimenu('Label', 'Edit', 'tag', 'edit');

%% menu daq (no longer used, now in uipanel) 
% menu_daq = uimenu('Label', 'Acquisition', 'tag', 'daq');
% menu_daq_io = uimenu('Parent', menu_daq, 'label', 'I/O', 'tag', 'IO');
% menu_daq_io = uimenu('Parent', menu_daq, 'label', 'Calibration', 'tag', 'calibration');

menu_analyze = uimenu('Label', 'Analyze', 'tag', 'analyze');
menu_help = uimenu('Label', 'Help', 'tag', 'help');
oae_addmenuitem(menu_analyze, 'Show Spectrograms', 'OAE = oae_spectrogram(OAE, SEQUENCE);','on');

%menu_sequence = oae_addmenuitem(menu_file, 'Sequences', '', 'on');
if ~isdeployed
    oae_addmenuitem(menu_file, 'Load Analysis Object', 'OAE = oae_loadanalysisobject(OAE(SEQUENCE).datapath);', 'on');
    menu_savesequence = oae_addmenuitem(menu_file, 'Save Analysis Object', 'OAE = oae_saveanalysis(OAE, SEQUENCE);', 'on');
end

oae_addmenuitem(menu_file, 'Export Current Analysis Data', 'OAE = oae_export_analysis(OAE, SEQUENCE);',   'on');
oae_addmenuitem(menu_file, 'Export Current Average as Soundfile',   'OAE = oae_exportcurrent_tavg(OAE, SEQUENCE);','on');


%% Quit
oae_addmenuitem(menu_file, 'Quit', 'oae_quit', 'on', 'Accelerator', 'W');
oae_addmenuitem(menu_edit, 'Settings', 'OAE = oae_analysis_settings(OAE, SEQUENCE);', 'on', 'tag', 'settings');


% sequence dropdown
oae_uicomponent(figh, 'Units', 'normalized',  'style', 'text', 'string', '  Sequence','position', [0.01 0.95 0.2 0.05], 'horizontalalignment', 'left', 'fontweight', 'bold');
dropdown = uicontrol(figh, 'Units', 'normalized',  'style', 'popupmenu', 'string', {''},'position', [0.01 0.925 0.2 0.05], 'tag', 'sequence', 'callback', @oae_sequence_dropdown_callback);
% cleaned dropdown
oae_uicomponent(figh, 'Units', 'normalized',  'style', 'text', 'string', '  Cleaned dataset','position', [0.22 0.95 0.2 0.05], 'horizontalalignment', 'left', 'fontweight', 'bold');
analyses = uicontrol(figh, 'Units', 'normalized',  'style', 'popupmenu', 'enable', 'off', 'string', {''},'position', [0.22 0.925 0.2 0.05], 'tag', 'datasets', 'callback', @oae_analyses_dropdown_callback);
% analyses dropdown
oae_uicomponent(figh, 'Units', 'normalized',  'style', 'text', 'string', '  Analysis','position', [0.42 0.95 0.2 0.05], 'horizontalalignment', 'left', 'fontweight', 'bold');
analyses = uicontrol(figh, 'Units', 'normalized',  'style', 'popupmenu', 'enable', 'off', 'string', {''},'position', [0.42 0.925 0.2 0.05], 'tag', 'analyses', 'callback', @oae_analyses_dropdown_callback);


%% Analysis Panel Setup
% takes up 25% of the width
paddingtop = 40; paddingbottom = 10; seq_info_h = 150;
vpad = 0.02;

width1 = round(ANL_GUI_SIZE(1)/4); height = ANL_GUI_SIZE(2)-paddingtop-paddingbottom-seq_info_h;
seq_panel = uipanel('title','Sequence Info:','position', [0.01 1-0.2 0.25 0.16-vpad], 'tag', 'sequence.panel');
oae_uicomponent(seq_panel, 'style', 'edit', 'enable', 'inactive', 'max', 100, 'string', '', 'position', [0.05 0.05 0.9 0.9], 'tag', 'sequence.info', 'FontSize', [9]);

pos = get(seq_panel, 'position');
panel = uipanel('title','Artifact Rejection', 'position', [0.01 pos(2)-0.4+vpad/2 0.25 0.4-vpad], 'tag', 'artifactrejection');
oae_uicomponent(panel, 'Units', 'normalized',  'style', 'popupmenu', 'string', {'Select new AR module'},'position', [0.01 0.85 0.7 0.1], 'tag', 'artifactrejection.modules', 'callback', @oae_dropdown_callback);
parameters = uipanel(panel, 'Title', 'Parameters', 'Position', [0.01 .1 0.98 .75], 'tag', 'artifactrejection.parameters', 'bordertype', 'none');
cmd = 'OAE = oae_artifactrejection(OAE, SEQUENCE);';
oae_uicomponent(panel, 'style', 'pushbutton', 'String', 'Run', 'units', 'normalized', 'enable', 'on', 'position', [0.5-0.15 0.02 0.3 0.08], 'tag', 'button.analyze', 'callback', cmd);          

pos = get(seq_panel, 'position');
panel = uipanel('title','Analysis', 'position', [0.01 vpad 0.25 0.4-vpad], 'tag', 'analysis');
dropdown = oae_uicomponent(panel, 'Units', 'normalized',  'style', 'popupmenu', 'string', {'Select Analysis'},'position', [0.01 0.85 0.7 0.1], 'tag', 'analysis.modules', 'callback', @oae_dropdown_callback);
parameters = uipanel(panel, 'Title', 'Parameters', 'Position', [0.01 .05 0.98 .8], 'tag', 'analysis.parameters', 'bordertype', 'none');
oae_toggle_ui(panel);

%start analysis button          
cmd = 'OAE = oae_analyze(OAE, SEQUENCE);';
oae_uicomponent(panel, 'style', 'pushbutton', 'String', 'Analyze', 'units', 'normalized', 'enable', 'off', 'position', [0.5-0.15 0.02 0.3 0.08], 'tag', 'button.analyze', 'callback', cmd);          

%% Analysis results setup
padding = 20; height = ANL_GUI_SIZE(2)-paddingtop-paddingbottom;
width2 = round(ANL_GUI_SIZE(1)/4)*3 - padding*2;
panel = uipanel('title','Results', 'position', [0.27 vpad 0.73 0.94-vpad], 'tag', 'results');

tools_panel = uipanel(panel, 'title','', 'position', [0.08 0.5+0.35 0.89 0.05], 'tag', 'tools', 'bordertype', 'none'); % plot tools
oae_icon(tools_panel, 'zoom_out', 'position', [0 0.01 10 0.1], 'callback', 'zoom');
% oae_icon(tools_panel, 'zoom_out', 'position', [0.03 0.01 10 0.1], 'callback', 'zoom(''out'')');
oae_icon(tools_panel, 'dataselect', 'position', [0.03 0.01 10 0.1], 'callback', 'datacursormode');

axis1 = axes('Parent', panel, 'Position', [0.08 .5 0.89 .35], 'tag', 'axis_1', 'XGrid', 'On', 'YGrid', 'On', 'fontsize', AXES_FONT_SIZE);
axes(axis1); %xlabel('Frequency (Hz)'); 
ylabel('Amplitude (dB)', 'fontsize', AXES_LABEL_SIZE);
axis2 = axes('Parent', panel, 'Position', [0.08 .1 0.89 .35], 'tag', 'axis_2','XGrid', 'On', 'YGrid', 'On', 'fontsize', AXES_FONT_SIZE);
axes(axis2); xlabel('Frequency (Hz)', 'fontsize', AXES_LABEL_SIZE); ylabel('Phase (Degrees)', 'fontsize', AXES_LABEL_SIZE);
linkaxes([axis1 axis2], 'x');
set(figh, 'userdata', {[] []}, 'visible', 'on');