function w=oae_error(msg, ttle),
if ~exist('ttle', 'var') || isempty(ttle),
    w = errordlg(msg);
else
    w = errordlg(msg, ttle);
end
errorStruct.message = msg;
errorStruct.identifier = 'OaeToolbox:error';
error(errorStruct);
