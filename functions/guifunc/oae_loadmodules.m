function oae_loadmodules(handle, modules, block)
%
% Wrapper to load modules into specified tags, onto a visible figure handle
%
%


% load system defaults file **This might need to go in root loader **\
% -------------------------

%defaults = oae_loadsystemdefaults();
if isempty(modules),
    modules = oae_loaddefaults(); 
end

for i=1:length(block),
    h = findobj(handle, 'tag', [block{i} '.modules']);
    mods = find(strcmp({modules.block}, block{i}));
    for j=1:length(mods),
        oae_addmodule(h, modules(mods(j)).uid, modules(mods(j)).label);
    end
    
    % check if default module set
    % ---------------------------
    if ~isempty(modules),
        def = find(strcmp({modules.block}, block{i}) & [modules.is_default] == 1);
        if ~isempty(def),
            label = modules(def).label;
            oae_selectpopupvalue(h, label);
            % set block to current active module uid
            set(findobj(handle, 'tag', block{i}), 'userdata', modules(def).uid);
            % execute module callback
            module_idx = find(strcmp({modules.label}, label));
            if ~isempty(module_idx),
                try
                    %evalin('caller', modules(module_idx).cmd);  % evaluates all the getters. 
                    evalin('base', modules(module_idx).cmd);
                    protocols_handle = findobj(handle, 'tag', 'protocols');
                    if ~isempty(protocols_handle),
                        % load protocols
                        oae_loadprotocols(protocols_handle, modules(module_idx).protocols);
                    end                    
                catch
                    try 
                        oae_log('eval in base');
                        evalin('base', modules(module_idx).cmd);
                    catch
                        oae_error('default module settings could not be loaded!');
                    end
                end
                
                % set default module values (e.g. last used) if they exist
                oae_load_module_defaults(handle, modules(module_idx));
                % save current state of the controller (so that we can revert if we need to).
                evalin('base', 'OAE(SEQUENCE).saveState;');
            end
        end
    end
end

end