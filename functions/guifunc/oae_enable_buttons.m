function oae_enable_buttons(varargin)


for i=1:length(varargin),
    panel = findobj('tag', varargin{i});
    for j=1:length(panel),
    handles = findobj(panel(j), 'Style', 'pushbutton');
    for k=1:length(handles),
        set(handles(k), 'enable', 'on');
    end
    end
end

end