function oae_clearresults()

handle = findobj('tag', 'results');
children = get(handle, 'children');
for j=1:length(children),
    type = get(children(j), 'Type');
    switch (type),
        case 'axes'
            tag = get(children(j), 'tag');
            cla( children(j) );
            set(children(j), 'tag', tag);
        otherwise
            delete( children(j) );
    end
end

drawnow;
end