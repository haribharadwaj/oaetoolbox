function OAE = oae_otb_options(OAE, SEQUENCE)
% UI for OTB configuration options

%load oae_settings;
paperposition   = [18 180 800 432];
WINMINX         = 17;
WINMAXX         = 360;
WINYDEC			= 13;
NBLINES         = 16;
WINY		    = WINYDEC*NBLINES;
BORDERINT       = 4;
BORDEREXT       = 10;
%BGCOLOR         = [0.7020    0.7020    0.7020];
BGCOLOR         = [0.9    0.9    0.9];
COLOR           = [1 1 1];
FONTSIZE        = 8;
FONTNAME        = 'Arial';
SIZE            = [300 400];

set(0, 'Units', 'Pixels');
scrsz = get(0,'MonitorPositions');
scrsz = scrsz(1,:);

fig = figure('Units','pixels', ...
	'PaperPosition', paperposition, ...
	'PaperUnits','points', ...
	'numbertitle', 'off', ...
	'Units', 'Pixels', ...
    'Position', [scrsz(3)/2-SIZE(1)/2 scrsz(4)/2-SIZE(2)/4 SIZE(1) SIZE(2)], ...
	'Tag', 'popout', ...
    'Toolbar', 'none', ...
    'Menubar', 'none', ...
    'visible', 'on', ...
	'Userdata', {[] []});



panel = uipanel('title', 'OTB Configuration', 'units', 'normalized', 'position', [0.01 0.01 0.98 0.98], 'bordertype', 'none');


uicontrol(panel, 'Units', 'normalized', 'style', 'text', 'string', 'Save raw data files?', 'position', [0.01 0.85 0.5 0.1]);
uicontrol(panel, 'Units', 'normalized', 'style', 'popupmenu', 'string', {'Off' 'On'}, 'tag', 'saveToFile', 'position', [0.58 0.85 0.4 0.1], 'value', OAE(SEQUENCE).autosave+1, 'callback', @oae_update_options, 'userdata', [0 1]);

uicontrol(panel, 'Units', 'normalized', 'style', 'text', 'string', 'Recording Buffer Size', 'position', [0.01 0.75 0.5 0.1]);
bufferSizes = {'256' '512' '1024' '2048' '4098' '8192'};
uicontrol(panel, 'Units', 'normalized', 'style', 'popupmenu', 'string', bufferSizes, 'tag', 'bufferSize', 'position', [0.58 0.75 0.4 0.1], 'value', find(strcmp(bufferSizes, num2str(OAE(SEQUENCE).bufferSize))), 'callback', @oae_update_options, 'userdata', str2double(bufferSizes));

uicontrol(panel, 'Units', 'normalized', 'style', 'text', 'string', 'Data directory', 'position', [0.01 0.65 0.5 0.05]);
uicontrol(panel, 'Units', 'normalized', 'style', 'edit', 'tag', 'datapath', 'position', [0.01 0.55 0.98 0.07], 'string', OAE(SEQUENCE).datapath, 'enable', 'inactive');
uicontrol(panel, 'Units', 'normalized', 'style', 'pushbutton', 'tag', 'datapath', 'position', [0.01 0.47 0.4 0.07], 'string', 'Change','callback', @oae_update_options);
