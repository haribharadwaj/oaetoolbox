function h = oae_waitbar(varargin)

string = 'Processing...';
name = 'Please wait...';
for i=1:2:length(varargin),
    switch varargin{i},
        case 'string'
            string = varargin{i+1};
        case 'name'
            name = varargin{i+1};
    end
end

scrsz = get(0, 'MonitorPositions');
if size(scrsz,1) > 1,
    scrsz = scrsz(1,:);
end
BGCOLOR = get(0, 'DefaultUicontrolBackgroundColor');
h = figure('units', 'pixels', 'position', [scrsz(3)/2 scrsz(4)/2 280 60],'color', BGCOLOR, 'Toolbar', 'none','Menubar', 'none', 'name', name, 'resize', 'off', 'numbertitle', 'off');
uicontrol(h, 'style', 'text', 'units', 'pixels', 'string', string, 'position', [0 30 280 20], 'fontsize', 12, 'horizontalalignment', 'center');
% add progress bar
[jcomp, hcontainer] = javacomponent('javax.swing.JProgressBar');
set(jcomp, 'Minimum',0, 'Maximum', 300, 'Value',299);
set(hcontainer, 'position', [40 10 200 20], 'backgroundcolor', BGCOLOR);
set(h, 'visible', 'on');