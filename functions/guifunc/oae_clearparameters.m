function oae_clearparameters(module)

if iscell(module),
    module = module{:};
end
str = sprintf('%s.parameters', module);
h = findobj('tag', str);
children = get(h, 'children');
if ~isempty(children),
    delete(children);
    pause(0.5);
end
% clear current module from userdata
set(get(h, 'parent'), 'userdata', []);
refresh;
drawnow;

end