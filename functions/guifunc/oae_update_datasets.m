function oae_update_datasets(OAE, SEQUENCE)

obj = OAE(SEQUENCE);
handle = findobj('tag', 'datasets');
numDatasets = length(obj.datasets);
datasets = [];
for i=1:numDatasets,
    if isempty(obj.datasets(i).label),
        datasets(end+1).name = sprintf('(%i) cleaned dataset', length(datasets)+1);
    else
        datasets(end+1).name = sprintf('(%i) %s', length(datasets)+1, obj.datasets(i).label);
    end
    datasets(end).value = i;
end

if ~isempty(datasets),
    % get the current dataset
    % get the current dataset
    if ~isempty(OAE(SEQUENCE).currentIndex),
        current_dataset = OAE(SEQUENCE).currentIndex(1);
    else
        current_dataset = length(datasets);
    end
    set(handle, 'String', {datasets(:).name}, 'value', current_dataset);
else
    set(handle, 'String', {' '}, 'value', 1);
end

if ~isempty(datasets),
    set(handle, 'enable', 'on');    % enable the dropdown
    oae_toggle_ui(findobj('tag', 'analysis'), 1);    % also make sure analysis module set is now enabled
else
    set(handle, 'enable', 'off');
    oae_toggle_ui(findobj('tag', 'analysis'), 0);    % also make sure analysis module set is now enabled
end

end