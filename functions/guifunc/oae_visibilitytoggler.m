function oae_visibilitytoggler(panel, tag)
% toggles visibility of an element given a tag and panel. 

handle = findobj('tag', panel);
h = findobj(handle, 'tag', tag);
v = get(h, 'visible');

if strcmp(lower(v),'off');
    set(h, 'visible', 'on')
elseif strcmp(lower(v),'on')
    set(h,'visible', 'off')
end
