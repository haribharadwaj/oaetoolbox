function oae_update_analyses(OAE, SEQUENCE)

obj = OAE(SEQUENCE);
handle = findobj('tag', 'analyses');
numAnalyses = 0;
current_analysis_index = 0;

% check if current dataset is valid, otherwise set to last dataset
if (obj.current_dataset > length(obj.datasets))
    obj.current_dataset = length(obj.datasets);
end

if ~isempty(obj.datasets) && ~isempty(obj.current_dataset),
    numAnalyses = obj.datasets(obj.current_dataset).analysisIndex-1;
    if ~isempty(obj.currentIndex) && length(obj.currentIndex) > 1,
        current_analysis_index = obj.currentIndex(2);
    end
end

analyses = []; counter = 1;
for i=1:numAnalyses,
    if (~isempty(obj.datasets(obj.current_dataset).analysisObj(i).Label)),
        analyses(counter).name = sprintf('(%i) %s', counter, obj.datasets(obj.current_dataset).analysisObj(i).Label);
        analyses(counter).value = i;
        counter = counter +1;
    end
end

if ~isempty(analyses),
    set(handle, 'String', {analyses(:).name}, 'value', current_analysis_index);
else
    set(handle, 'String', {' '}, 'value', 1);
    % clear the results figure windows
    axis1 = findobj('tag', 'axis_1');
    axis2 = findobj('tag', 'axis_2');
    cla(axis1); cla(axis2);
    h=findobj('tag','legend');
    delete(h);
    h=findobj('tag', 'components');
    delete(h);
    
end

if ~isempty(analyses),
    set(handle, 'enable', 'on');
    oae_plot(OAE, SEQUENCE);
else
    set(handle, 'enable', 'off');
end

end