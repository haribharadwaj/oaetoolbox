function h = oae_icon(parent, iconType, varargin)

position = [0 0]; cmd = '';
for i=1:2:length(varargin),
    switch varargin{i},
        case {'position'}
            position = varargin{i+1};
            set(parent, 'units', 'pixels');
            ppos = get(parent, 'position');
            position = [position(1).*ppos(3) position(2).*ppos(4)];
        case {'callback'}
            cmd = varargin{i+1};
    end
end

%h = uicontrol(parent, 'style', 'pushbutton', 'units', 'pixels', 'position', [position(1) position(2) 17 17], 'callback', cmd);
g = get(0, 'DefaultUipanelBackgroundColor');
pth = oae_getsystempath();

switch iconType
    case 'delete'
        %icon = iconize(imread([pth 'icons' filesep 'delete.png']));
        icon = [pth 'icons' filesep 'delete_sm.gif'];
        [jButton c] = javacomponent(javax.swing.JButton(''),[position(1) position(2) 20 20], parent);
        jButton.setIcon(javax.swing.ImageIcon(icon));
        jButton.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton.setBorder([]);
        bgcolor = num2cell(get(gcf, 'Color'));
        jButton.setBackground(java.awt.Color(bgcolor{:}));
        jButton.setCursor(java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton.setToolTipText('delete sequence');
        set(jButton, 'ActionPerformedCallback', cmd);
        set(c,'Units', 'normalized');
    case 'zoom_in'
        icon = [pth 'icons' filesep 'zoom_in.png'];
        [jButton c] = javacomponent(javax.swing.JButton(''),[position(1) position(2) 16 18], parent);
        jButton.setIcon(javax.swing.ImageIcon(icon));
        jButton.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton.setBorder([]);
        bgcolor = num2cell(get(gcf, 'Color'));
        jButton.setBackground(java.awt.Color(bgcolor{:}));
        jButton.setCursor(java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton.setToolTipText('Zoom');
        set(jButton, 'ActionPerformedCallback', cmd);
        set(c,'Units', 'normalized');
    case 'zoom_out'
        icon = [pth 'icons' filesep 'zoom_out.png'];
        [jButton c] = javacomponent(javax.swing.JButton(''),[position(1) position(2) 18 18], parent);
        jButton.setIcon(javax.swing.ImageIcon(icon));
        jButton.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton.setBorder([]);
        bgcolor = num2cell(get(gcf, 'Color'));
        jButton.setBackground(java.awt.Color(bgcolor{:}));
        jButton.setCursor(java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton.setToolTipText('Reset Zoom');
        set(jButton, 'ActionPerformedCallback', cmd);
        set(c,'Units', 'normalized'); 
    case 'dataselect'
        icon = [pth 'icons' filesep 'select.png'];
        [jButton c] = javacomponent(javax.swing.JButton(''),[position(1) position(2) 18 18], parent);
        jButton.setIcon(javax.swing.ImageIcon(icon));
        jButton.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton.setBorder([]);
        bgcolor = num2cell(get(gcf, 'Color'));
        jButton.setBackground(java.awt.Color(bgcolor{:}));
        jButton.setCursor(java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton.setToolTipText('data point selection');
        set(jButton, 'ActionPerformedCallback', cmd);
        set(c,'Units', 'normalized'); 
        
end

set(parent, 'units', 'normalized');        
        
% ptmp = icon(:,:,1);
% ptmp(ptmp==255) = g(1).*255;
% icon(:,:,1) = ptmp;
% 
% ptmp = icon(:,:,2);
% ptmp(ptmp==255) = g(2).*255;
% icon(:,:,2) = ptmp;
% 
% ptmp = icon(:,:,3);
% ptmp(ptmp==255) = g(3).*255;
% icon(:,:,3) = ptmp;
% 
% 
% set(h, 'cdata', icon);


end




function out = iconize(a)

% Find the size of the acquired image and determine how much data will need
% to be lost in order to form a 18x18 icon
[r,c,d] = size(a);
r_skip = ceil(r/18);
c_skip = ceil(c/18);

% Create the 18x18 icon (RGB data)
out = a(1:r_skip:end,1:c_skip:end,:);

end