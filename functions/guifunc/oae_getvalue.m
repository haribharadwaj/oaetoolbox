function value = oae_getvalue(panel, tag, varargin)

datatype = 'int';
if ~isempty(varargin),
    datatype = varargin{1};
end
%vv = version( '-release')
if  isobject(panel) % strcmp(vv, '(R2015a)')
   handle = panel.Children;
else
    handle = findobj('tag', panel);
end
h = findobj(handle, 'tag', tag);
type = get(h, 'style');
if ~isempty(type),
    switch type,
        case {'edit', 'text'},
            value = get(h, 'string');
        case {'popupmenu'}
            ind = get(h, 'Value');
            vals = get(h, 'string');
            if isempty(ind),
                ind = 1;
            end
            if ischar(vals),
                value = vals;
            else
                value = vals{ind};
            end
        case {'checkbox', 'pushbutton'}
            value = get(h, 'value');
            datatype = '';
    end
    
    switch datatype
        case 'int'
            % check if multi-element str 9e.g. spaces)
            value = str2double(value);
    end
else
    value = [];
end