function handle = oae_uicomponent(varargin)
%
% handle = oae_uicomponent(varargin)
% add a uicontrol
% Added functionality for togglebuttons Joshua J. Hajicek 9/27/12

if nargin < 4,
    error('InputChk: Wrong number of input arguments');
end

target = [];
if mod(nargin,2),
    target = varargin{1};
    varargin = varargin(2:end);
end

% defaults
style = '';

% validate some of the uicontrol parameters
validStyles = {'text', 'edit', 'popupmenu', 'pushbutton', 'checkbox', 'radiobutton', 'listbox', 'togglebutton'};
checkStyles = @(x) any(validatestring(x,validStyles));
for i=1:2:length(varargin),
    switch varargin{i},
        case 'style'
            if checkStyles(varargin{i+1}),
                switch varargin{i+1},
                    case {'text', 'checkbox', 'radiobutton'},
                        bgcolor = get(0, 'DefaultUipanelBackgroundColor');
                    case {'edit' 'popupmenu'}
                        bgcolor = get(0, 'DefaultUipanelBackgroundColor');
                    otherwise
                        bgcolor = get(0, 'DefaultUIControlBackgroundColor');
                end
            else
                error('invalid uicontrol style.');
            end
        case 'backgroundcolor'
            if (isnumeric(varargin{i+1}) && (length(varargin{i+1}) == 3))    
                bgcolor = varargin{i+1};
            else
                error('invalid uicontrol background color.');
            end
    end    
end

if ~isempty(target),
    varargin = {target varargin{:}};
end


handle = uicontrol(varargin{:});
%set(handle, 'BackgroundColor', bgcolor);


end