function oae_update_options(hObject, ~)

OAE = evalin('caller', 'OAE');
SEQUENCE = evalin('caller', 'SEQUENCE');

style = get(hObject, 'style');
name = get(hObject, 'tag');
switch style,
    case 'popupmenu'
        userdata = get(hObject, 'userdata');
        string = get(hObject, 'string');
        value = get(hObject, 'value');
        if ~isempty(userdata),
            new_value = userdata(value);
        else
            new_value = string{value};
        end
    case 'pushbutton',
        % this is a special scenario for datapath, we need to get a folder
        % location
        new_value = uigetdir(pwd, 'Pick a new data directory');
        if isnumeric(new_value) && new_value == 0,
            return;
        else
            %need to update accompanying UI text box
            textfield = findobj(get(hObject, 'parent'), 'tag', name, '-and', 'style', 'edit');
            set(textfield, 'string', new_value);
        end
end


% % determine variable type 
% data_type = class(eval([OAE(SEQUENCE) '.' name]));
% switch data_type,
%     case {'double', 'single', 'int'}
%         eval([OAE(SEQUENCE) '.' name '=' str2double(new_value)]);
%     case {'char'}
%         eval([OAE(SEQUENCE) '.' name '=' new_value]);
% end

for i=1:SEQUENCE,
    OAE(i).(name) = new_value;
end

%ok, let's put them back into the workspace
assignin('base', 'OAE', OAE);


% save/update the default object
OAE(SEQUENCE).saveController;