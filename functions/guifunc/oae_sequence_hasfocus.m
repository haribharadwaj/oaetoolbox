function oae_sequence_hasfocus(jEditbox, event, focus)
% oae_sequence_hasfocus(jEditbox, event, focus)
%   updates background color when editbox receives/loses focus
%   jEditbox - java swing component
%   event - unused
%   focus - 0 or 1 (true or false)
if focus,
    jEditbox.Border = [];
    jEditbox.setBackground(java.awt.Color(1, 1, 1));
else
    c = get(0,'DefaultFigureColor');
    jEditbox.Border = [];
    jEditbox.setBackground(java.awt.Color(c(1), c(2), c(3)-0.001));
end