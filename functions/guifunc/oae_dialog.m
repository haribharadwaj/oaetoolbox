function oae_dialog(msg, type)
% oae_dialogs
%
% 04/09/18, SH add the function TODO: add different dialog types as needed

if ~exist('type','var')||isempty(type),
    type = 0;
    sz = [250 80];
end

h = findobj('tag', 'OAEToolbox');
if isempty(h),
    h = findobj('tag', 'analysis.window');
end
pos = get(h,'position');
d = dialog('position', [pos(1)+pos(3)/2-sz(1)/2 pos(2)+pos(4)/2-sz(2)/2 sz],'Name','OTB Message', 'units', 'pixels');
drawnow;
set(d, 'units', 'normalized');
txt = uicontrol('Parent',d,...
    'Style','text',...
    'Position',[0.1 0.2 0.8 0.6],...
    'String',msg);

btn = uicontrol('Parent',d,...
    'Position',[0.4 0.1 0.2 0.2],...
    'String','Close',...
    'Callback','delete(gcf)');