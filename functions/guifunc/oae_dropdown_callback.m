function oae_dropdown_callback(hObject, eventdata, handles)

uids = get(hObject,'UserData');
index = get(hObject,'Value')-1;
modules = oae_loaddefaults();


if index,
    % find protocol based on uid.
    module_uid = str2num(uids{index});
    module_idx = find([modules.uid] == module_uid);
    cmd = modules(module_idx).cmd;

    
    % check if protocols exists. If so, add what we got.
    handle = get(hObject, 'parent'); % protocols is on the parent level
    % set block to current active module uid
    block_handle = get(hObject, 'parent');
    current_module_uid = get(block_handle, 'userdata');
    
    % make sure not loading the same module. Also fixes bug on OSX in which
    % using keyboard causes the previous module and next selected module to
    % both fire the callback
    if current_module_uid ==  modules(module_idx).uid,
        return;
    end

    % save the current state of the previously active module
    block = get(get(hObject, 'parent'), 'tag');
    if ~sum(strcmpi(block, {'paradigm' 'artifactrejection' 'analysis'})), % don't auto-save paradigm or artifactrejection modules
        % check if object has changed
        is_changed = evalin('base', 'OAE(SEQUENCE).checkState');
        if is_changed,
            % should we save the current settings?
            options.Interpreter = 'none';
            options.Default = 'No';
            set(0,'DefaultUicontrolUnits','Pixels'); %Bug fix: questdlg does not show properly when default units are 'normalized'. So override temporarily.
            btn = questdlg('Do you want to apply the current settings?', 'Save settings...', 'Yes', 'No', options);
            switch btn,
                case 'Yes',
                    % save last used settings, TODO: this is the same code as oae_savedefaults. Need to create more generic savedefaults
                    block = get(get(hObject, 'parent'), 'tag');
                    module = findobj('tag', [ block '.parameters']);
                    params = flipud(get(module, 'children'));    % REMARK: Need to flip the paramters u/d to resort them in desceinding order.
                    parameters = [];
                    counter = 1;
                    for i=1:length(params),
                        tag = get(params(i), 'tag');
                        style = [];
                        try,
                            style = get(params(i), 'style');
                        catch
                        end
                        % check if figure object.
                        % if so, skip
                        if (0),
                            continue;
                        end
                        if ~isempty(tag) && isempty(regexp(tag, 'protocols')) && ~strcmp(style, 'pushbutton'),
                            %         if strcmp(style, 'checkbox'),
                            %             val = oae_getvalue(module, tag, 'value');
                            %         else
                            %             val = oae_getvalue(module, tag, 'string'); %%%%
                            %         end
                            
                            userdata = get(findobj('tag', tag), 'userdata');
                            parameters(counter).tag = tag;
                            parameters(counter).value = get(findobj('tag', tag), 'value'); %oae_getvalue(module, tag, 'value');
                            parameters(counter).string = get(findobj('tag', tag), 'string'); %oae_getvalue(module, tag, 'string');
                            parameters(counter).userdata = userdata;
                            counter = counter +1;
                        end
                    end
                    previous_module_idx = find([modules.uid] == current_module_uid);
                    modules(previous_module_idx).parameters = parameters;
                    pth = oae_getuserdefaultspath;
                    defaults_file = [pth oae_getuid '.defaults.mat'];
                    save(defaults_file, 'modules');
                    assignin('base', 'modules', modules);
                otherwise,
                    try,
                        %revert to previous controller settings
                        evalin('base', 'OAE(SEQUENCE) = OAE(SEQUENCE).revertState;');
                    catch
                    end
            end
            set(0,'DefaultUicontrolUnits','Normalized');
        end
    end
    
    evalin('base', cmd ); % runs the setter
    set(block_handle, 'userdata', modules(module_idx).uid); % set active module in userdata
    protocols_handle = findobj(handle, 'tag', 'protocols');
    if ~isempty(protocols_handle),
        % load protocols
        oae_loadprotocols(protocols_handle, modules(module_idx).protocols);
    end
    
    % set default module values (e.g. last used) if they exist
    oae_load_module_defaults(handle, modules(module_idx));
    % save current state of the controller (so that we can revert if we need to).
    evalin('base', 'OAE(SEQUENCE).saveState;');
else
    module = get(hObject,'tag');
    module = explode(module, '.');
    module = module(1);
    oae_clearparameters(module);
end