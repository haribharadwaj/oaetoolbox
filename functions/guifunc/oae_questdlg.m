function response = oae_questdlg(quest_string, selections)
% response = oae_questdlg(quest_string, selections)
%
% ask a question convience wrapper
% returns index of selected option

response = [];

options.Interpreter = 'none';
options.Default = selections{1};
set(0,'DefaultUicontrolUnits','Pixels');
btn = questdlg(quest_string, 'Next...', selections{:}, options);
set(0,'DefaultUicontrolUnits','Normalized');
response = find(strcmp(selections, btn));