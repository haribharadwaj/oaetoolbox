function [OAE i] = oae_update_analysis_sequence(OAE, SEQUENCE, i)
%
% update the sequence for analysis
% This function mainly clears the gui and resets results
% 10/2/12 - Added T to displayed params.  Joshua J. Hajicek

if (i ~= SEQUENCE),    
    oae_plot([],[]);
    %OAE(i).setActiveDataset(); % added JJH 03-01-2017
    
    
    %reload modules for visible elements
    handle = findobj('tag', 'artifactrejection.modules');
    set(handle, 'value', 1);
    oae_clearparameters('artifactrejection');
    
    handle = findobj('tag', 'analysis.modules');
    set(handle, 'value', 1);
    oae_clearparameters('analysis');
    
    oae_update_datasets(OAE, i);
    oae_update_analyses(OAE, i);
    
end

% update sequence info
% --------------------
seq_info = findobj('tag', 'sequence.info');
str = [];

str = [str sprintf('Paradigm: %s,     ', upper(OAE(i).paradigmLabel))];
str = [str sprintf('Ear: %s ', OAE(i).ear)];
str = [str sprintf('\n')];
str = [str sprintf('Patient ID: %s ', OAE(i).patientID)];
str = [str sprintf('\n')];
str = [str sprintf('DOB: %s ', OAE(i).DOB)];
str = [str sprintf('\n')];
str = [str sprintf('Sex: %s ', OAE(i).sex)];
str = [str sprintf('\n')];
switch OAE(i).paradigmType,
    case {'logsweep', 'sweep'}
        for j=1:OAE(i).nBuffer,
            str = [str sprintf('Buffer #%i:\n', j)];
            for k=1:size(OAE(i).fstart,1) % SF size = 1 DP size = 2
                if ~isnan(OAE(i).fstart(k)) && ~isempty(OAE(i).fend),
                    str = [str sprintf('  f%i: %i - %i,    ', k, OAE(i).fstart(k,j), OAE(i).fend(k,j))];
                    str = [ str sprintf( 'L%i: %i\n', k, OAE(i).L(k,j) ) ];
                end
            end
        end
        str = [str sprintf( 'T (s): %.1f,', OAE(i).T ) ];
        str = [str sprintf( '  No. Sweeps: %i,', OAE(i).nRepetitions ) ];
        str = [str sprintf('  Fs: %i Hz\n', OAE(i).fs)];
    case {'tone', 'discrete'}
        str = [str sprintf( 'T (s): %.1f,', OAE(i).T ) ];
        str = [str sprintf( '  No. Points: %i,', OAE(i).nBuffer ) ];
        str = [str sprintf('  FS: %i Hz\n', OAE(i).fs)];
end
str = [str sprintf('\n')];
str = [str sprintf('Notes: %s ', OAE(i).notes)];
set(seq_info, 'string', str);
% Update datasets when sequence changes
% if ~isempty(OAE(SEQUENCE).current_dataset)
% OAE(SEQUENCE).current_dataset = 1; 
% OAE(SEQUENCE).currentIndex = [i 1]; 
% OAE(SEQUENCE).setActiveDataset(); 
% oae_update_analyses(OAE, SEQUENCE);

end
