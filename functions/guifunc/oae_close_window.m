function oae_close_window(hObject, event)

type = get(hObject, 'type');
while ~strcmp(lower(type), 'figure'),
    hObject = get(hObject, 'parent');
    type = get(hObject, 'type');
end

t = get(hObject, 'tag');
if strcmp(t, 'OAEtoolbox')
    disp('cannot close main window with this function')
elseif strcmp(t, 'popout'),
    % check if object has changed
    is_changed = evalin('base', 'OAE(SEQUENCE).checkState');
    if is_changed,    
        % should we save the current settings?
        options.Interpreter = 'none';
        options.Default = 'No';
        set(0,'DefaultUicontrolUnits','Pixels'); %Bug fix: questdlg does not show properly when default units are 'normalized'. So override temporarily.
        btn = questdlg('Do you want to apply the current settings?', 'Save settings...', 'Yes', 'No', options);
        switch btn,
            case 'Yes',
                block = findobj(event.Source, '-regexp', 'tag', 'save_button');
                oae_savedefaults(block, event);
                evalin('base', 'OAE(SEQUENCE) = OAE(SEQUENCE).saveState;'); % resave current state after save
            otherwise,
                try,
                    %revert to previous controller settings
                    evalin('base', 'OAE(SEQUENCE) = OAE(SEQUENCE).revertState;');
                catch
                end
        end
        set(0,'DefaultUicontrolUnits','Normalized');
    end
    delete(hObject)
else
    delete(hObject)
end