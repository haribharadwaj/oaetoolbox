function oae_load_module_defaults(handle, module)
%
% load module default values
%

if isfield(module, 'parameters'),
    if ~isempty(module.parameters),
        defaults = module.parameters;
        for j=1:length(defaults),
            h = findobj(handle, 'tag', defaults(j).tag);
            if ~isempty(h),
                string          = defaults(j).string;
                value           = defaults(j).value;
                userdata        = defaults(j).userdata;
                previous_string = get(h, 'string');
                previous_value  = get(h, 'value'); 
                switch get(h, 'style'),
                    case 'popupmenu'
                        val = string{value};
                        str = get(h, 'string');
                        idx = find(strcmp(str, val));
                        if ~isempty(idx),
                            set(h, 'value', idx);
                        else
                            set(h, 'value', defaults(j).value, 'string', defaults(j).string);
                        end
                    otherwise
                        set(h, 'value', defaults(j).value, 'string', defaults(j).string);
                end
                % call the setter if callback exists
                if ~isempty(get(h, 'callback')),
                    try
                        evalin('base', get(h, 'callback'));
                    catch
                        % set the value back to original state, then break
                        set(h, 'value', previous_value, 'string', previous_string);
                        break;
                    end
                end
            end
        end
    end
end