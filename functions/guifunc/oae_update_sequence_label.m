function oae_update_sequence_label(hObject, event, SEQUENCE)
% oae_update_sequence_label(hObject, event, SEQUENCE)
%       Sequence label callback
label = get(hObject, 'string');
OAE = evalin('caller', 'OAE');
OAE(SEQUENCE).label = label;
oae_sequence_hasfocus(findjobj(hObject), [], 0);
assignin('base', 'OAE', OAE);
