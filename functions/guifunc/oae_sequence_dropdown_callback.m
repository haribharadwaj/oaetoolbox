function oae_sequence_dropdown_callback(hObject, eventdata, handles)
% If sequence changes we also need to update the cleaned datasets and
% analysis sets so they are accurately updated in the cleaned and analysis
% dropdowns/sequences. Joshua Hajicek - GC CUNY 3/1/2017

cmd = sprintf('[OAE SEQUENCE] = oae_update_analysis_sequence(OAE, SEQUENCE, %i);', get(hObject,'Value'));
evalin('base', cmd);

