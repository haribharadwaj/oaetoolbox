function oae_refresh_protocol(hObject, eventdata)

val = get(hObject,'Value')-1;
protocols = get(hObject,'userdata');
menu = get(hObject, 'tag');

cmd = [];

% save new protocol
% -----------------
if val,
    
    protocol = protocols(val);
    handle = get(hObject, 'Parent');
    
    fields = fieldnames(protocol);
    for j=1:length(fields),
        value = getfield(protocol, fields{j});
        h = findobj('tag', fields{j});
        if ~isempty(h),
            style = get(h, 'style');
            switch style,
                case 'popupmenu'
                    val = find(strcmp(get(h, 'string'), value));
                    if isempty(val), val = 1; end
                    set(h, 'value', val);
                otherwise
                    set(h, 'string', value);
            end
            if isempty(cmd),
                cmd = get(h, 'callback');
            end
        end
        
        if j == length(fields),
            hgfeval(get(h,'Callback'));
        end
    end
    
    % evaluate protocol's callback
    % ----------------------------
    if ~isempty(cmd),
        evalin('base', cmd);
    end
end

end
