function handle = oae_addmenuitem(parent, label, callback, enable, varargin)

accelerator = '';
checked = 'off';
tag = '';
for i=1:2:length(varargin),
    switch lower(varargin{i}),
        case 'label'
        case 'accelerator'
            accelerator = varargin{i+1};
        case 'checked'
            checked = varargin{i+1}; 
        case 'tag'
            tag = varargin{i+1};    
    end
end

handle = uimenu('Parent', parent, ...
    'Label', label, ...
    'Callback', callback, ...
    'enable', enable, 'Accelerator', accelerator, 'Checked', checked, 'Tag', tag);



