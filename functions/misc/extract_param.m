function val = extract_param(str, param)
% extract paramater from string
% Copyright (c) 2009, Simon Henin
if nargin ~= 2,
    error('myApp:argChk', 'Wrong number of input arguments');
end

val = regexprep(regexp(str, [param '=[0-9]{1,}'], 'match', 'once'), [param '='], '');
if ischar(val), val = str2num(val); end
if ~isnumeric(val), val = str2num(val{:}); end