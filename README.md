OAEToolbox version 3.0

OTB 3.0 represents an overhaul of the toolbox targeting MATLAB versions > 2014b, leveraging the improved graphics system to improve realtime display. In addition, this release improves defaults/protocol database handling, along with numerous stability improvements.

