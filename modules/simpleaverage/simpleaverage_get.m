function OAE = simpleaverage_get( OAE, SEQUENCE )
%% callback function for ui elements (e.g. to update the controls & update
%% the sequence OBJECT
% 10/2/12 v 1.7.0 added dummy tag to resolve a paramaters issue - JJH
% 10/2/12 added call to simple average since there are no params to invoke
% the setter. Joshua J. Hajicek
cmd = ['OAE = simpleaverage_set( OAE, SEQUENCE );'];
uigeom = {1.8 1.8};
uilist = { ...
    {'style'  'text'      'string'    'TEMOPORAL AVERAGING PARAMETERS'} ...
    {'style'  'text'      'string'    'No available parameters', 'tag', 'dummy'} ...

  
 };

oae_addparameters('artifactrejection.parameters', uilist, uigeom, 'topborder', 0.01, 'fontsize', 10);
OAE = simpleaverage_set( OAE, SEQUENCE );


end