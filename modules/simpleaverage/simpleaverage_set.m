function OAE = simpleaverage_set( OAE, SEQUENCE )

handle = 'artifactrejection.parameters'; 
%simpleaverageBypassFlag = oae_getvalue(handle, 'checkboxBypass');

N = 1;
while (N <= SEQUENCE)
    OAE(N).artifactrejectionCallback    = 'simpleaverage_callback';
    OAE(N).artifactrejectionLabel       = 'Temporal Average';
    N = N + 1;
end

end