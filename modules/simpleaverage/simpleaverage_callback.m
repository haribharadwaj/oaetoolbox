function obj = simpleaverage_callback( obj )
% 10/2/2012 v1.7.0 - modified to handle data stored in individual files - Joshua
% J. Hajicek

n = size( obj.getData( 1, obj.Buffer, obj.Condition ), 2 ); 
tmp = zeros( 1, n ); 
tmp = double(tmp);
nf  = tmp;
sgn = 1;
b = fir1(100, 100./obj.fs*2, 'high'); % hp filter
for r = 1:obj.nRepetitions
    data = obj.getData( r, obj.Buffer, obj.Condition );
    data = filtfilt(b,1, data);
    tmp = data+tmp;
    nf  = nf+sgn.*data;
    sgn = -sgn;
end
% check if even number of repititions, if not add/subtract last buffer back
% off
if mod(r ,2) ~= 0,
    % remove the last file from the noise floor estimate
    nf = nf+sgn*data; % since sign has already been flipped, this will subtract the last data back off
    nf = nf/(obj.nRepetitions-1);
else
    nf = nf/obj.nRepetitions;
end
avg = tmp/obj.nRepetitions;
obj = obj.saveAverage(avg);
obj = obj.saveNoiseFloor(nf);

if ~isdeployed && 0,
    filename = sprintf('%s-%s-systemdelay-%i.snd', date, now, obj.systemDelay);
    sndwrite(avg, obj.fs, 32, 'floatingpoint', '', [oae_getsystempath filename]);
end
disp('temporal average completed');
end


