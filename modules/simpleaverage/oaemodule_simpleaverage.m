function [vers block label cmd] = oaemodule_simpleaverage(fig)

vers  = 'simpleaverage 1.0';
label = 'Temporal Average';
cmd   = ['OAE = simpleaverage_get(OAE, SEQUENCE);'];
block = 'artifactrejection';   % the name of the UIPANEL/ or as we call blocks

end