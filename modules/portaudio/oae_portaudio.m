function obj = oae_portaudio(obj)
% 9/28/2012  changed the way recordings are saved, they are now saved to a
% file using the method obj.saveRecordingToFile - Joshua J. Hajicek
%
% 10/5/12 - modified to fix bug that caused inconsistent buffer sizes to be returned
% when using two devices. 
%
% 03/27/14 - Simon Henin
% fix issue with 1-channel playback playing through 2-channels using master
% device mode.
%
y = obj.getCurrentStimulus();


% resize the data, if necessary
if size(y,2) > size(y,1),
    y = y';
end
% get I/O channels
OutputChannels = obj.OutputChannels;
OutputDevices = obj.OutputDevices;
InputChannels = obj.InputChannels;
InputDevices = obj.InputDevices;

numOutputDevices = length(unique(OutputDevices));
pageSize = obj.bufferSize;
%pageBuffer = pageSize*4;
pageBuffer = pageSize*4;  % setting to 10 to reduce buffer overflow 

% initialize devices, and fill buffers
pasound = []; inputID = [];
% display number of threads for matlab
% N = maxNumCompThreads;
% fprintf(1, 'number of comp threads', N);

try
    if ~obj.audioInit,
        clear PsychPortAudio
        PsychPortAudio('Verbosity', 1);
%         [oldyieldInterval, oldMutexEnable, lockToCore1] = PsychPortAudio('EngineTunables', 0, 0, 0);
        if ~isunix,
            Priority(2);
        end
    end
catch
    oae_error('Portaudio driver not found.');
    return;
end

for i=1:length(numOutputDevices),
    
    DeviceID = OutputDevices(i);
    index = find(OutputDevices == DeviceID);
    Out = OutputChannels(index)';
    
    % check for duplex
    if InputDevices == DeviceID,
        inputID = i;
        chanlist = [];
        for j=1:length(Out),
            chanlist = [chanlist Out(j)];
        end
        chanlist = [chanlist, InputChannels];
        if isempty(obj.audioInit) || ~obj.audioInit,
            
            % check if device is still connected
            DeviceName = obj.recordingDeviceName;
            dev = PsychPortAudio('GetDevices');
            if isempty(find(strcmp({dev.DeviceName}, DeviceName))),
                oae_error('Your configured I/O device is not connected!', 'IO Error');
                return;
            else
                % check if device ID is still the same, otherwise update
                % the object with it's new DeviceID
                idx = find(strcmp({dev.DeviceName}, DeviceName));
                if dev(idx).DeviceIndex ~= DeviceID,
                    DeviceID = dev(idx).DeviceIndex;
                    obj.OutputDevices(i)    = DeviceID;
                    obj.InputDevices(i)     = DeviceID;
                end
            end
        
            % Fix for 1-channel recordings playing back through 2-channels
            % Use master device mode (+8) and open individual channels as
            % slaves. However, need to open master with at least
            % 2-channels (e.g. one will be unused), otherwise it does not work
            % Modified 3/27/2014, Simon Henin
            PsychPortAudio('Close'); % make sure any open devices are closed
            pamaster = PsychPortAudio('Open', DeviceID, 3+8, 2, obj.fs, [max(2,length(Out)), length(InputChannels)], [], 0.005);
            % less aggressive version of PsychPortAudio
            % pamaster = PsychPortAudio('Open', DeviceID, 3+8, 0, obj.fs, [max(2,length(Out)), length(InputChannels)], [], []);
            PsychPortAudio('Start', pamaster, 0, 0, 1);
            if isunix,
                pasound(i) = PsychPortAudio('OpenSlave', pamaster, 3, [length(Out), length(InputChannels)], [OutputChannels(index); repmat(InputChannels,1, length(OutputChannels(index)))]);
            else
                % TODO: check if this works on windows
                pasound(i) = PsychPortAudio('OpenSlave', pamaster, 3, [length(Out), length(InputChannels)], [OutputChannels(index); repmat(InputChannels,1, length(OutputChannels(index)))]);
            end
        end
    else
        if isempty(obj.audioInit) || ~obj.audioInit,
            
            % check if device ID is still the same, otherwise update
            % the object with it's new DeviceID
            DeviceName = obj.recordingDeviceName;
            dev = PsychPortAudio('GetDevices');
            idx = find(strcmp({dev.DeviceName}, DeviceName));
            if isempty(find(strcmp({dev.DeviceName}, DeviceName))),
                oae_error('Your configured I/O device is not connected!', 'IO Error');
                return;
            else,
                % check if device ID is still the same, otherwise update
                % the object with it's new DeviceID
                idx = find(strcmp({dev.DeviceName}, DeviceName));
                if dev(idx).DeviceIndex ~= DeviceID,
                    DeviceID = dev(idx).DeviceIndex;
                    obj.OutputDevices(i) = DeviceID;
                end
            end
            
            pamaster = PsychPortAudio('Open', DeviceID, 1+8, 1, obj.fs, max(2, length(Out)), [], 0.005);
            PsychPortAudio('Start', pamaster, 0, 0, 1);
            pasound(i) = PsychPortAudio('OpenSlave', pamaster, 1, length(Out), Out);
        end
    end
    
    if obj.audioInit,
        pasound = obj.getExtraProp('pasound');
    end
    
%     if isempty(obj.audioInit) || ~obj.audioInit,
%         fprintf('OTB: priming audio buffer\n');
%         dummystim = eps.*ones(length(Out), 1*obj.fs);
%         PsychPortAudio('FillBuffer', pasound(i), dummystim);
%         PsychPortAudio('GetAudioData', pasound(i), 1, 1, 1);
%         PsychPortAudio('Start', pasound(i),1,0,0);
%         pause(1);
%         [audiodata absrecposition overflow cstarttime] = PsychPortAudio('GetAudioData', pasound(i));
%         pause(1);
%         [audiodata absrecposition overflow cstarttime] = PsychPortAudio('GetAudioData', pasound(i)); 
%         PsychPortAudio('Stop', pasound(i));
%     end
    if isempty(obj.audioInit) || ~obj.audioInit,
        fprintf('OTB: priming audio buffer\n');
        dummystim = eps.*ones(length(Out), 0.1*obj.fs);
        PsychPortAudio('FillBuffer', pasound(i), dummystim);
        PsychPortAudio('GetAudioData', pasound(i), 4, 0.1, 0.1);
        for j=1:3,
            PsychPortAudio('Start', pasound(i),1,0,0);
            pause(0.1); PsychPortAudio('GetAudioData', pasound(i));
            pause(0.1); PsychPortAudio('GetAudioData', pasound(i));
            PsychPortAudio('Stop', pasound(i));
        end
    end    
    stimulus = y(:,index);
    PsychPortAudio('FillBuffer', pasound(i), stimulus');
end

% % flush the output buffer
% if ~obj.audioInit,    
%     yy = zeros(100,length(index));
%     PsychPortAudio('FillBuffer', pasound(i), yy');
%     PsychPortAudio('GetAudioData', pasound(inputID), pageBuffer/obj.fs, pageSize/obj.fs, pageSize/obj.fs);
%     PsychPortAudio('Start', pasound(inputID),1,0,1);
%     PsychPortAudio('Stop', pasound(inputID), 1);
% end


input = zeros(1, max(size(y)));
numBuffers = ceil(((obj.fs*obj.T)+obj.onsetDelay + obj.offsetDelay)/pageSize);

if isempty(obj.audioInit) || ~obj.audioInit,
    
    PsychPortAudio('GetAudioData', pasound(inputID), pageBuffer/obj.fs, obj.bufferSize/obj.fs, obj.bufferSize/obj.fs);
    obj = obj.addExtraProp('pasound', pasound);
    obj.audioInit = true;
end

% playback/buffering variables
nPlaybackSamples = floor((obj.T*obj.fs))+obj.onsetDelay+obj.offsetDelay-1;
bufferTime = (obj.bufferSize - obj.bufferSize/4)/obj.fs;
endrecposition = 0;
bufferCount = 1;
%updateTimeLimit = 0.075; % threshold (in seconds) for updating the GUI
%updateTimeLimit = 0.025; %this threshold works better on Mac Pro
updateTimeLimit = 0;

[audiodata absrecposition overflow cstarttime] = PsychPortAudio('GetAudioData', pasound(inputID));  % 10/4/12 new - flush output buffer - allocates memory and then seeting it to some arbitrary size so that it allocates memory
%fprintf('time to get stim: %2.4f\n', GetSecs-t);
for i=1:length(numOutputDevices),
    status = PsychPortAudio('GetStatus', pasound(i));
    if status.XRuns > obj.underflow,
        fprintf(1, 'Warning: overflow detected on Start.\nShould not affect playback unless multiple occurences are logged within a sequence.\n.....\n');
        obj.underflow = obj.underflow + 1;   % the first overflow/underrun is becasue we let portaudio decide what size to use, becuase it doesn't know what size to return it generates this error the first time. This allows portaudio to optimize itself. 
    end
    PsychPortAudio('Start', pasound(i),1,0,0);
end

pause(bufferTime);
while endrecposition <= nPlaybackSamples,
  
    if obj.playback,
        [audiodata absrecposition overflow cstarttime] = PsychPortAudio('GetAudioData', pasound(inputID), [], obj.bufferSize/obj.fs, obj.bufferSize/obj.fs);
        
        timer = tic;
        if overflow,
            fprintf(1, 'OTB: portaudio overflow detected. buffer# %i of %i\n', bufferCount, numBuffers);
        end
        
        datalen = length(audiodata);
        endrecposition = absrecposition + datalen;  % removed the + one and made the made while loop <= from just <.
        
        % old debugging code
        %fprintf('%i - %i ', absrecposition+1, absrecposition+datalen);  % debugging - tells us the absolute position of the buffers returned
        %fprintf(1, 'length of audiodata: %i\n', datalen);
        %fprintf(1, 'current position: %i\n', absrecposition);
        
        input(absrecposition+1:absrecposition+datalen) = audiodata;
        while (endrecposition >= bufferCount*obj.rtBufferSize),
            % if we have enough time, let's run updateGUI
            % -------------------------------------------
            timeRemaining = bufferTime - toc(timer);
            if  (timeRemaining > updateTimeLimit),
                updateTimer = tic;
                obj = obj.updateGUI(input((bufferCount-1)*obj.rtBufferSize+1:(bufferCount)*obj.rtBufferSize));
                %fprintf(1, 'time to update: %f\n', toc(updateTimer));
            else
                fprintf(1, 'Not enough time to updateGUI. time left: %0.5f s\n', timeRemaining);
            end
            bufferCount = bufferCount + 1;
        end
        
        %obj = obj.updateGUI(audiodata);    
        %pause(0.1);
        %input((buffer-1)*pageSize+1:buffer*pageSize) = audiodata;
        
        %--> Is this needed? Commenting out for now because it causes an unnecessary delay
%         pTime = bufferTime - toc(timer); 
%         %fprintf(1, 'pauseTime: %2.4f\n', pTime);
%         pause(pTime);
    else
        endrecposition = Inf;
        for i=1:length(numOutputDevices),
            PsychPortAudio('Stop', pasound(i),0);
        end
        break;
    end
end
% fprintf('time2: %2.4f\n', GetSecs-t);
%fprintf('\nnPlaybackSamples: %i\n', nPlaybackSamples);  % for debugging
 %[startTime endPositionSecs xruns estStopTime] = PsychPortAudio('Stop', pahandle [,waitForEndOfPlayback=0] [, blockUntilStopped=1] [, repetitions] [, stopTime]);";
% It's the 3 return value there in the 'Stop' call
for j=1:length(pasound),
    [startTime endPositionSecs xruns estStopTime] = PsychPortAudio('Stop', pasound(i), 1, 1);
    % Drain the input buffer
    % ----------------------
    [audiodata absrecposition] = PsychPortAudio('GetAudioData', pasound(inputID));
    
    if xruns > obj.underflow,
        fprintf(1, 'Warning: %i buffer over- or underruns detected\n', xruns);
        obj.underflow = obj.underflow+1;
    end
end
%obj = obj.saveRecording(input);

% This should be done inside the controller, removed from here for now
% if ~obj.isCustom       % save recording to file if not making a custom recording - otherwise use obj.data
%     obj = obj.saveRecordingToFile(input);
% else
%     obj = obj.saveRecording(input);
% end

% save to object and/or save to file 
obj = obj.saveRecording(input);

if obj.rep == obj.nRepetitions,
    fprintf(1, 'Closing portaudio...\n');
    for i=1:length(pasound),
        PsychPortAudio('Close', pasound(i));
    end
    clear pasound;
    obj.audioInit = false;
    
    if ~isunix,
        Priority(0);
    end

end

end