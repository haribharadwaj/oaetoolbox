%
% 
% MOCR module hook
function [vers block label cmd] = oaemodule_mocr(fig)

vers='mocr 1.0';
label = 'MOCR';
cmd=  [  'OAE = mocr_get(OAE, SEQUENCE);'];
block = 'paradigm';


end