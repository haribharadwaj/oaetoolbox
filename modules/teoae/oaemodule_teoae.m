function [vers block label cmd] = oaemodule_teoae(fig)
% OAEMODULE_TEOAE
% teoae acquisition plugin
% Implements double-evoked teoae (2E) procedure
vers    ='teoae 1.0';
label   = 'TEOAE (2E)';
cmd     = ['OAE = teoae_get(OAE, SEQUENCE);'];
block   = 'paradigm';


end