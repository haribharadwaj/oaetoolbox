function obj = teoae_custom_artifactrejection( obj )
%
% custom post-processing for double-evoked TEOAE
% Collpases data into 1-buffer based on
% P_total = (P1+P2)-P3
% AR may consist of taking the residual then taking the total energy within
% the residual. Filter 0.5kHz lowpass 5.7ms group delay. Separate them into
% IQR throwing out all buffrers >2.25 times the IQR range, however, Tukey
% says that you should extend the cutoff fence by mulitplying the IQR by
% 1.5 as the raw IQR is too agressive. 1stQ-(1.5*IQR), 3thQ+(1.5*IQR).
% Some say 3 is also okay, Shawn was using someting in the middle and we'll
% go with that. 
n = size( obj.getData( 1, 1, 1 ), 2 ); 
tmp = zeros( obj.nRepetitions, n ); 
tmp = double(tmp);

for r = 1:obj.nRepetitions
    p1 = obj.getData( r, obj.Buffer, obj.Condition );
    p2 = obj.getData( r, obj.Buffer+1, obj.Condition );
    p3 = obj.getData( r, obj.Buffer+3, obj.Condition );
    
    tmp(r,:) = p1+p2-p3;
end
avg = tmp/obj.nRepetitions;
obj = obj.saveAverage(avg);

if ~isdeployed && 0,
    filename = sprintf('%s-%s-systemdelay-%i.snd', date, now, obj.systemDelay);
    sndwrite(avg, obj.fs, 32, 'floatingpoint', '', [oae_getsystempath filename]);
end
disp('teoae post-processing completed');
end