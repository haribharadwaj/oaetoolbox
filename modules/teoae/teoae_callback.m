function OAE = teoae_callback(OAE, SEQUENCE)

% %% callback function for ui elements (e.g. to update the controls & update
% %% the sequence OBJECT


handle = findobj(OAE(SEQUENCE).fig, 'tag', 'paradigm.parameters');
oae_addparameters(handle, uilist, uigeom);

%%load protocols & keep it in the userdata field
pth = which(mfilename);
pth = fileparts(pth); 
load([pth filesep 'protocols.mat']);
protocol_names = {protocols(:).label};
set(findobj(handle, 'tag', 'protocol'), 'string', protocol_names);
set(findobj(handle, 'tag', 'protocol'), 'userdata', protocols);


%load default protocol
def = find([protocols(:).default]);
protocol = protocols(def);
fields = fieldnames(protocols);
for j=1:length(fields),
    h = findobj(handle, 'tag', fields{j});
    if ~isempty(h),
    set(h, 'string', getfield(protocol, fields{j}));
    end
end
eval(cmd);