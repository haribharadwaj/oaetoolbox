function [OAE] = nseoae_set(OAE, SEQUENCE)
%
%

handle        = findobj('tag', 'analysis.parameters');
type          = oae_getvalue(handle, 'analysisType', 'string');
nfilt         = oae_getvalue(handle, 'nfilt');
nstep         = oae_getvalue(handle, 'nstep');
state         = oae_getvalue(handle, 'state');
difference    = oae_getvalue(handle, 'calcDiff');
phaseAccum    = oae_getvalue(handle, 'phaseAccum');
analysis_type = 'fs';
%check if default filter needs to be recalculated
if (state == 1 && strcmp(type, 'Separated Components')),
    nfilt         = floor((OAE(SEQUENCE).fs/2)*(OAE(SEQUENCE).T/log2(OAE(SEQUENCE).fend(1)/OAE(SEQUENCE).fstart(1))) / 2);
    nstep         = floor(nfilt*0.10);
    oae_setvalue(findobj('tag', 'nfilt'), nfilt);
    oae_setvalue(findobj('tag', 'nstep'), nstep);
    oae_setvalue(findobj('tag', 'state'), 2);
    set(findobj('tag', 'phaseAccum'), 'visible', 'on');
    analysis_type = 'sep';
elseif (state == 2 && strcmp(type, 'Fine-structure')),
    nfilt         = floor((OAE(SEQUENCE).fs/2)*(OAE(SEQUENCE).T/log2(OAE(SEQUENCE).fend(1)/OAE(SEQUENCE).fstart(1))) / 8);
    nstep         = floor(nfilt*0.10);
    oae_setvalue(findobj('tag', 'nfilt'), nfilt);
    oae_setvalue(findobj('tag', 'nstep'), nstep);
    oae_setvalue(findobj('tag', 'state'), 1);
    set(findobj('tag', 'phaseAccum'), 'visible', 'off');
    analysis_type = 'fs';
elseif strcmp(type, 'sfoae-sweep'),
    analysis_type = 'sfoae';
end

% update stored analysisObj
OAE(SEQUENCE).analysisObj.Label            = sprintf('nseoae [%s]', analysis_type);
OAE(SEQUENCE).analysisObj.analysisCallback = 'nseoae_callback';
OAE(SEQUENCE).analysisObj.analysisType     = type;
OAE(SEQUENCE).analysisObj.nfilt            = nfilt;
OAE(SEQUENCE).analysisObj.nstep            = nstep;
OAE(SEQUENCE).analysisObj.calcDifference   = logical(difference);
OAE(SEQUENCE).analysisObj.phaseAccumFlag   = ~phaseAccum;
OAE(SEQUENCE).analysisObj.paradigmType     = OAE(SEQUENCE).paradigmType;

end

    