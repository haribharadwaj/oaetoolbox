function [vers block label cmd] = oaemodule_nseoae(fig)
%
%
% NSEOAE
% Least-Squares analysis of OAEs
%
% % TODO: add more documentation

vers    = 'nseoae 1.0';
label   = 'NSEOAE (LSF)';
cmd     =  [  'OAE = nseoae_get(OAE, SEQUENCE);'];
block     = 'analysis';


end