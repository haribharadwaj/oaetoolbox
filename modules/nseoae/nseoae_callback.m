function obj = nseoae_callback(obj, oae)
%
%
% 10/4/2012 - now checking for NaNs in fstart which is what is used for any
% CAS stim.  Prevents a bug that prevented procesing.  Joshua J. Hajicek
% 01-16-2017 since saveRecording divides pref out, i removed it from the
% calculation of the magnitude and added it back to the computation of
% pascals. Joshua Hajicek - CUNY Graduate Center
% general parameters
% 04-09-17 SH. Added a check for negative valued components, which will
% cause singular valued SVD. Removes these components to keep this function
% robust.
params.fs = oae.fs;
params.T = oae.T;

%analysis parameters
params.nskip = 0; %obj.nskip;
sensitivity = obj.sensitivity;
params.nfilt = obj.nfilt;
params.nstep = obj.nstep;
gain = obj.gain;
pref = 2e-5;


fstart = oae.fstart(:,oae.Buffer, oae.Condition);
if ~isempty(oae.fend),
    fend = oae.fend(:,oae.Buffer, oae.Condition);
else
    fend = [];
end
paradigm = oae.paradigmLabel;

switch lower(paradigm),
    case {'dpoae', 'dpoae-mocr'}
        
        switch lower(obj.analysisType),
            case {'fine-structure'}
                %add primary components
                for i=1:length(fstart)-sum(isnan(fstart)),
                    params.component(i).type = 'logsweep';
                    params.component(i).f1 = fstart(i);
                    params.component(i).f2 = fend(i);
                    params.component(i).label = sprintf('f%i', i);
                end
                
                %combined analysis
                params.component(3).type = 'logsweep';
                params.component(3).f1 = 2*(fstart(1)) - fstart(2);
                params.component(3).f2 = 2*(fend(1)) - fend(2);
                params.component(3).label = '2f1-f2';
                
                %% the 3f1-2f2 component
                params.component(4).type = 'logsweep';
                params.component(4).f1 = 3*(fstart(1)) - 2*fstart(2);
                params.component(4).f2 = 3*(fend(1)) - 2*fend(2);
                params.component(4).label = '3f1-2f2';
 
                obj.labelsOn = logical([0 0 1 1]);  % [f1 f2 2f1-f2 2f2-f1])
                 
            case {'separated components'}
                %add primary components
                for i=1:length(fstart)-sum(isnan(fstart)),  % gets rid of NAN which are always last for CAS
                    params.component(i).type = 'logsweep';
                    params.component(i).f1 = fstart(i);
                    params.component(i).f2 = fend(i);
                    params.component(i).label = sprintf('f%i', i);
                end
                
                i = i+1;
                %combined analysis
                params.component(i).type = 'logsweep';
                params.component(i).f1 = 2*(fstart(1)) - fstart(2);
                params.component(i).f2 = 2*(fend(1)) - fend(2);
                params.component(i).label = 'gen';
                
                i=i+1;
                %% the 2f2-f1 component
                params.component(i).type = 'logsweep';
                params.component(i).f1 = 2*fstart(1) - fstart(2);
                params.component(i).f2 = 2*fend(1) - fend(2);
                params.component(i).label = 'refl';
                params.component(i).delay = '1./(0.033+0.00004.*x+0.000000002.*x.^2)'; % use matlab operations syntax (e.g. .*);
                obj.labelsOn = logical([0 0 1 1]);  % [f1 f2 gen refl])
        end
         
    case {'sfoae' 'sfoae-mocr'}
                % primary 
                for i=1:length(fstart)-sum(isnan(fstart)),   % index only valid stimuli, not CAS
                    params.component(i).type = 'logsweep';
                    params.component(i).f1 = fstart(i);
                    params.component(i).f2 = fend(i);
                    params.component(i).label = sprintf('f%i', i);
                end
                    
                % sfoae component
                params.component(2).type = 'logsweep';
                params.component(2).f1 = fstart(1);
                params.component(2).f2 = fend(1);
                params.component(2).label = 'sfoae';
                params.component(2).delay = '1./(0.033+0.00004.*x+0.000000002.*x.^2)'; % DEFAULT use matlab operations syntax (e.g. .*)
                obj.labelsOn = logical([1 1]);
                
    case {'discrete dpoae'}
        
        % primary 
        for i=1:length(fstart)-sum(isnan(fstart)),   % index only valid stimuli, not CAS
            params.component(i).type = 'tone';
            params.component(i).f1 = fstart(i);
            params.component(i).label = sprintf('f%i', i);
        end
        
        % add dpoae
        params.component(i+1).type = 'tone';
        params.component(i+1).f1 = 2*params.component(1).f1 - params.component(2).f1;
        params.component(i+1).label = 'dpoae';
        obj.labelsOn = logical([0 0 1]);
end


% Check for any negative valued components. This can occur
% with certain combinations of f1/f2. Particularly an issue
% for 3*f1-2*f2 component. Using this removal method to
% keep the function robust.
remove_comps = [];
for i=1:length(params),
    if (isfield(params.component(i), 'f1') && (params.component(i).f1 < 0))
        remove_comps(end+1) = i;
        oae_warning(sprintf('Component %i has negative values (f1=%4.2f). It will be removed from nseoae model.', i, params.component(i).f1));
    elseif (isfield(params.component(i), 'f2') && (params.component(i).f2 < 0))
        remove_comps(end+1) = i;
        oae_warning(sprintf('Component %i has negative values (f2=%4.2f). It will be removed from nseoae model.', i, params.component(i).f2));
    end
end
params.component(remove_comps)= [];
obj.labelsOn(remove_comps) = [];


%% run it!
avg = obj.getCurrentAverage();
nf = obj.getCurrentNoiseFloor();

%
% removing obj.phaseAccumFlag option. This should be a module specific
% variable (e.g. add a checkbox). The analysis object should be completely
% ignorant of such module specific attributes.
%
%result = nseoae(double(avg), params, obj.phaseAccumFlag); 
result  = nseoae(double(avg), params);
if ~isempty(nf),
    nf      = nseoae(double(nf), params);    
end
 
if obj.phaseAccumFlag,
    % use total_phase instead of phase. Just replace in result structure so
    % that the rest of the code runs the same
    for i=1:length(result),
        result(i).phase = result(i).total_phase;
    end
end

% for now, these values are only used for discrete tones
result(1).pa_std = [];
result(1).db_std = [];

%some additional post-processing for discrete tones
switch lower(paradigm),
    case {'discrete dpoae'}
        for i=1:length(result),
            result(i).f = result(i).f(1);
            result(i).pa_std = std(result(i).mag); 
            result(i).db_std = std( 20*log10(result(i).mag) ); % removed pref from the denominator as it's divided out by the method saveRecording
            result(i).mag = median(result(i).mag);
            if ~isempty(nf),
                result(i).rmserror = median(nf(i).mag);
            else
                result(i).rmserror = median(result(i).rmserror);
            end
            result(i).phase = median(result(i).phase.*(180/pi)); %TODO: What should we do with the phase
        end
end
obj.fs = params.fs;
obj.T  = params.T;
obj.labels = {params.component(:).label};

%residual = nseoae(result(1).residual, params);

% save the result, frequency x component matrix (eg. f = 100 x 4)
switch lower(paradigm),
    case {'discrete dpoae'}
        obj = obj.saveAnalysis([result(:).f], [result(:).mag], [result(:).pa_std], 20*log10([result(:).mag]), [result(:).db_std], [result(:).phase], [result(:).rmserror]); % took pref out, as it's divided out in the saveRecording (jjh)
    otherwise
        
        % check for nf results
        if ~isempty(nf),
            noise_floor = reshape([nf.mag], length(nf(1).mag), length(nf));
        else
            noise_floor = reshape([result.rmserror], length(result(1).rmserror), length(result));
        end
        % reshape everything into frequency x component matrix
        obj = obj.saveAnalysis(...
            reshape([result.f], length(result(1).f), length(result)), ...
            reshape([result.mag], length(result(1).mag), length(result)), ...
            reshape([result.pa_std], length(result(1).pa_std), length(result)), ...
            20*log10(reshape([result.mag], length(result(1).mag), length(result))), ...
            reshape([result.db_std], length(result(1).db_std), length(result)), ...
            reshape([result.phase].*(180/pi), length(result(1).phase), length(result)), ...
            noise_floor ...
            );
end
%obj = obj.saveAnalysis([result(:).f], [result(:).mag], 20*log10([result(:).mag]), [result(:).phase], 20*log10([residual(:).mag]));

end