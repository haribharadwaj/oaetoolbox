% soae acquisition plugin
%
%   add Spontaneous OAE (SOAE) paradigm sequences
% 
%   Setup Options:
%       Duration: length of the soae recording, in seconds (e.g. 120 = 2 minutes)
%       Repetitions: Number of recordings to perform
%
% version 1.0
% OAEToolbox, March 23rd, 2018, SH
%
function [vers block label cmd] = oaemodule_soae(fig)

vers='soae 1.0';
label = 'SOAE';
cmd=  'OAE = soae_get(OAE, SEQUENCE);';
block = 'paradigm';


end