function OAE = soae_set(OAE, SEQUENCE)
% 
% function that sets the object

%reset the sequence
OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

handle = 'paradigm.parameters'; 
T = oae_getvalue(handle, 'duration');
nRepetitions = oae_getvalue(handle, 'nRepetitions');
protocol = oae_getvalue(handle, 'protocols', 'string');


%% module's special routine to set the the object
fs = OAE(SEQUENCE).fs;
pref = 2e-5;

%check all variables properly set
if oae_validate([T nRepetitions]),
    OAE(SEQUENCE) = OAE(SEQUENCE).addSOAE(T);
    OAE(SEQUENCE).paradigmLabel = 'soae';
    OAE(SEQUENCE).T = T;
    OAE(SEQUENCE).protocol = protocol;
    OAE(SEQUENCE).nRepetitions = nRepetitions;
    OAE(SEQUENCE).paradigmType = 'soae';
    
    % Override any realtime analysis, and artifact rejection (averaging).
    OAE(SEQUENCE).setProp('displayRT', false);
    OAE(SEQUENCE).artifactrejectionCallback = [];
end