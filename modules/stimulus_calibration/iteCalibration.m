function obj = iteCalibration( obj )
%
% obj = iteCalibration( obj ) - In the ear (ITE) Calibration
%
% 03/16/2017 - JJH, City University of New York
% Updated to remove the first 4 sweeps (the first 3 is probably all that's
% needed to be removed, but I'm not sure this intinal portaudio
% initialization thing is system dependant or not).
% 03/16/17 - SH, added padding = systemDelay to ensure proper recording
% 03/18/17 - added some nicer graphics handling
% 06/18/17 - SH, improved smoothing of inverse filter and added a 10dB
% limit on corrections to avoid output clipping


%% Generate the chirp (Shawn's code)
% TODO: use oae_chirp system function instead of this. But keeping it for
% now since we are still debugging.


% Limits (no filtering outside these limits)
flim = obj.getProp('calibrationFreqLimits');
if isempty(flim),
    oae_error('Calibration Frequency Limits are empty. Please update them in the stimulus calibration module');
    return;
end
fmin = flim(1);
fmax = flim(2);        
        
fs = obj.fs;
len = .05;
nfft = round(len* fs);  % try making it fs/64 or fs/128
f = [50,fmax+fmax*0.1]; % frequency minimum and maximum (Hz)
stepSize = (f(2)-f(1))/(nfft-1); % frequency step size (Hz)
F = (f(1):stepSize:f(end))';
rampN = round(nfft * len);
phi = 0;
for jj = 1:nfft
    p(jj,1) = cos(phi);
    phi = phi + ((2*pi) / (1/F(jj) * fs));
end
rampOn = hann(rampN*2);
rampOn = rampOn(1:rampN);
rampOff = hann(rampN*2);
rampOff = rampOff(1:rampN);
p(1:rampN) = p(1:rampN) .* rampOn;
p = flipud(p);
p(1:rampN) = p(1:rampN) .* rampOff;
p = flipud(p);
segmt = p * .95;
padLen = 0.001;
padN = round(padLen * fs);
pad = zeros(padN,1);
Output = [pad;segmt;pad];

% add additional padding at the end of the signal to account for systemDelay
Output = [Output; zeros(obj.systemDelay, size(Output,2))];

% scale down the output so that it can be filtered without clipping
Output = Output*0.05; % give us some headroom

%% run calibration routine through each output
nReps           = 68;
unCalibrated    = 0; %needs to perform uncalibrated recording
doPlot          = 1;

scrsz = get(0,'ScreenSize');
sz = [800 350];



% configure a new calibration window
h = findobj('tag', 'ITECal');
if isempty(h),
    resp = figure('position', [scrsz(3)/2-sz(1)/2 scrsz(4)/2-sz(2)/2 sz], 'color', get(findobj('tag', 'OAEToolbox'), 'color'), 'name', 'ITE Calibration', 'tag', 'ITECal', 'numbertitle', 'off');
    ax1 = subplot(221); title('Recorded Waveform'); set(ax1, 'tag', 'ax1');
    ax2 = subplot(223); title('Probe Check Response'); set(ax2, 'tag', 'ax2');
    ylabel('Magnitude (dB)'); xlabel('Frequency (Hz)');
    ax3 = subplot(222); title('Current ITE Compensation Filter'); set(ax3, 'tag', 'ax3');
    ylabel('Magnitude (dB)'); xlabel('Frequency (Hz)');
    ax4 = subplot(224); title('Estimated ITE Response'); set(ax4, 'tag', 'ax4');
    ylabel('Magnitude (dB)'); xlabel('Frequency (Hz)');
else
   ax1 = findobj(h, 'tag', 'ax1'); 
   ax2 = findobj(h, 'tag', 'ax2'); 
   ax3 = findobj(h, 'tag', 'ax3'); 
   ax4 = findobj(h, 'tag', 'ax4'); 
end

ok = 0;
xtick = [500 1000 2000 4000 8000 16000];
xtickl= {'500', '1000', '2000', '4000', '8000', '16000'};
xlimits = [fmin-fmin*0.2 fmax+fmax*0.2];
while (ok ~= 1),
    
    for i=1:obj.nOutput,
        h = oae_waitbar('string', sprintf('Acquiring response from channel %i', obj.OutputChannels(i)), 'name', 'ITE Calibration');
        data = obj.makeCustomRecording(Output, obj.OutputChannels(i), nReps, unCalibrated, ax1, 'Waveform');
        close(h);
        

        % fft & derive inverse filter
        avg = mean(data(5:end,:),1);        % removing the first 4 chirps as they may be shifted the first time portaudio starts recording (jjh).
        obj.probe_check_data{end+1} =avg;   % store it in the object
        nfft = 2048; %length(avg);                 %2.^(nextpow2(length(avg))-1); %2048;
        if mod(nfft,2),
            nfft = nfft-1;
        end
        
        Y = fft(avg, nfft);
        Y = Y./nfft; %preserve signal scaling
        Y = 2*Y(1:floor(nfft/2));
        
        % frequency vector
        f = (0:nfft-1)*(fs/nfft);
        f = f(1:floor(nfft/2));
        
        YY = 20*log10(abs(Y));
        
        % plot the fft of the initial probe check
        axes(ax2); hold on;
        f1k = find(f>=999.9, 1, 'first');
        plot(f, YY-YY(f1k), 'color', obj.linecolors(i,:));
        set(gca, 'xscale', 'log', 'XTick', xtick, 'XTickLabel', xtickl);
        xlim(xlimits);
        
        idx = find(f >= fmin & f<fmax);
        % % use max value with frequency range?
        % [val,idxx] = max(YY(idx));
        % idxx = idx(1)+idxx-1;
        
        % Uncomment - Unity gain - try for testing
        % or value at 1k, since this is what output calibrated at
        f1 = find(f>=1000, 1, 'first');
        f2 = find(f<=1000, 1, 'last');
        if (abs(f(f1) - 1000) < abs(f(f2) -1000)),
            idxx = f1;
        else
            idxx = f2;
        end
        
        % inverse transfer function relative to max value
        YYY = -1*(YY-YY(idxx));
        %smooth this function, otherwise you'll have a filter that tries to over
        %correct
        % Hz = smooth(f, YYY, 0.5, 'loess');
        Hz = smooth(f, YYY);                        % SH 06/09/17, changed tthis to regular moving average smoothing. Makes a much tighter fit, but fir2 seems to be able to handle it
        
        % some limiting characteristics
        % impose a 10dB limit of excursions from 0
        idx_limit = find(abs(Hz) > 10);
        Hz( idx_limit ) = sign(Hz(idx_limit)).*10; 
        % also set values beyond fmax to 0dB (unity gain)
        idx_max = find(f > fmax);
        Hz( idx_max ) = 0;
%         % finally, let's re-smooth this function to round off the hard edges
%         Hzprime = smooth(f, Hz, 10);
        
        
        % generate filter using fir2 (must be in volts or pascals (not dB)
        F = [0; f(idx)'; fs/2];
        A = [0; 10.^(Hz(idx)./20); 0];
        b = fir2(400, F./(fs/2), A);               % SH 06/09/17, chaned to 1000 order filter. (Was 128). Stopped being a pansy about filtering and went all out.
        
        
        % plot the estimated filter response
        [h,frq] = freqz(b,1,fs, fs);
        axes(ax3);
%         plot(f, 20*log10(abs(h)));
        l = findall(gca, 'type', 'line');
        delete(l);
        line(frq, 20*log10(abs(h))); 
        set(gca, 'xscale', 'log', 'XTick', xtick, 'XTickLabel', xtickl);
        xlim(xlimits);
        drawnow;
        
        
        % set the filter "kernel", in this case fir filter coefficients
        obj = obj.setStimulusCalibrationFilter(obj.OutputChannels(i), b);
        
        
        % filter the output and re-perform the measurement for assessment
        M = (length(b)-1)./2;
        pad = zeros(M,1);
        Output1 = filter(b,1, [Output; pad]);
        Output1 = Output1(M+1:end);
        
        h = oae_waitbar('string', sprintf('Acquiring filtered response from channel %i', obj.OutputChannels(i)), 'name', 'ITE Calibration');
        data = obj.makeCustomRecording(Output1, obj.OutputChannels(i), nReps, 0, ax1, 'Waveform');
        close(h);
        
        % plot what the freq response will now look like
        %     [H,f] = freqz(b,1, nfft, fs);
        %     axes(ax2);
        %     semilogx(f, 20*log10(abs(H))); hold on;
        %     xtick = [125 250 500 1000 2000 4000 8000 16000];
        %     xtickl= {'125' '250', '500', '1000', '2000', '4000', '8000', '16000'};
        %     set(gca,'XTick', xtick, 'XTickLabel', xtickl);
        %     ylabel('Magnitude (dB)'); xlabel('Frequency (Hz)');
        %     title('Estimated Magnitude Correction');
        %     axis tight;
        
        %       y = filter(b,1, Output);
        nfft = 2048;
        avg = mean(data,1);
        Y = fft(avg, nfft);
        Y = abs(2*Y(1:nfft/2+1))./nfft;
        f = fs.*(0:nfft/2)./nfft;
        axes(ax4); hold on;
        f1k = find(f>=999.9, 1, 'first');
        YY = 20*log10(Y);
        plot(f, YY-YY(f1k), 'color', obj.linecolors(i,:));
        set(gca, 'xscale', 'log', 'XTick', xtick, 'XTickLabel', xtickl);
        xlim(xlimits);
        ylabel('Magnitude (dB)'); xlabel('Frequency (Hz)');
        title('Estimated Frequency Response');
        
    end
    ok = oae_questdlg('OK to proceed?', {'Yes', 'Repeat Measurement'});
end



