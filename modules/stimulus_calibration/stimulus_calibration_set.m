function OAE = stimulus_calibration_set(OAE, SEQUENCE )

% each time a the the filter file is changed we update the filter kernel.
% if the probe has no associated filter kernel decide what to do nothing has been implemented for other probes.
% if ER10B+ build the kernel based off of norm10cmHID
% call obj = setMicCalFilter(kernel) to set the kernel
% finish

% some notes: Your filter must be a an "Even" symmetric filter with an
% oddnumber of points where the center sample is the center of the kernel.
% Even means h(n) = h(-n) assuming the central sample is sample 0.  However
% since matlab indexes from 1:end  it will be h(centerIdx-i) =
% h(centerIdx+i).  If your filter kernel is set up properly we can account
% for the group delay of the filter, if not group delay cannot be accounted
% for which will throw off the LSF.
% Your .mat file which contains your filter kernel (time domain) can have any name but
% the variable it is stored in must be called micFilterkernel and it must be 4091 points.
%
% BUG: linspace was using OAE.bufferSize but everything else was using 4096 for
% the buffersize. This caused a bug. Fixed 8-24-12 JJH

handle      = findobj( 'tag', 'calibration.parameters');
stimCalType = oae_getvalue(handle, 'stimCalType', 'string');
doDisplay   = oae_getvalue(handle, 'displayITEResult');
freqLimits  = oae_getvalue(handle, 'calibration_freq_limits', 'string');
freqLimits = regexprep(freqLimits, '\[|\]', '');
freqLimits  = str2double(regexp(freqLimits, ' ', 'split'));

% set for all sequences
N = 1;
while (N <= SEQUENCE)
    
    %OAE(N)= OAE(N).setProp( 'micSens', micSens);
    OAE(N)= OAE(N).setProp( 'stimCalType',  stimCalType);
    OAE(N)= OAE(N).setProp( 'doCalibrationPlot', doDisplay );
    OAE(N)= OAE(N).setProp( 'eartipType', oae_getvalue(handle, 'eartipType', 'string') );
    OAE(N)= OAE(N).setProp( 'calibrationFreqLimits',  freqLimits);
    
    switch oae_getvalue(handle, 'stimCalType', 'string')
        case {'None', 'None (Constant Voltage)'}
            OAE(N).calibrationCallback = [];
            OAE(N).stimCalibrationFilter = [];
        case 'In-the-Ear'
            OAE(N).calibrationCallback = 'iteCalibration';
        otherwise
            OAE(N).calibrationCallback = [];
            OAE(N).stimCalibrationFilter = [];
    end
    
    
    N = N +1;
end

