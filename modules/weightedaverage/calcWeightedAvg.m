function wAvg = calcWeightedAvg(obj, weights, window, frameSize, nStep, idx)
% modified to handle subsets of data based in idx, which can be the full
% index or a subset of indexes, mainly for purposes of computing noise
% floors - Joshua J. Hajicek 01-15-2017
nFiles      = size(idx,2);
nx          = size( obj.getData(idx(1), obj.Buffer, obj.Condition), 2 );
nFrames     = floor((nx - frameSize )/ nStep ) + 1;
win         = full(window);                                                        % create column vector from sparse matrix


cumwin = zeros(1,(nStep * nFrames) + frameSize - nStep);                   % this will be the size of the reconstructed average file since we don't analyze partial data frames, hence the averaged file will usually be smaller than the size of the raw data
avg = zeros(2,size(cumwin,2));

for i = 1:nFrames                                                           % takes ~.1 seconds to build.
    i_tmp = (i - 1) * nStep;
    cumwin( i_tmp + (1:frameSize)) = cumwin( i_tmp+(1:frameSize)) + win;
end

for m = 1:nFiles                                                      
    cumdata = zeros(  size(cumwin) );
    data = obj.getData(idx(m), obj.Buffer, obj.Condition);
    for i = 1:nFrames
        i_tmp = (i - 1) * nStep;
        cumdata(i_tmp + (1:frameSize))  =...
            cumdata(i_tmp + ( 1:frameSize )) +  ( weights(m, i) * data( i_tmp +(1:frameSize) ) .* win );
    end
    %weightedFile(m,:) = cumdata ./ cumwin;
    %% divide clean data by the cumulative window - if the window used has zeros at edges we can get NaN's the following modifications avoid this
    weightedFile(m,:) = cumdata(2:end-1) ./ cumwin(2:end-1); 
end
wAvg(1,:) = sum(weightedFile, 1);                                                % Because the weights matrix includes the weights, we don't need to divide the sum by the number of files as the weights matrix has that built into it and is designed for 2 buffers.
wAvg = [0 wAvg 0 zeros(1, nx - size(wAvg,2) ) ];      % pad with zeros to restore data to original size 

end





