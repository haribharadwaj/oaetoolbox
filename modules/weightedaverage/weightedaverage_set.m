function OAE = weightedaverage_set( OAE, SEQUENCE );
% added paramter visibility toggling

handle = findobj( 'tag', 'artifactrejection.parameters'); % UIpanel
N = 1;

ol = oae_getvalue(handle, 'arOverlap');  % overlap %

if ol < 50 || ol > 99 || isnan(ol)
    oae_error('Warning: Overlap must be between 50 & 99 percent, please set it accordingly')
    return
end
x = oae_getvalue(handle, 'arDoThreshold', 'string');
x = lower(x);

if strcmp(x, 'yes')
    y = 1;
    hts = findobj('tag', 'arThresholdStatic');
    set(hts, 'Visible','on')
    ht = findobj( 'tag', 'arThreshold');
    set(ht, 'Visible', 'on')
elseif strcmp(x, 'no')
    y = 0;
    hts = findobj('tag', 'arThresholdStatic');
    set(hts, 'Visible','off')
    ht = findobj( 'tag', 'arThreshold');
    set(ht, 'Visible', 'off')
else
    oae_error('Warning: Not using Thresholds for artifact rejection')
    OAE(N).arDothreshold = 0;
end

while (N <= SEQUENCE)
%     OAE(N)= OAE(N).addExtraProp( 'arNoiseType', oae_getvalue(handle, 'arNoiseType', 'string') );   % specify string for popup menus -need to use OAE(SEQUENCE)= because extraprops private
%     OAE(N)= OAE(N).addExtraProp( 'arOverlap', oae_getvalue(handle, 'arOverlap') );
    OAE(N).arNoiseType                  =  oae_getvalue(handle, 'arNoiseType', 'string');   % specify string for popup menus -need to use OAE(SEQUENCE)= because extraprops private
    OAE(N).arDoThreshold                = y;   % logic 1 or 0
    OAE(N).arThreshold                  = oae_getvalue(handle, 'arThreshold');
    OAE(N).arOverlap                    =  oae_getvalue(handle, 'arOverlap');
    OAE(N).artifactrejectionCallback    = 'weightedaverage_callback';
    OAE(N).artifactrejectionLabel       = 'Weighted Average';
    N = N + 1;
end
end