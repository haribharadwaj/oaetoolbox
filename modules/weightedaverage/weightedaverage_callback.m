function obj = weightedaverage_callback( obj );
%
% 
%
% 10/11/12 - fixed bug where fc_low for wideband filter may not be correct when using
% upsweeps. Affected the BP filter fc high cutoff. Joshua J. Hajicek
% 09/25/16 - Modified this to work with windowfunctions that may have
% zeros at edges - which will cause NaNs after division with the cum
% window. I also ensured that the returned signal is the same size as what was passed to it. Joshua J. Hajicek
% Removed reliance on systemDelay. Now systemDelay should be centralized.
% 11-17-2016 - JJH
% Added noise floor calculations - Joshua J. Hajicek 1-13-2017


%disp('Running weightedaverage_callback');
%data = obj.getCurrentDataset();
% overlap         = obj.getExtraProp( 'arOverlap' );      % stored in obj.extraprops
overlap         = obj.getProp( 'arOverlap' );      % stored in obj.extraprops
threshold       = obj.getProp( 'arThreshold' );
doThreshold    =  obj.getProp( 'arDoThreshold' );
% noiseType       = obj.getExtraProp( 'arNoiseType' );    % stored in obj.extraprops
noiseType       = obj.getProp( 'arNoiseType' );    % stored in obj.extraprops
frameSize       = obj.getProp( 'rtNfilt' );             % Nfilt for finestructure (8 Hz BW)
fs              = obj.getProp( 'fs' );                  % sampling frequency
%nOut            = obj.getProp( 'OutputChannels' );      % size of nOutput (1 x n)
%stimulus        = obj.getCurrentStimulus( 'stimulus' );            % # of nOutputs x stimulus length (m) (n x m x buffers x c conditions)
%nRepetitions    = obj.getProp( 'nRepetitions' );        % (1 x 1)
fstart          = obj.fstart;              % (nPrimary x Buffer x Condition)
fend            = obj.fend;                % (nPrimary x Buffer x Condition)
T               = obj.getProp( 'T' );
paradigmLabel   = obj.getProp( 'paradigmLabel' );
%windowType      = 2;                                 % 2 = hann
nStep           = floor(overlap * 0.01 * frameSize);
nx              = size(obj.getData(1,obj.Buffer, obj.Condition),2);
nFrames         = floor((nx - frameSize )/ nStep ) + 1;
nSweeps         = obj.getProp( 'nRepetitions' );
%win             = oae_computeWindow(0:nStep-1, 2) % 2 = hann window
window          = hann(frameSize)';                                                % kI = scaler: k = hann
window          = window/sqrt( sum( window.^2 ));                                   % scales the window function so it adds to one
window          = sparse(window);
Iwindow         = diag(window);
filtOrder       = 2;   % don't be too agressive with the order - 2 is sufficient. 
fc_low          = 200; % go down to 200 because lowest OAE may be around 300 Hz. 
fc_high         = max( [ max( fstart(:, obj.Buffer, obj.Condition)) max( fend(:, obj.Buffer, obj.Condition))] ) ;
%% MODIFY TO WORK WITH NTRIALS, NCOND, NBUFS WHERE EACH IS SPLIT INTO TWOA AND THEN COMBINED


if mod(nSweeps,2),
   modu = 1; 
else
    modu = 0;
end
switch paradigmLabel
    case { 'dpoae' , 'dpoae-mocr'}
        fstartOAE = 2*fstart(1, obj.Buffer, obj.Condition) - fstart(2, obj.Buffer, obj.Condition);
        fendOAE   = 2*fend(1, obj.Buffer, obj.Condition) - fend(2, obj.Buffer, obj.Condition);
    case { 'sfoae', 'sfoae-mocr'}
        fstartOAE = fstart(1, obj.Buffer, obj.Condition);
        fendOAE = fend(1, obj.Buffer, obj.Condition);
    otherwise
        { 'no oae frequency was specified for this type of OAE'}
end
t_OAE = ( ( 0 : nFrames - 1 ) * nStep + frameSize / 2 )/ fs;                 %time vecotor for OAE
oaeFqVec = fstartOAE * (( fendOAE / fstartOAE ).^( t_OAE / T ));


%%  BUILD MODEL

switch paradigmLabel
    case {'dpoae', 'dpoae-mocr'}
        nPhi = 2;                              %f1, f2, 2f1-f2, 2f2-f1
        Phi = cell(nPhi);
        PhiF = cell(nPhi);
        fstart(3) = 2*fstart(1) - fstart(2);
        fstart(4) = 2*fstart(2) - fstart(1);
        fend(3) = 2*fend(1) - fend(2);
        fend(4) = 2*fend(2) - fend(1);
        
        for i = 1:nPhi
            f1 = fstart(i);
            f2 = fend(i);
            gamma = log(f2/f1)/T;
            Phi{i} = @(t) ((2*pi) * f1 * T/log(f2/f1)) * (f2/f1).^(t/T);
            PhiF{i} = @(t) f1.*exp(gamma .* t);
        end
        
        
    case {'sfoae', 'sfoae-mocr'}
        nPhi = 1;
        f1 = fstart(1);
        f2 = fend(1);
        gamma = log(f2/f1)/T;
        Phi = cell(nPhi);
        PhiF = cell(nPhi);
        Phi{1} = @(t) ((2*pi) * f1 * T/log(f2/f1)) * (f2/f1).^(t/T);
        PhiF{1} = @(t) f1.*exp(gamma .* t);                            % f1
        
    otherwise
        disp('Weighted Average will only work with swept SFOAE and DPOAE, select another averaging method')
end


% LSF

for m = 1 : nFrames;
    
    istep = ((m - 1) * nStep)+1;                                             % index of first sample of data_frame (starting with 0)
    tframe = ( ( 0:frameSize -1 )/fs )+ istep/fs;                            % original way    % time window for the data_frame in seconds
    %t_frame = (i_tmp +( 1:frame_size ))/fs;
    ntau =  floor( frameSize/2 + istep );
    A = zeros( frameSize, nPhi*2 );
    
    % BUILD MODEL
    for i=1:nPhi,
        
        %fvec(i,istep) = feval(PhiF{i}, t1 + tfilt/2);  % frequency is the middle of the window
        fvec(i,istep) = feval(PhiF{i}, ntau);
        %pf = feval(Phi{i}, (tvec - delayfunc(i,ntau) )); % phase function with optional delay at frequency in middle of this window (e.g. ntau)
        pf = feval(Phi{i}, (tframe)); % phase function with optional delay at frequency in middle of this window (e.g. ntau
        A( :, (2*i)-1) = cos(pf);
        A( :, (2*i)) = sin(pf);
        %phicorr(i,istep) = -(pi/2)*(delayfunc(i,ntau)+delayfunc(i, ntau-1))*(feval(PhiF{i}, (ntau + 1)/fs) - feval(PhiF{i}, (ntau - 1)/fs));   
    end
    
    F =  A' * Iwindow * A;                                                % F = A'WA;
    for n = 1:nSweeps % old, before computing the noise floor - JJH
        data = obj.getData(n,obj.Buffer,obj.Condition);
        dataFrame = data( istep + ( 1:frameSize ) );                      %original way    % data_frame, will buffer function work?
        %        frameData(n,i) = dataFrame;                                        % store data frames, we need them later
        dataFrame = dataFrame(:);                                           % ensure data_frame is a column vector
        coefs = F\( A' * Iwindow * dataFrame );
        sigEstimate = A*coefs;
        residual = dataFrame - sigEstimate;
        windowed_residual = window.' .* residual;
        %noiseFloor(n, i) = sqrt(mean(windowed_residual.^2));               % not needed
        
        switch noiseType
            case { 'Broadband' }
                [RMSnoise(n, m) noise] = bpFilterRMS(windowed_residual, fc_low, fc_high, filtOrder, fs );
            case { 'OAE Frequency'}
                
                [RMSnoise(n, m) noise] = oaeNoiseRMS( windowed_residual, oaeFqVec, filtOrder, fs, m  );% x, fvec, order, fs, i
            otherwise
                disp('weightedaverage_callback: unknown noise type')
        end
        
    end
end
 
   

%% This is where the change needs to be for computing the noise floor - when calculating weights, just split RMSnoise into 2 groups, then in calc weights, each will be normalized differently. 
% if we do it before, there is the risk of running the filter twice...

ind1 = 1:2:nSweeps - modu; %ind1 = ind1';
ind2 = 2:2:nSweeps - modu; %ond2 = ind2';
for i = 1:2
    if doThreshold
        weights = calcWeights(eval(['RMSnoise(ind' num2str(i) ',:)']), threshold);
    else
        weights = calcWeights(eval(['RMSnoise(ind' num2str(i) ',:)']));
    end
    wAvg = calcWeightedAvg(obj, weights, window, frameSize, nStep, eval(['ind' num2str(i)]));
    eval(['wAvg' num2str(i) '= wAvg;']);  % wAvg1 and wAvg2
end
nf = wAvg1 - wAvg2;

if doThreshold
    weights = calcWeights(RMSnoise, threshold);
else
    weights = calcWeights(RMSnoise);
end
ind = 1:nSweeps;
wAvg = calcWeightedAvg(obj, weights, window, frameSize, nStep, ind);


obj = obj.saveAverage(wAvg);
obj = obj.saveNoiseFloor(nf);

if ~isdeployed && 0,
    filename = sprintf('%s-%s-systemdelay-%i.wavg.snd', date, now, obj.systemDelay);
    sndwrite(wAvg, obj.fs, 32, 'floatingpoint', '', [oae_getsystempath filename]);
    filename = sprintf('%s-%s-systemdelay-%i.floor.snd', date, now, obj.systemDelay);
    sndwrite(nf, obj.fs, 32, 'floatingpoint', '', [oae_getsystempath 'floor.' filename]);
end
disp('Weighted average complete.')
   %for n = 1:nSweeps % old, before computing the noise floor - JJH
%     for n = 1:2:nSweeps - modu
%         data = obj.getData(n,obj.Buffer,obj.Condition);
%         dataFrame = data( istep + ( 1:frameSize ) );                      %original way    % data_frame, will buffer function work?
% %        frameData(n,i) = dataFrame;                                        % store data frames, we need them later
%         dataFrame = dataFrame(:);                                           % ensure data_frame is a column vector
%         coefs = F\( A' * Iwindow * dataFrame );
%         sigEstimate = A*coefs;
%         residual = dataFrame - sigEstimate; 
%         windowed_residual = window.' .* residual;
%         %noiseFloor(n, i) = sqrt(mean(windowed_residual.^2));               % not needed
%         
%         switch noiseType
%             case { 'Broadband' }
%                 [RMSnoise(n, m) noise] = bpFilterRMS(windowed_residual, fc_low, fc_high, filtOrder, fs );
%             case { 'OAE Frequency'}
%                 
%                 [RMSnoise(n, m) noise] = oaeNoiseRMS( windowed_residual, oaeFqVec, filtOrder, fs, m  );% x, fvec, order, fs, i
%             otherwise
%                 disp('weightedaverage_callback: unknown noise type')
%         end
%         
%     end 
% end
% if doThreshold
%     weights = calcWeights(RMSnoise, threshold);
% else
%     weights = calcWeights(RMSnoise);
% end
% wAvg = calcWeightedAvg(obj, weights, window, frameSize, nStep);
% obj = obj.saveAverage(wAvg);
% disp('Weighted average complete.')

