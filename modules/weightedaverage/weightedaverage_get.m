function OAE = weightedaverage_get( OAE, SEQUENCE )
%% callback function for ui elements (e.g. to update the controls & update
%% the sequence OBJECT
% 10/2/12  v1.7.0 added call to setter so that if selected settings get
% applied. This resolves an issue where if a differnt AR paradigm is selected but no parameters
% are changed and you hit apply, the old AR paradigm is saved. Joshua J.
% Hajicek
cmd = ['OAE = weightedaverage_set( OAE, SEQUENCE );'];
uigeom = {2 [1 1] [1 1] [1 1] [1 1] [1 1] 1};
uilist = { ...
    {'style'  'text'      'string'    'WEIGHTED AVERAGING PARAMETERS'} ...
    {'style'  'text'      'string'    'Select Protocol:'} ...
    {'style'  'popupmenu' 'string'    'protocol', 'tag' , 'protocols', 'callback', @oae_update_protocol} ...
    {'style'  'text'      'string'    'Type of Noise for AR'} ...
    {'style'  'popupmenu' 'string'    {'Broadband' 'OAE Frequency'}, 'tag', 'arNoiseType', 'callback', cmd } ...
    {'style'  'text'      'string'    'Use Threshold Rejection' } ...
    {'style'  'popupmenu' 'string'    {'No' 'Yes'}, 'tag', 'arDoThreshold', 'callback', cmd} ...
    {'style'  'text'      'string'    'Threshold (dB SPL)', 'visible', 'off','tag', 'arThresholdStatic'} ...
    {'style'  'edit'      'string'    '10', 'tag', 'arThreshold', 'Visible', 'off', 'callback', cmd} ...
    {'style'  'text'      'string'    'Frame Overlap %'} ...
    {'style'  'edit'      'string'    '75', 'tag', 'arOverlap', 'callback', cmd} ... 
    };

handle = 'artifactrejection.parameters';    % handles to the UIPanel tag that goes under the modules - handle can be actual handle for fighandle or just the exact string of the tag for the UIpanel
oae_addparameters(handle, uilist, uigeom, 'topborder', 0.01, 'fontsize', 10);
OAE = weightedaverage_set( OAE, SEQUENCE );   % to invoke setter if selected
end