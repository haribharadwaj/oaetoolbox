function [rms noise] = oaeNoiseRMS(x, fvec, order, fs, i  )
% x, fc_low, fc_high, order, fs
%
% Returns RMS value and filtered noise for the frequency vector you specify. 
% order: 1= -6dB/oct, 2 = -12dB/oct, 3 = -18dB/oct etc... 
% Don't be too agressive with your order or you will get artifacts!
            
            if i < size( fvec, 2 )                                       % use for all but last frame
                
                fc_low = fvec( i ) - (fvec( i + 1 ) - fvec( i ));
                fc_high = fvec( i ) +  (fvec( i + 1 ) - fvec( i ));
                
            else                                                            % use for last frame
                
                fc_low  = fvec( i ) -  (fvec( i ) - fvec( i - 1 ));
                fc_high = fvec( i ) +  (fvec( i ) - fvec( i - 1 ));
                
            end
            
            if fc_low > fc_high
                fc_l = fc_high;
                fc_h = fc_low;
                fc_low = fc_l;
                fc_high = fc_h;
            end
            
     
            [ b , a ] = butter(order, [ fc_low, fc_high ] /( fs /2 ));  % because we are using filtfilt- the order of the filter is doubled so "order-1" is used to get the correct order         % second order filter with cutoffs defined by the freq difference between on frame above and below, space gets larger as we go up in f2
            
            if 0  %  1 to check filter shape for artifacts
                figure(10108)
                BW = [floor(fc_low) floor(fc_high)]
                [H, F] = my_freqz(b, a, max(size(x)));
                F = (F*fs)/(2*pi);   % convert radians per sample to frequency
                semilogx(F, 20*log10(abs(H)));
                ylim([-20 0]);
                xlim([500 10000]);
                pause(0.01)
            end
            % can use filtfilt to get zero phase but may slow things down a
            % bit and noise is noise. If you use filtfilt do order/2
            noise = filter( b, a, full(x) );       % bandpass filter the noise - this is a zero phase filter applied both in the forward and reverse directions (order is doubled)
            rms = sqrt(mean(noise.^2));               % rms of the noise around the oae
            