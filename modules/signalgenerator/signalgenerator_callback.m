function OAE  = signalgenerator_callback( OAE, SEQUENCE, params)
%SIGNALGENERATOR_CALLBACK 
% Plays & records specified signal for display.
% This module is useful for verifying the system calibration.
% should add a freerun mode: Runs forever until the button is toggled off
% and plots the appropriate FFT. 
obj = OAE(SEQUENCE);

% some initial checks
if isempty(obj.nOutput) || isempty(obj.nInput),
    oae_error('Please configure your I/O setting.');
    return;
end

sens = obj.sensitivity;
v2pa = obj.v2pa;
if isempty(sens),
    ok = oae_questdlg('Input sensitivity has not been measured on this system. If you want to continue, the input sensitivity will be set to 1V/pa. Would you like to proceed?', {'Yes', 'No'});
    if ok==2,
        return;
    else
        sens = repmat([1], 1, obj.nInput);
    end
end
if isempty(v2pa),
    ok = oae_questdlg('Output sensitivity has not been measured on this system. If you want to continue, the ouput sensitivity will be set to 1V/pa. Would you like to proceed?', {'Yes', 'No'});
    if ok==2,
        return;
    else
        v2pa = repmat([1], 1, obj.nOutput);
    end
end
pref = 2e-5;
switch lower(params.sigType),
    case {'tone'}
        t = 0:1/obj.fs:params.duration;
        
%         % build tone in frequency domain
%         n = nextpow2(length(t));
%         if (2^n) == length(t),
%             nfft = 2^n;
%         else
%             nfft = 2^(n-1);
%         end
%         f = linspace(0,obj.fs/2,nfft/2+1);
%         Y = zeros(1, length(f));
%         freq_index = find(f>=params.sigFrequency, 1, 'first');
%         Y(freq_index) = 1./sqrt(2); %rms power
%         %Y(freq_index) = 1; %rms power
%         y = freq2time(Y);
%         
%         chan_idx = find(obj.OutputChannels == params.outputChannel);
%         y = (10^(params.level/20)*pref*obj.v2pa(chan_idx)).*ramp(y, obj.fs, 0.007);
        
       % build in time domain to test dunctionality
        chan_idx = find(obj.OutputChannels == params.outputChannel);
        t = 0:1/obj.fs:params.duration;
        y = (10^(params.level/20)*pref*v2pa(chan_idx)).*oae_ramp(sin(2*pi.*t*params.sigFrequency), obj.rampTime, obj.fs, 'both');
        y = y';
    case {'noise'}
        chan_idx = find(obj.OutputChannels == params.outputChannel);
        t = 0:1/obj.fs:params.duration;
        noise = (rand(1,length(t))-0.5).*1; % put it in -1 to 1 range
        y = oae_ramp((10^(params.level/20)*pref*v2pa(chan_idx)).*(noise./(sqrt(mean(noise.^2)))), obj.rampTime, obj.fs, 'both');
%         y = (10^(params.level/20)*pref*v2pa(chan_idx)).*(noise.*sqrt(2)));
    case {'logsweep'}
        chan_idx = find(obj.OutputChannels == params.outputChannel);
        t = 0:1/obj.fs:params.duration;
        T = t(end);
        logsweep = oae_logsweep(params.f1, params.f2, T, obj.fs); % log sweep;
        y = oae_ramp((10.^(params.level/20)*pref*v2pa(chan_idx))*logsweep, obj.rampTime, obj.fs, 'both');
    case {'linsweep'}
        chan_idx = find(obj.OutputChannels == params.outputChannel);
        t = 0:1/obj.fs:params.duration;
        T = t(end);
        linsweep = oae_linsweep(params.f1, params.f2, T, obj.fs); % linear sweep;
        y = oae_ramp((10.^(params.level/20)*pref*v2pa(chan_idx))*linsweep, obj.rampTime, obj.fs, 'both');
    case 'click'
        chan_idx = find(obj.OutputChannels == params.outputChannel);
        t = 0:1/obj.fs:params.duration;
        T = t(end);
        T = T+(obj.systemDelay/obj.fs); % click at 3ms + systemDelay. System delay is added in order to ensure enough samples are recorded.
        N = round(T * obj.fs); % total number of samples
        pad = zeros(N,1);
        y = zeros(N,1);
        y(round(.003*obj.fs)) = 0.2511;
        y = (10.^(params.level/20)*pref*v2pa(chan_idx))*y;
    case 'chirp'
        chan_idx = find(obj.OutputChannels == params.outputChannel);
        t = 0:1/obj.fs:params.duration;
        T = t(end);
        y = oae_ramp((10.^(params.level/20)*pref*v2pa(chan_idx))*oae_chirp(T, obj.fs), obj.rampTime, obj.fs, 'both');
        % should we add an ISI? 
    otherwise
end

h = oae_waitbar();
% make a custom recording
% USAGE:
% y = makeCustomRecording( obj, x, playBackCh, nReps, applyCalibration, doPlot, plotTitle )
% data = obj.makeCustomRecording( y, params.outputChannel, params.nReps, 1, 1, 'Signal Generator');
data = obj.makeCustomRecording( y, params.outputChannel, params.nReps, 1, 0);
close(h);
%keyboard;
if params.displayResult,
    
    
    
    
    
    if params.nReps > 1,
    data = mean(data);
    end
    h = figure('numbertitle', 'off', 'name', 'OTB Signal Generator');
    subplot(2,1,1);
    plot(data);
    title('Time Domain Signal')
    % frequency response estimation
    switch lower(params.sigType)
        case 'tone'
        case 'noise'
        case 'linsweep'   %use welch periodogram or nseoae
        p.T = params.duration                               % 6 second sweep
        p.nfilt = 5512;                        % window size
%       params.nstep = 51;                          % step size
%       params.component(1).type = 'logsweep';      % component type            (values can be 'logsweep', 'tone', 'linear')
%       params.component(1).f1 = 1000;              % logsweep start frequency  (for 'tone' type, only f1 is necessary)
%       params.component(1).f2 = 8000;              % logsweep end frequency    (for 'tone' type, only f1 is necessary)
%
%   Optional PARAMS:
%                                                   % This (nskip) is a very important parameter, as can cause misalignment 
%                                                   % between LSF model and actual signal, especially for long filter windows    
%       params.nskip = 100;                         % samples to skip, default = 0 (e.g. due to system delay, propogation delay)
%       params.windowType = 2;                      % window function (-1 = rectangular, 0 = welch, 1 = hamming, 2 = hann [default], 3 = parzen, 4 = blackman)
%       params.fs = 44100;                          % sampling rate (defau
%     oae(i).time         = tcoef;
%     oae(i).f            = fvec(i,:);
%     oae(i).coefs        = [coef((2*i)-1,:); coef((2*i),:)];
%     oae(i).mag          = hypot(coef((2*i)-1,:), coef((2*(i)),:));
%     oae(i).phase        = -atan2(coef((2*i),:),coef((2*i)-1,:));
%     oae(i).total_phase  = -atan2(coef((2*i),:),coef((2*i)-1,:))+p_delay(i,:);
%     oae(i).delay        = t_delay(i,:);
%     oae(i).rmserror     = rmserror(i,:);
        case {'click', 'chirp'}
            
        case 'logsweep'
        case 'chirp'
    end
    
    
    subplot(2,1,2);
    L = length(data);
    nfft = 2^nextpow2(L);
    f = linspace(0,obj.fs/2,nfft/2+1);
%     Y = fftpeak(data, nfft);
    Y = fft(data, nfft);
    %%
    YY = 20*log10(2*abs(Y(1:nfft/2+1))./nfft);
    semilogx(f, YY);
    [YYmax, I] = max(YY);
    hold on
    semilogx(f(I), YY(I), 'og')
    title(['Spectrum: Max=' num2str(YYmax)]);
    ylabel('Magnitude (dB SPL)');
    grid on
    hold off
    if strcmp(lower(params.sigType),'logsweep')
        xlim([ min([params.f1, params.f2]) max([params.f1, params.f2])]);
    end
end



end

