function [ OAE ] = signalgenerator_set( OAE, SEQUENCE )
%SIGNALGENERATOR_SET 
%  Adding dynamic static text to GUI to better reflect parameters required
%  for input into each field for each type of stimulus (tone, logsweep,
%  noise)
% Modifications
%   6-11-2017 - added dynamic text for clicks and chirps

handle = findobj('tag', 'runCalButton');
doIte = get(handle, 'value');
if doIte
    %check if calibration routine exists
    if (isempty(OAE(SEQUENCE).calibrationCallback) && ~strcmp(OAE(SEQUENCE).calibrationCallback, '')),
        oae_error('There is no calibration routine set for the current sequence.', 'Problem');
        return;
    end
    
    handle = findobj('tag', 'calStatusText');  
    oae_setvalue(handle, 'Status: Calibrating...');
    OAE(SEQUENCE) = OAE(SEQUENCE).runCalibration();
    oae_setvalue(handle, 'Status: Output is Calibrated');
    handle = findobj('tag', 'runCalButton');
    oae_setvalue(handle, 'Re-Run Output Calibration');
    return
else


% handles selectables
handle = 'extras.parameters';
sigType = oae_getvalue(handle, 'signalType', 'string');
sigFrequency = oae_getvalue(handle, 'signalFrequency');
level = oae_getvalue(handle, 'Level');
duration = oae_getvalue(handle, 'Duration');
nReps = oae_getvalue(handle, 'numRepetitions');
outputChannel = oae_getvalue(handle, 'outputChannel');
displayResult = oae_getvalue(handle, 'displayResult');
start = oae_getvalue(handle, 'start');

hf = findobj('tag','sgSignalFrequencyStatic');
hff = findobj('tag', 'signalFrequency');
hl = findobj('tag', 'sgLevelStatic');
hll = findobj('tag', 'Level');
hd = findobj('tag', 'sgDuration');


switch lower(sigType)   % Dynamic text to better help the enduser input the appropriate parameters
    case 'tone'
        set(hf,'string','Signal Frequency (Hz):', 'visible', 'on')
        set(hff, 'visible', 'on')
        set(hl, 'string', 'dB SPL')
        set(hd, 'string','Duration (s):')
    case {'logsweep', 'linsweep', 'chirp'}
        set(hf,'string','Enter: f-start (Hz)    f-end (Hz):', 'visible', 'on')
        set(hff, 'visible', 'on')
        set(hl, 'string', 'dB SPL')
        set(hd, 'string','Duration (s):')
    case {'noise'}
        set(hff, 'visible', 'off', 'string', '')
        set(hf, 'visible', 'off') 
        set(hl, 'string', 'dB SPL')
        set(hd, 'string','Duration (s):')
    case 'click'
        set(hff, 'visible', 'off', 'string', '')
        set(hf, 'visible', 'off') 
        set(hl, 'string', 'dB peSPL')
        set(hd, 'string', 'ISI (s)')
    otherwise
        set(hf,'string','Signal Frequency (Hz):')
        set(hff, 'visible', 'on')
        set(hl, 'string', 'dB SPL')
        set(hd, 'string','Duration (s):')
end

if start,
    switch lower(sigType)
        case 'tone'
            %validate arguments
            if oae_validate([sigFrequency level duration nReps])
                %TODO: build paramters structure for callback
                params.sigType = sigType;
                params.sigFrequency = sigFrequency;
                params.level = level;
                params.duration = duration;
                params.nReps = nReps;
                params.outputChannel = outputChannel;
                params.displayResult = displayResult;
                %send info to signalgenerator callback
                r = signalgenerator_callback(OAE, SEQUENCE, params);
            else
                oae_error('Please enter a value for all fields!');
            end
        case {'logsweep', 'linsweep'}
            % retreive sigFrequency as a string since it is possible that
            % it was input as: 1000 8000 or [1000 8000]
            % so parse the string to get its components
            sigFrequency = oae_getvalue(handle, 'signalFrequency', 'string');
            %validate arguments
            if oae_validate([sigFrequency level duration nReps])
                sigFrequency = str2double(strsplit(regexprep(sigFrequency, '(\[)|(\])', ''), ' '));
                if length(sigFrequency) ~= 2,
                   oae_error('Signal frequency must be a 2-element array, indicating start and end frequencies (e.g. [1000 8000]).');
                   return;
                end
                %TODO: build paramters structure for callback
                params.sigType = sigType;
                params.f1 = sigFrequency(1);
                params.f2 = sigFrequency(2);
                params.level = level;
                params.duration = duration;
                params.nReps = nReps;
                params.outputChannel = outputChannel;
                params.displayResult = displayResult;
                %send info to signalgenerator callback
                r = signalgenerator_callback(OAE, SEQUENCE, params);
            else
                oae_error('Please enter a value for all fields!');
            end
        case {'noise'}
            %validate arguments
            if oae_validate([level duration nReps])
                %TODO: build paramters structure for callback
                params.sigType = sigType;
                params.sigFrequency = sigFrequency;
                params.level = level;
                params.duration = duration;
                params.nReps = nReps;
                params.outputChannel = outputChannel;
                params.displayResult = displayResult;
                params.f1 = 250;
                params.f2 = 16000;
                %send info to signalgenerator callback
                r = signalgenerator_callback(OAE, SEQUENCE, params);
            else
                oae_error('Please enter a value for level, duration and # of repititions!');
            end
        case 'click'
            %validate arguments
            if oae_validate([level duration nReps])
                %TODO: build paramters structure for callback
                params.sigType = sigType;
                params.level = level;
                params.duration = duration;
                params.nReps = nReps;
                params.outputChannel = outputChannel;
                params.displayResult = displayResult;
                %send info to signalgenerator callback
                r = signalgenerator_callback(OAE, SEQUENCE, params);
            else
                oae_error('Please enter a value for level, duration and # of repititions!');
            end
            
        case 'chirp'
             sigFrequency = oae_getvalue(handle, 'signalFrequency', 'string');
            %validate arguments
            if oae_validate([sigFrequency level nReps])
                sigFrequency = str2double(strsplit(regexprep(sigFrequency, '(\[)|(\])', ''), ' '));
                if length(sigFrequency) ~= 2,
                   oae_error('Signal frequency must be a 2-element array, indicating start and end frequencies (e.g. [1000 8000]).');
                   return;
                end
                %TODO: build paramters structure for callback
                params.sigType = sigType;
                params.f1 = sigFrequency(1);
                params.f2 = sigFrequency(2);
                params.level = level;
                params.duration = duration;
                params.nReps = nReps;
                params.outputChannel = outputChannel;
                params.displayResult = displayResult;
                %send info to signalgenerator callback
                r = signalgenerator_callback(OAE, SEQUENCE, params);
            else
                oae_error('Please enter a value for all fields!');
            end
    end
    
end


end

