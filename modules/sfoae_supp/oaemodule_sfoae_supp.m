%
% 
% sfoae_suppression - 2 condition - acquisition plugin
function [vers block label cmd] = oaemodule_sfoae_supp_2c(fig)

vers='SFOAE Suppression 1.0';
label = 'SFOAE Suppression';
cmd =  [  'OAE = sfoae_supp_get(OAE, SEQUENCE);'];
block = 'paradigm';


end