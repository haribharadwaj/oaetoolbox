function obj = oae_playrec_recorder(obj)
%
% Wrapper function for recording using playrec
% OAEToolbox, copyright 2012

% reorder stimulus matrix. playrec uses different convention then others
y = obj.getCurrentStimulus();


%% error checking
if ( obj.nOutput ~= min(size(y))),
    oae_error('Number of output channels does not match the number of stimuli');
end

%Initialise if not initialised
if(~playrec('isInitialised'))
    fprintf('Initialising playrec to use sample rate: %d, playDeviceID: %d and recDeviceID: %d\n', obj.fs, obj.playbackDeviceID, obj.recordingDeviceID);
    %playrec('init', obj.fs, obj.playbackDeviceID, obj.recordingDeviceID, obj.nOutput, obj.nInput);
    playrec('init', obj.fs, obj.playbackDeviceID, obj.recordingDeviceID);
    
    % This slight delay is included because if a dialog box pops up during
    % initialisation (eg MOTU telling you there are no MOTU devices
    % attached) then without the delay Ctrl+C to stop playback sometimes
    % doesn't work.
    pause(0.5);
end

if(~playrec('isInitialised'))
    error ('Unable to initialise playrec correctly');
end

if(playrec('pause'))
    fprintf('Playrec was paused - clearing all previous pages and unpausing.\n');
    playrec('delPage');
    playrec('pause', 0);
end


% setup buffering
pageBufCount = 10;      %number of pages of buffering
runMaxSpeed = true;    %When true, the processor is used much more heavily
                        %(ie always at maximum), but the chance of skipping is
                        %reduced
                        
%nBuffers = size(y,2)*obj.fs/pageSize;

% recording = playrec('playrec', y, obj.OutputChannels, -1, obj.InputChannels);
% playrec('block',recording);    
% input = playrec('getRec', recording);


pageSize = obj.bufferSize;
pageNumList = [];
nextPageSamples = zeros(pageSize, 1);
runMaxSpeed = true;
pageBufCount = 10;%ceil(size(y,2))/pageSize;

input = zeros(1, length(y));
pageBuffer = 0;
numBuffers = ceil(obj.fs*obj.T/pageSize);

playrec('resetSkippedSampleCount');
for buffer = 1:numBuffers
    if (buffer == numBuffers),
        pageNumList = [pageNumList playrec('playrec', y((buffer-1)*pageSize+1:end, :), ...
        obj.OutputChannels, -1, obj.InputChannels)];
    else
        pageNumList = [pageNumList playrec('playrec', y((buffer-1)*pageSize+1:(buffer)*pageSize, :), ...
            obj.OutputChannels, -1, obj.InputChannels)];
    end
    
    % runMaxSpeed==true means a very tight while loop is entered until the
    % page has completed whereas when runMaxSpeed==false the 'block'
    % command in playrec is used.  This repeatedly suspends the thread
    % until the page has completed, meaning the time between page
    % completing and the 'block' command returning can be much longer than
    % that with the tight while loop
    if(length(pageNumList) > pageBufCount)
        if(runMaxSpeed)
            while(playrec('isFinished', pageNumList(1)) == 0)
            end
        else        
            playrec('block', pageNumList(1));
        end

        PageSamples = playrec('getRec', pageNumList(1));
        playrec('delPage', pageNumList(1));
        pageNumList = pageNumList(2:end);
        %update runtime GUI after each page - now a page is set to 4096
        %samples
        
        %obj = obj.updateGUI(PageSamples);

        %input = [input; PageSamples];
        pageBuffer = pageBuffer+1;
        input((pageBuffer-1)*pageSize+1:pageBuffer*pageSize) = PageSamples;
        
        if (pageBuffer)*obj.rtBufferSize < length(input),
            obj = obj.updateGUI(input((pageBuffer-1)*obj.rtBufferSize+1:(pageBuffer)*obj.rtBufferSize));
        end
    end
end


%% Get final pages
while length(pageNumList) > 1,
    
    if(runMaxSpeed)
        while(playrec('isFinished', pageNumList(1)) == 0)
        end
    else        
        playrec('block', pageNumList(1));
    end

    PageSamples = playrec('getRec', pageNumList(1));
    playrec('delPage', pageNumList(1));
    pageNumList = pageNumList(2:end);
    %update runtime GUI after each page
    %obj = obj.updateGUI(PageSamples);
    
    %input = [input; PageSamples];
    pageBuffer = pageBuffer+1;
    input((pageBuffer-1)*pageSize+1:pageBuffer*pageSize) = PageSamples;
    
    %obj = obj.updateGUI(input((pageBuffer-1)*obj.rtBufferSize+1:(pageBuffer)*obj.rtBufferSize));
end

% check if we had any dropped/skipped samples
skipped = playrec('getSkippedSampleCount');
if skipped,
    fprintf(1, 'playrec warning: %i samples skipped during playback/recording. \n', skipped);
end

if length(pageNumList) == 1,
    
    if(runMaxSpeed)
        while(playrec('isFinished', pageNumList(1)) == 0)
        end
    else        
        playrec('block', pageNumList(1));
    end
    
    PageSamples = playrec('getRec', pageNumList(1));
    playrec('delPage', pageNumList(1));
    %update runtime GUI after each page
    pageBuffer = pageBuffer+1;
    input((pageBuffer-1)*pageSize+1:end) = PageSamples;
end


% convert column into row data for the object
obj = obj.saveRecording(input);              






