function [vers block label cmd] = oaemodule_playrec(fig)

vers = '1.0 (playrec v.2.1.1)';
label = 'playrec';
% menu callback commands
cmd =  [  'OAE = playrec_options(OAE, SEQUENCE);'];
block = 'daq';

end
