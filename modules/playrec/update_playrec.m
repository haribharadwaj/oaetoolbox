function OAE = update_playrec(OAE, SEQUENCE)


deviceName = oae_getvalue('daq.parameters', 'deviceName', 'string');

dev = playrec('getDevices');
device_index = find(strcmp({dev(:).name}, deviceName));

oae_setvalue('deviceID', device_index);

nInputAvailable = 0;
nOutputAvailable = 0;

numchan = {};
if dev(device_index).inputChans,
    nInputAvailable = dev(device_index).inputChans;
    for i=1:dev(device_index).inputChans,
        numchan{i} = num2str(i);
    end
    handle = findobj('tag', 'numInputChannels');
    oae_setvalue(handle, numchan);
else
    handle = findobj('tag', 'numInputChannels');
    set(handle, 'string', {''}, 'value', 1);
end

numchan = {};
if dev(device_index).outputChans,
    nOutputAvailable = dev(device_index).outputChans;
    for i=1:dev(device_index).outputChans,
        numchan{i} = num2str(i);
    end
    handle = findobj('tag', 'numOutputChannels');
    oae_setvalue(handle, numchan);
else
    handle = findobj('tag', 'numOutputChannels');
    set(handle, 'string', {''}, 'value', 1);
end

% select number of in/out channels
nInput = oae_getvalue('daq.parameters', 'numInputChannels');
nOutput = oae_getvalue('daq.parameters', 'numOutputChannels');

% get selected channels
selected_input_channels = get(findobj('tag', 'numInputChannels'), 'userdata');
if isempty(selected_input_channels) || (length(selected_input_channels) ~= nInput),
    if ~isnan(nInput),
        selected_input_channels = 1:nInput;
        oae_setvalue('numInputChannels', nInput, 'userdata', selected_input_channels);
    end
end
selected_output_channels = get(findobj('tag', 'numOutputChannels'), 'userdata');
if isempty(selected_output_channels) || (length(selected_output_channels) ~= nOutput),
    if ~isnan(nOutput),
        selected_output_channels = 1:nOutput;
        oae_setvalue('numOutputChannels', nOutput, 'userdata', selected_output_channels);
    end
end

% get frequency sampling rate = default set to 48000
fs = oae_getvalue('daq.parameters', 'samplingRate');

% get system sample delay
systemDelay = oae_getvalue('daq.parameters', 'systemDelay');

% Update the Object
OAE(SEQUENCE).daqCallback           = 'oae_playrec_recorder';
OAE(SEQUENCE).playbackDeviceID      = dev(device_index).deviceID;
OAE(SEQUENCE).recordingDeviceID     = dev(device_index).deviceID;
OAE(SEQUENCE).recordingDeviceName   = dev(device_index).name;
OAE(SEQUENCE).nOutputAvailable      = nOutputAvailable;
OAE(SEQUENCE).nOutput               = nOutput;
OAE(SEQUENCE).nInputAvailable       = nInputAvailable;
OAE(SEQUENCE).nInput                = nInput;
OAE(SEQUENCE).OutputChannels        = selected_output_channels;
OAE(SEQUENCE).InputChannels         = selected_input_channels;
OAE(SEQUENCE).systemDelay           = systemDelay;
OAE(SEQUENCE).fs = fs;
end