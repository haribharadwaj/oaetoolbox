function OAE = dpoae2_set(OAE, SEQUENCE),

handle = findobj('tag', 'paradigm.parameters');
f2s = oae_getvalue(handle, 'f2start');
f2e = oae_getvalue(handle, 'f2end');
ratio = oae_getvalue(handle, 'ratio');
L1 = oae_getvalue(handle, 'L1');
L2 = oae_getvalue(handle, 'L2');
nreps = oae_getvalue(handle, 'nRepetitions');
T = oae_getvalue(handle, 'T');
bufferType = oae_getvalue(handle, 'bufferType', 'string');

if oae_validate([L1 L2 f2s f2s T nreps ratio]),
    %reset the sequence
    OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

    switch bufferType,
        case {'up/down', 'alternating'}
            OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [round(f2s./ratio) f2s], [round(f2e./ratio) f2e], T);
            OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [round(f2e./ratio) f2e], [round(f2s./ratio) f2s], T);
        otherwise
            OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [round(f2s./ratio) f2s], [round(f2e./ratio) f2e], T);
    end
    OAE(SEQUENCE).nRepetitions = nreps;
    OAE(SEQUENCE).paradigmType = 'logsweep';
    OAE(SEQUENCE).paradigmLabel = 'dpoae';
end