function OAE = discrete_dpoae_set(OAE, SEQUENCE),

handle = findobj('tag', 'paradigm.parameters');
f_dp_start = oae_getvalue(handle, 'f_dp_start');
f_dp_end = oae_getvalue(handle, 'f_dp_end');
ratio = oae_getvalue(handle, 'ratio');
ppo = oae_getvalue(handle, 'ppo');
L1 = oae_getvalue(handle, 'L1');
L2 = oae_getvalue(handle, 'L2');
T = oae_getvalue(handle, 'duration');
protocol = oae_getvalue(handle, 'protocols', 'string');

%check all variables properly set
if oae_validate([f_dp_start f_dp_end ratio ppo L1 L2 T]),
    
    
    % Initialize a new sequence
    OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();
    
    % These variables are taken from genCom-DP-fixedratio.sh
    % Need to discuss with Glenis
    
%     % Greenwood parameters
%     kw = 1.38155106/10;
%     f0 = 20822.626311;
%     fc = -145.552;
%     norder = 1;     %order = 1 (2*f1-f2), 2 = (3f1-2f2);
%     fmult = (norder + 1) - norder * ratio;
%     dx = 0.4/ppo;   % This 0.4/points_per_fine_structure_period
%     df = 6;         % desired frequency resolution of primaries
%     
%     f1 = []; f2 = [];
%     xstart = -log((f_dp_start - fc)/f0)/kw;
%     xstop = -log((f_dp_end - fc)/f0)/kw;
%     x = xstart:-dx:xstop;
%     x = x(randperm(length(x)));
%     while length(x)>0,
%         xx = x(1);
%         fdp = f0 * exp(-kw * xx) + fc;
%         f1 = floor(fdp/fmult/df + 0.5) * df;
%         f2 = floor((f1 + (f1 - fdp)/norder)/2 + 0.5) * 2;
%         OAE(SEQUENCE) = OAE(SEQUENCE).addTone([L1 L2], [f1 f2], T);
%         x(1) = [];
%     end
    
    % Do logspace array with points-per-octave
    nOct = abs(log2(f_dp_end / f_dp_start));
    x = round( logspace( log10(f_dp_start), log10(f_dp_end), abs(log2(f_dp_end/f_dp_start))*ppo) ); % rounded to nearest interger
    x = x(randperm(length(x)));
    % make sure they are multiples of 4
    fmult = 4;
    for i=1:length(x),
        if mod(x(i), fmult) ~= 0,
            x(i) = x(i) + (fmult-rem(x(i),fmult)); 
        end
        fdp = x(i);          % this will be a integer multiple of fmult (e.g. 4)   
        f1 = fdp/(2-ratio);  % these may not be integers, is that a problem? 
        f2 = ratio*f1;
        OAE(SEQUENCE) = OAE(SEQUENCE).addTone([L1 L2], [f1 f2], T);
    end
    
    % TODO: Add VU meter for discrete tones (+ stimulus analysis?)
    OAE(SEQUENCE).setProp('displayRT', false);
    OAE(SEQUENCE).paradigmLabel = 'discrete dpoae';
    OAE(SEQUENCE).T = T;
    OAE(SEQUENCE).protocol = protocol;
    OAE(SEQUENCE).nRepetitions = 1;
    OAE(SEQUENCE).paradigmType = 'tone';
end

end
