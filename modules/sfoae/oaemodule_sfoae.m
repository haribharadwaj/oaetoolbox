%
% 
% sfoae acquisition plugin without suppressor
function [vers block label cmd] = oaemodule_sfoae(fig)

vers='sfoae 1.0';
label = 'SFOAE';
cmd =  [  'OAE = sfoae_get(OAE, SEQUENCE);'];
block = 'paradigm';


end