function OAE = set_sfoae(OAE, SEQUENCE)


%reset the sequence
OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

% return the values of the edit box
handle = findobj(OAE(SEQUENCE).fig, 'tag', 'paradigm.parameters');
f1s = oae_getvalue(handle, 'f1s', 'int');
f1e = oae_getvalue(handle, 'f1e', 'int');
L1 = oae_getvalue(handle, 'L1', 'int');
Toct = oae_getvalue(handle, 'Toct', 'int');
nReps = oae_getvalue(handle, 'nRepetitions', 'int');
bufferType = oae_getvalue(handle, 'bufferType', 'string');
protocol = oae_getvalue(handle, 'protocol', 'string');

%% TODO: add variable validation routine
if all([L1 f1s f1e nReps Toct]) & ~isnan([L1 f1s f1e nReps Toct]),
    
    %reset the sequence
    OAE(SEQUENCE) = OAE(SEQUENCE).initializeSequence();

    Noct = log2(f1e/f1s);
    T = Noct * Toct;
    if T,
        OAE(SEQUENCE).paradigmLabel = 'sfoae';
        OAE(SEQUENCE).protocol = protocol;
        OAE(SEQUENCE).T = T;
        switch bufferType,
            case 'up/down'
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1s,f1e,T);
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1e,f1s,T);
                %         OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
                %         OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1e f2e], [f1s f2s], T);
            case 'alternating'
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1s,f1e,T);
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1s,f1e,T);
                %         OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
                %         OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
            otherwise
                OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1s,f1e,T);
                %         OAE(SEQUENCE) = OAE(SEQUENCE).addSweep([L1 L2], [f1s f2s], [f1e f2e], T);
        end
        %         OAE(SEQUENCE) = OAE(SEQUENCE).addSweep(L1, f1s,f1e,T);
        OAE(SEQUENCE).nRepetitions = nReps;
    end
end

OAE(SEQUENCE).paradigmType = 'logsweep';
end
