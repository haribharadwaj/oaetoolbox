function OAE = portaudio3_get(OAE, SEQUENCE)
%
% portaudio getter GUI

% setup Object I/O
OAE(SEQUENCE).driver = 'portaudio';
OAE(SEQUENCE).daqCallback = 'oae_portaudio3';


% We'll be using the PsychPortAudio mex file from PsychToolbox
% Let's clear any instances of the file loaded into memory so that we get
% any new devices attached since last load
clear PsychPortAudio;

% User/Device parameters
% On windows, we need to check for ASIO enabled devices.
asio = 0;

if ~isdeployed
    if ispc
        olddir = pwd;
        pa = which('portaudio_x86.dll');
        pth = fileparts(pa);
        cd(pth);
        dev = PsychPortAudio('GetDevices');
        if ~isempty(dev),
            asio = 1;
        end
        cd(olddir);
    end
else
%     clear PsychPortAudio;
%     dev = PsychPortAudio('GetDevices');
end
% for OSX, and non-asio
if ~asio,    
    clear PsychPortAudio;
    dev = PsychPortAudio('GetDevices');
end

devices = {};
for i=1:length(dev),
    devices{i} = sprintf('(%s) %s [%s]', num2str(i), dev(i).DeviceName, dev(i).HostAudioAPIName);
end


title = 'Device options';
cmd = ['OAE = portaudio3_set(OAE, SEQUENCE);'];
uilist = {...
    {'style'  'text'        'string'    'Device'} ...
    {'style'  'popupmenu'   'string' , devices   , 'tag', 'deviceLabel', 'callback', cmd} ...
    {'style'  'text'        'string'    '# Input Channels'} ...
    {'style'  'popupmenu'   'string' , {''} , 'tag', 'numInputChannels', 'callback', cmd} ...
    {'style'  'text'        'string'          'dummy'      'visible'       'off'} ...
    {'style'  'pushbutton'  'string'    'Select Channels', 'callback', {@portaudio_select_channels, 'input_select'}} ...
    {'style'  'text'        'string'    '# Output Channels'} ...
    {'style'  'popupmenu'   'string' , {''} , 'tag', 'numOutputChannels', 'callback', cmd} ...
    {'style'  'text'        'string'          'dummy'      'visible'       'off'} ...
    {'style'  'pushbutton'  'string'    'Select Channels', 'callback', {@portaudio_select_channels, 'output_select'}} ...
    {'style'  'text'        'string'    'Input Sampling Rate (Hz)'} ...
    {'style'  'popupmenu'   'string',    {'44100' '48000'},    'tag', 'samplingRate', 'callback', cmd} ...
    {'style'  'text'        'string'    'System Sample Delay'} ...
    {'style'  'edit'        'string',    '0',    'tag', 'systemDelay', 'callback', cmd} ...
    {'style'  'pushbutton'  'string',    'Measure System Delay',   'callback', {@oae_measure_system_delay, OAE(SEQUENCE), 'daq.parameters'}} ...
    {'style'  'text'        'string'    ''  'tag'   'deviceID'  'visible'   'off'} ...
    {'style'  'text'        'string'    ''  'tag'   'deviceName'  'visible'   'off'} ...
    {'style'  'checkbox'    'string'    'Route CAS to other device', 'tag', 'routeCAS', 'callback', cmd} ...
    {'style'  'popupmenu'   'string'    devices, 'tag', 'routeDevice', 'visible', 'off', 'callback', cmd} ...
    };
uigeom = {[0.5 1.5] [1.25 .75] [1.25 .75] [1.25 .75] [1.25 0.75] [1.25 .75] [1.25 .75] [2] [2] [2] [2] [2]};


handle = findobj('tag', 'daq.parameters');
set(handle, 'title', title);
oae_addparameters('daq.parameters', uilist, uigeom);
evalin('base', cmd);


end

