function channels = portaudio_select_channels(hObject, event, type)


panel = get(hObject, 'parent');
index = oae_getvalue('daq.parameters', 'deviceID');
dev = PsychPortAudio('GetDevices');

switch type,
    case {'input_select'},
        numChan = dev(index).NrInputChannels;
        selected_channels = get(findobj('tag', 'numInputChannels'), 'userdata');
    case {'output_select'},
        numChan = dev(index).NrOutputChannels;
        selected_channels = get(findobj('tag', 'numOutputChannels'), 'userdata');
end

dimensions = [220 30*ceil(numChan/4)+80];
h = figure('Units','pixels', ...
	'PaperUnits','points', ...
	'numbertitle', 'off', ...
	'resize', 'off', ...
	'Units', 'Pixels', ...
	'Tag','OAEToolbox', ...
    'Toolbar', 'none', ...
    'Menubar', 'none' ...
    );
try,
    set(h, 'NextPlot','new');
catch, end;


uicontrol(h, 'style', 'text', 'string', 'Select Channels', 'units', 'pixels', 'horizontalAlignment', 'center', 'position', [dimensions(1)/2-100 dimensions(2)-30 200 30]);
p = get(h, 'position');
set(h, 'position', [p(1) p(2) dimensions(1) dimensions(2)]);
x = 20; y=dimensions(2)-60; width = 50; height = 30;
controls = [];
for i=1:numChan,
    value = 0;
    if find(selected_channels == i), value = 1; end
    controls(i) = uicontrol(h, 'style', 'checkbox', 'string', num2str(i), 'value', value, 'units', 'pixels', 'position', [x y width height]);
    if ~mod(i,4) & i ~= numChan,
        x = 20; y = y - height;
    else
        x = x + width;
    end
end
y = y - height;
uicontrol(h, 'style', 'pushbutton', 'string', 'apply', 'units', 'pixels', 'position', [dimensions(1)/2-100 y 200 30], 'callback', {@playrec_set_channels, h, controls, type});

end



function playrec_set_channels(hObject, event,h, controls, type)
% set inputChan value, initiate callback.
panel = get(hObject, 'parent');
channels = [];
for i=1:length(controls),
    if get(controls(i), 'value'),
        channels = [channels str2double(get(controls(i),'string'))];
    end
end
cmd = [];
switch type,
    case {'input_select'},
        val = oae_getvalue(panel, 'numInputChannels');
        cmd = get(findobj('tag', 'numInputChannels'), 'callback');
        if (val ~= length(channels)),
            oae_error('Wrong number of input channels');
        else
        oae_selectpopupvalue('numInputChannels', num2str(val), 'userdata', channels);
        close(h);
        end
    case {'output_select'},
        val = oae_getvalue(panel, 'numOutputChannels');
        cmd = get(findobj('tag', 'numOutputChannels'), 'callback');
        if (val ~= length(channels)),
            oae_error('Wrong number of output channels');
        else
        oae_selectpopupvalue('numOutputChannels', num2str(val), 'userdata', channels);
        close(h);
        end
end

if ~isempty(cmd),
    evalin('caller', cmd);
end
end