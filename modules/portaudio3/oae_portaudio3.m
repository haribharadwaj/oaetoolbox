function obj = oae_portaudio3(obj)
% 9/28/2012  changed the way recordings are saved, they are now saved to a
% file using the method obj.saveRecordingToFile - Joshua J. Hajicek
%
% 03/27/14 - Simon Henin
% fix issue with 1-channel playback playing through 2-channels using master
% device mode.
%
% 02/23/2017 - Joshua Hajicek
% Merged recent changes from oae_portaudio.m to oae_portaudio3.m so that
% they work more similar. 
% 3/9/17 Updated to flush internal buffers before getting real data first.
% This prevents hardware based zeros at the front of the first buffer and possible signal shifts during the next two calls. 
y = obj.getCurrentStimulus();


% resize the data, if necessary
if size(y,2) > size(y,1),
    y = y';
end
% get I/O channels
OutputChannels  = obj.OutputChannels;
OutputDevices   = obj.OutputDevices;
InputChannels   = obj.InputChannels;
InputDevices    = obj.InputDevices;

numOutputDevices = length(unique(OutputDevices));
pageSize = obj.bufferSize;
%pageBuffer = pageSize*4;
pageBuffer = pageSize*20;  % setting to 10 to reduce buffer overflow 

% initialize devices, and fill buffers
pasound = []; inputID = [];
% display number of threads for matlab
% N = maxNumCompThreads;
% fprintf(1, 'number of comp threads', N);

try
    if ~obj.audioInit,
        clear PsychPortAudio
        PsychPortAudio('Verbosity', 1);
%         [oldyieldInterval, oldMutexEnable, lockToCore1] = PsychPortAudio('EngineTunables', 0, 0, 0);
        if ~isunix,
            Priority(2);
        end
    end
catch
    oae_error('Portaudio driver not found.');
    return;
end

for i=1:length(numOutputDevices),
    
    DeviceID = OutputDevices(i);
    index = find(OutputDevices == DeviceID);
    Out = OutputChannels(index)';
    
    % check for duplex
    if InputDevices == DeviceID,
        inputID = i;
        chanlist = [];
        for j=1:length(Out),
            chanlist = [chanlist Out(j)];
        end
        chanlist = [chanlist, InputChannels];
        if isempty(obj.audioInit) || ~obj.audioInit,
            
            % check if device is still connected
            DeviceName = obj.recordingDeviceName;
            dev = PsychPortAudio('GetDevices');
            if isempty(find(strcmp({dev.DeviceName}, DeviceName))),
                oae_error('Your configured I/O device is not connected!', 'IO Error');
                return;
            else
                % check if device ID is still the same, otherwise update
                % the object with it's new DeviceID
                idx = find(strcmp({dev.DeviceName}, DeviceName));
                if dev(idx).DeviceIndex ~= DeviceID,
                    DeviceID = dev(idx).DeviceIndex;
                    obj.OutputDevices(i)    = DeviceID;
                    obj.InputDevices(i)     = DeviceID;
                end
            end
        
            % Fix for 1-channel recordings playing back through 2-channels
            % Use master device mode (+8) and open individual channels as
            % slaves. However, need to open master with at least
            % 2-channels (e.g. one will be unused), otherwise it does not work
            % Modified 3/27/2014, Simon Henin
            PsychPortAudio('Close'); % make sure any open devices are closed
            pamaster = PsychPortAudio('Open', DeviceID, 3+8, 2, obj.fs, [max(2,length(Out)), length(InputChannels)], [], 0.005);
            % less aggressive version of PsychPortAudio
            % pamaster = PsychPortAudio('Open', DeviceID, 3+8, 0, obj.fs, [max(2,length(Out)), length(InputChannels)], [], []);
            PsychPortAudio('Start', pamaster, 0, 0, 1);
            if isunix,
                pasound(i) = PsychPortAudio('OpenSlave', pamaster, 3, [length(Out), length(InputChannels)], [OutputChannels(index); repmat(InputChannels,1, length(OutputChannels(index)))]);
            else
                % TODO: check if this works on windows
                pasound(i) = PsychPortAudio('OpenSlave', pamaster, 3, [length(Out), length(InputChannels)], [OutputChannels(index); repmat(InputChannels,1, length(OutputChannels(index)))]);
            end
        end
    else
        if isempty(obj.audioInit) || ~obj.audioInit,
            
            % check if device ID is still the same, otherwise update
            % the object with it's new DeviceID
            DeviceName = obj.recordingDeviceName;
            dev = PsychPortAudio('GetDevices');
            idx = find(strcmp({dev.DeviceName}, DeviceName));
            if isempty(find(strcmp({dev.DeviceName}, DeviceName))),
                oae_error('Your configured I/O device is not connected!', 'IO Error');
                return;
            else,
                % check if device ID is still the same, otherwise update
                % the object with it's new DeviceID
                idx = find(strcmp({dev.DeviceName}, DeviceName));
                if dev(idx).DeviceIndex ~= DeviceID,
                    DeviceID = dev(idx).DeviceIndex;
                    obj.OutputDevices(i)    = DeviceID;
                end
            end
                
            pamaster = PsychPortAudio('Open', DeviceID, 1+8, 1, obj.fs, max(2, length(Out)), [], 0.005);
            PsychPortAudio('Start', pamaster, 0, 0, 1);
            pasound(i) = PsychPortAudio('OpenSlave', pamaster, 1, length(Out), Out);
        end
    end
    
    if obj.audioInit,
        pasound = obj.getExtraProp('pasound');
    end
    
%     if isempty(obj.audioInit) || ~obj.audioInit,
%         fprintf('OTB: priming audio buffer\n');
%         dummystim = eps.*ones(length(Out), 1*obj.fs);
%         PsychPortAudio('FillBuffer', pasound(i), dummystim);
%         PsychPortAudio('GetAudioData', pasound(i), 1, 1, 1);
%         PsychPortAudio('Start', pasound(i),1,0,0);
%         pause(1);
%         [audiodata absrecposition overflow cstarttime] = PsychPortAudio('GetAudioData', pasound(i));
%         pause(1);
%         [audiodata absrecposition overflow cstarttime] = PsychPortAudio('GetAudioData', pasound(i)); 
%         PsychPortAudio('Stop', pasound(i));
%     end
    if isempty(obj.audioInit) || ~obj.audioInit,
        fprintf('OTB: priming audio buffer\n');
        dummystim = eps.*ones(length(Out), 0.1*obj.fs);
        PsychPortAudio('FillBuffer', pasound(i), dummystim);
        PsychPortAudio('GetAudioData', pasound(i), 4, 0.1, 0.1);
        for j=1:3,
            PsychPortAudio('Start', pasound(i),1,0,0);
            pause(0.1); PsychPortAudio('GetAudioData', pasound(i));
            pause(0.1); PsychPortAudio('GetAudioData', pasound(i));
            PsychPortAudio('Stop', pasound(i));
        end
    end
    stimulus = y(:,index);
    PsychPortAudio('FillBuffer', pasound(i), stimulus');
end

% % flush the output buffer
% if ~obj.audioInit,    
%     yy = zeros(100,length(index));
%     PsychPortAudio('FillBuffer', pasound(i), yy');
%     PsychPortAudio('GetAudioData', pasound(inputID), pageBuffer/obj.fs, pageSize/obj.fs, pageSize/obj.fs);
%     PsychPortAudio('Start', pasound(inputID),1,0,1);
%     PsychPortAudio('Stop', pasound(inputID), 1);
% end


input = zeros(1, max(size(y)));
numBuffers = ceil(((obj.fs*obj.T)+obj.onsetDelay + obj.offsetDelay)/pageSize);
playTime = obj.T+obj.onsetDelay/obj.fs+obj.offsetDelay/obj.fs;

if isempty(obj.audioInit) || ~obj.audioInit,
    PsychPortAudio('GetAudioData', pasound(inputID), obj.T*4, playTime, playTime);
    obj = obj.addExtraProp('pasound', pasound);
    obj.audioInit = true;
end

for i=1:length(numOutputDevices),
    status = PsychPortAudio('GetStatus', pasound(i));
    if status.XRuns > obj.underflow,
        fprintf(1, 'Warning: overflow detected on Start.\nShould not affect playback unless multiple occurences are logged within a sequence.\n.....\n');
        obj.underflow = obj.underflow + 1;   % the first overflow/underrun is becasue we let portaudio decide what size to use, becuase it doesn't know what size to return it generates this error the first time. This allows portaudio to optimize itself. 
    end
    PsychPortAudio('Start', pasound(i),1,0,0);
end

pause(playTime);
[input absrecposition overflow cstarttime] = PsychPortAudio('GetAudioData', pasound(inputID), [], playTime, playTime);
obj = obj.updateGUI(input);

for j=1:length(pasound),
    [startTime endPositionSecs xruns estStopTime] = PsychPortAudio('Stop', pasound(i), 1);
    if xruns > obj.underflow,
        fprintf(1, 'Warning: %i buffer over- or underruns detected\n', xruns);
        obj.underflow = obj.underflow+1;
    end
end

% This should be done inside the controller, removed from here for now
% if ~obj.isCustom       % save recording to file if not making a custom recording - otherwise use obj.data
%     obj = obj.saveRecordingToFile(input);
% else
%     obj = obj.saveRecording(input);
% end

% save to object and/or save to file 
obj = obj.saveRecording(input);

if obj.rep == obj.nRepetitions,
    fprintf(1, 'Closing portaudio...\n');
    for i=1:length(pasound),
        PsychPortAudio('Close', pasound(i));
    end
    clear pasound;
    obj.audioInit = false;
    
    if ~isunix,
        Priority(0);
    end

end

end