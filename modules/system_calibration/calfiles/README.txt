stimulus calibration files should have the following format

coupler_myname.mat
e.g. coupler_ER10C1994.mat would be the ER10C calibration for an ER10Cprobe with serialnumber 1994

FPL calibrations should have the following format
Thevenincal_myname.mat

mic calibration files should have the following formats

miccal_myname.mat

files should be a time domain IR with 4096 samples.  

ITE cals won't be stored 

magDiffs contains a variable magDiffs which is the magnitude difference in dB from norm10cm - calculated probe transfer functions using the Er10C circuit and ER7C mic. 
f is the frequency for the difference.  they are 2048 in length
