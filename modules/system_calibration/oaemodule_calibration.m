function [vers block label cmd] = oaemodule_calibration(fig)


vers  = 'calibration 2.0';
label = 'System Calibration';
cmd   = ['OAE = calibration_get(OAE, SEQUENCE);'];
block = 'calibration';   % the name of the UIPANEL/ or as we call blocks

end