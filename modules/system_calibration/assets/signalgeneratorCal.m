function OAE = signalgeneratorCal(OAE, SEQUENCE)

handle = findobj('tag', 'calStatusText');
oae_setvalue(handle, 'Status: Calibrating...');
OAE(SEQUENCE) = iteCalibration2(OAE(SEQUENCE));   % zero do not plot, 1 = plot it cal
oae_setvalue(handle, 'Status: Output is Calibrated');
handle = findobj('tag', 'runCalButton');
oae_setvalue(handle, 'Re-Run Output Calibration');