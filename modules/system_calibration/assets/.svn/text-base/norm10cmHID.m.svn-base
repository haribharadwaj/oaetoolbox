function cresp=norm10cmHID(flist)
% normalization for ER10C connected directly to HearID biased input
%    Created target equalization by dividing norm10cm by abs(equalizeDPPIC), 
%    which removes the missing DP-PIC gain from the original norm10cm.
%    Then adjusted exponents below until log-log plot of norm10cmHID matches the target.
%    Adjusted gcor so norm10cmHID matches norm10cm at 1kHz. 
% pb, 2004/april/14
%
% based on Pats norm10cm.f90 
%    for ER10C + MA PIC for SFOAE - B&K4157 coupler
%    5/16/2002 - readjusted for ER10C+MA PIC in B&K4157 coupler with deep insertion.
% 
% Modifications by Joshua Hajicek: Only added the following comments:
% This function provides a rough estimate of the magnitude correction which
% must be mulitipled by both real and imaginary parts of a DP, TE, SF, or
% MEPA response measured with the ER10C microphone.  
% Example usage in TEOAE: 
%             fqMin = sig.Fmax/N_fqIndices;                       % calculate the min frequency as well as (it's the same) the size of the frequency bin for the FFT
%             fqIndiceVec = 1:1:N_fqIndices;                      % create the frequncy vector indices and frequencies (next line) which is fed into Paul's ER10C mic and 20 dB gain channel complex filters
%             fqVec = fqMin * fqIndiceVec;                        % Calculate the frequency vector used in TE
%             HinvMic = norm10cmHID( fqVec );                     % magnitude equalization appears to be the correct EQ and is normalized at 1khz
%             Hinv = HinvMic;                                     % only apply mic correction but comment out above line;  
%             fbuf =  time2freq( tbuf0 );                         % convert time domain buffer to frequency domain
%             fbuf1 = Hinv' .* fbuf;                              % multiply mic magnitude correctoin by the real and imaginary part of the response to keep the phases the same
%
% (taken from an inverse transfer function measurement of the ER10C). 
% input flist - list of frequencies for the frequency bins


gcor=0.97; % 0.5 in norm10cm
cresp=ones(1,length(flist));
for i=1:length(flist)
	f=flist(i);
	if(f<780.0), cresp(i)=cresp(i)*(f/780.0); end
	if(f<1100.0), cresp(i)=cresp(i)*((f/1100.0)^0.5); end % 0.9 in norm10cm
	if(f<2400.0), cresp(i)=cresp(i)*((2400.0/f)^0.8); end % 1.6 in norm10cm
	if(f>=2400.0), cresp(i)=cresp(i)*((f/2400.0)^3.5); end % 2.0 in norm10cm
	if(f>=3600.0), cresp(i)=cresp(i)*((3600/f)^1.5); end % 1.5 in norm10cm
	if(f>=6000.0), cresp(i)=cresp(i)*((6000.0/f)^2.0); end % 2.0 in norm10cm
	if(f>=7000), cresp(i)=cresp(i)*((7000/f)^3.0); end % 4.0 in norm10cm
	cresp(i)=cresp(i)*gcor;
end

return