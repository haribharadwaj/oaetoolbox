function OAE = oae_measure_output_sens(OAE, SEQUENCE, h)
%
% 
% This function is based on the B&K 4230 calibrator which plays a 93.8 dB SPL
% tone at 1000 Hz.  Connect the probe into the 1/2 adapter on the
% calibrator. Hit the measure input sens. 
% Should update to handle different calibrators but for now it only works
% with the B&K.
% Microphone calibration is not applied here, nor should it be since we normalize to the sensitivity at 1000 kHz and we calculate the sensitivity to a 1kHz tone.

% check that input sentivity has already been performed
input_sens = OAE(SEQUENCE).sensitivity;


% choose your poison (note the poison demonstrates resonable similarity JJH)
if ~isempty(input_sens),
    % we can either calibrate using SLM OR since input_sens has already been calibrated, we can do an sort of "loopback"
    % calibration.
    options.Interpreter = 'none';
    options.Default = 'Sound Level Meter Calibration';
    set(0,'DefaultUicontrolUnits','Pixels');
    selections = {'Self-calibrate (estimate)', 'Sound Level Meter Calibration'};
    btn = questdlg(sprintf('How would you like to calibrate your output?'), 'Next...', selections{1}, selections{2}, options);
    set(0,'DefaultUicontrolUnits','Normalized');
    switch btn
        case selections(1)
            option = 1;
        case selections(2)
            option = 2;
    end
else
    option = 2;
end    

if option == 1,
    % tell the user what to do
    uiwait(msgbox({'1) Remove all input/microphone gain from your system, remember to check both the microphone-preamp and soundcard input for any gain.' '' '2) Set "Total System Gain" parameter to 0.' '' '3) Couple your probe to 2cc ear-simulator/coupler/cut-off syringe.'}, 'Instructions', 'modal'));
elseif option == 2,
    % tell the user what to do
    uiwait(msgbox({'Couple your probe to a sound level meter with Linear- or C-weighting selected' 'and enter the reading (dB SPL) into the ''Output Sensitivty'' box.'}, 'Instructions', 'modal'));
end


% playback a 0.5 amplitude sine wave
fs = OAE(SEQUENCE).getProp('fs');
amp = 1;
T = 4; % seconds
t = 0:1/fs:T;
f = 1000;
x =  oae_ramp( amp.*sin(2*pi*f*t), 0.05, fs, 'both');
xrms = sqrt( mean( x.^2 , 2) );  % vrms

 y = []; v2pa = [];
for i=1:OAE(SEQUENCE).nOutput,
    h = oae_waitbar('string', sprintf('Playback/Recording from ch. %i...', i));
    y = OAE(SEQUENCE).makeUncalibratedRec( x, i , 1 );  % (data, playbackCh, nReps) 
    close(h);
    
    vrms = sqrt( mean( y.^2 , 2) );  % vrms
    vmax = max(abs(y));
    pref = 2e-5;
    
     %   input_sens = vrms of 93.8 dB tone (at least that is what is done in
     %   oae_measure_input_sens)
     %   so output output sens should be a scaler (v2pa) results in measured dB
     % working equations are something like:
     % nominal_volts * v2pa = desired_level (in volts)
     % nominal_volts*1 = measured_level  (in this case, nominal_volts = 0.5, see above)
     % measured_level = vrms./input_sens
     % v2pa = measured_level / nominal_level  ** I think this is the only
     % one that matters
    
    pref = 2e-5;
    dB_rel = 20*log10(amp./pref);
    if option == 1, % self-cal
        v2pa(i) = 10^((dB_rel-20*log10(vrms./input_sens./pref))/20);
    else            % SLM cal
        % ask what SLM reading was
        prompt = {'Enter Sound Level Meter reading (dB SPL):'};
        dlg_title = 'Reading...';
        num_lines = 1;
        answer = inputdlg(prompt,dlg_title,num_lines);
        if isempty(answer),
            return;
        end
        dB = str2double(answer);
        v2pa(i) = (10^((dB_rel-dB)/20));
    end
end

%y = OAE.SEQUENCE.makeCustomRecording( x, playbackCh, nReps);  % only need to pass
%paramaters to a method, the obj is automatically passed. 

N = 1;
while N <= SEQUENCE,
    OAE(N).v2pa = v2pa;
    N = N+1;
end
handle = findobj('tag', 'outputSens');
oae_setvalue(handle, sprintf('%2.4f %2.4f', v2pa));
uiwait(msgbox({'After both "Input Sensitivity" and the "Output Sensitivity" have been calibrated, ' '' '1) Adjust your hardware gain to the desired level and,' '' '2) Set the "Total System Gain" to match your total hardware gain' '' 'Please disregard this message if you have not measured the "Input Sensitivity" yet'}, 'Instructions', 'modal'));




