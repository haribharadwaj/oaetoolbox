function OAE = calibration_set(OAE, SEQUENCE )

% each time a the the filter file is changed we update hte filter kernal.
% if the probe has no associated filter kernal decide what to do
% if ER10C build the kernal based off of norm10cmHID
% call obj = setMicCalFilter(kernal) to set the kernal
% finish

% some notes: Your filter must be a an "Even" symmetric filter with an
% oddnumber of points where the center sample is the center of the kernal.
% Even means h(n) = h(-n) assuming the central sample is sample 0.  However
% since matlab indexes from 1:end  it will be h(centerIdx-i) =
% h(centerIdx+i).  If your filter kernal is set up properly we can account
% for the group delay of the filter, if not group delay cannot be accounted
% for which will throw off the LSF.
% Your .mat file which contains your filter kernal (time domain) can have any name but
% the variable it is stored in must be called micFilterKernal.

handle = findobj( 'tag', 'calibration.parameters');
%micSens = oae_getvalue(handle, 'micSens');
probeSN = oae_getvalue(handle, 'probeSN', 'string');
%couplerCalFile = oae_getvalue(handle, 'couplerCalFile', 'string');
bufferSize = 4096;  % mic buffer size, not OAE.bufferSize
selectedModel = oae_getvalue(handle, 'probeModel', 'string');
currentModel = OAE(SEQUENCE).probeModel;
% quick check to see if probeMdel has been changed
% if ~strcmp(currentModel, selectedModel),
%     set(findobj('tag', 'micCalFile'), 'string', '');
% end

%micCalFile = oae_getvalue(handle, 'micCalFile', 'string') ;
stimCalType = oae_getvalue(handle, 'stimCalType', 'string');
systemSens = oae_getvalue(handle, 'systemSens');
doDisplay = oae_getvalue(handle, 'displayITEResult');

% set all sequences
N = 1;
while (N <= SEQUENCE),
    
    %OAE(N)= OAE(N).setProp( 'micSens', micSens);
    OAE(N)= OAE(N).setProp( 'stimCalType',  stimCalType);
    OAE(N)= OAE(N).setProp( 'probeModel', selectedModel );
    OAE(N)= OAE(N).setProp( 'probeSN',  probeSN );
    OAE(N)= OAE(N).setProp( 'doCalibrationPlot', doDisplay );
    %OAE(N)= OAE(N).setProp( 'couplerCalFile',  couplerCalFile);
    %OAE(N)= OAE(N).setProp( 'micCalFile', micCalFile );
    OAE(N)= OAE(N).setProp( 'eartipType', oae_getvalue(handle, 'eartipType', 'string') );
    if  ~isempty(systemSens)  % We will not set sens if it is empty
        OAE(N)= OAE(N).setProp( 'sensitivity', systemSens );
    end
    
    % specify string for popup menus -need to use OAE(N)= because extraprops private
    % OAE(N)= OAE(N).addProp( 'arFrameSize', oae_getvalue(handle, 'arFrameSize') );  % by default converts to an int
    % OAE(N)= OAE(N).addProp( 'arOverlap', oae_getvalue(handle, 'arOverlap') );
    
    switch oae_getvalue(handle, 'stimCalType', 'string')
        case 'None'
            OAE(N).calibrationCallback = 'coupler_calibration_callback';
            OAE(N).stimCalibrationFilter = [];
        case 'In-the-Ear'
            OAE(N).calibrationCallback = 'iteCalibration2';
    end
    
    % reset the micCalibrationFilter
    OAE(N).micCalibrationFilter = [];
    if strcmp(selectedModel, 'ER10C')
        
        f = linspace(0,OAE(N).fs/2,OAE(N).bufferSize/2+1);
        I = find(f>=1000);
        n = I(1);
        % normalize norm10cmHID ER10C mic response
        if 1   % us for custom transfer functions collected by Joshua Hajicek, 0 for norm10cmHID
            if isdeployed
                er10cMicCalfile = [ctfroot filesep 'modules' filesep 'calibration' filesep 'calfiles' filesep 'ER10CFilterImpulseResponses.mat'];
            else
                er10cMicCalfile = [pwd filesep 'modules' filesep 'calibration' filesep 'calfiles' filesep 'ER10CFilterImpulseResponses.mat'];
            end
            load(er10cMicCalfile);
            H = abs(time2freq(correction(:,6)));  % magnituted of a new ER10C probe mic  (my probe SN1994 is correction(:,7);) 5 other probes are (:,1-5)
            h = freq2time(H);
            
        else
            H = norm10cmHID(f(2:end));
            
        end
        H(1) = 0;                                               % set DC from NaN to zero
        norm1k = H(n);
        H = H./norm1k;
        h = freq2time(H/2914.104);                              % the 2914 (may be FFT size dependent)was found by taking the same recording, 'y' and applying the normalized Norm10cmHID output directly by frequency domain multiplication (Yhmic) and by doing it using matlabs fftfilt (output called yy (time domain)) and dividing max(abs(Yhmic))/max(abs(time2freq(yy)) to get the correction ratio
        h = shift(h, bufferSize/2);                             % max(abs(YY))/max(abs(Y(:,1)))
        [A, idx] = max(h);                                      % I is the center of the kernal
        i = (bufferSize/2)-3;
        tKernal = h( idx-i : idx+i );                           % now tKernal is  an even and symmetric filter kernal
        OAE(N).micCalibrationFilter = tKernal;
        OAE(N).micCalibrationFilterLength = length(tKernal);
        
        % for RT filter
        h2 = freq2time(H);                              % the 2914 was found by taking the same recording, 'y' and applying the normalized Norm10cmHID output directly by frequency domain multiplication (Yhmic) and by doing it using matlabs fftfilt (output called yy (time domain)) and dividing max(abs(Yhmic))/max(abs(time2freq(yy)) to get the correction ratio
        h2 = shift(h2, bufferSize/2);                        %  max(abs(YY))/max(abs(Y(:,1)))
        [A2, idx] = max(h2);                                                        % I is the center of the kernal
        i = (bufferSize/2)-3;
        rtTKernal = h2( idx-i : idx+i );   % now rtTKernal is  an even and symmetric filter kernal
%         figure(2000); plot(rtTKernal);
%         hold on
        OAE(N).rtMicCalibrationFilter = rtTKernal;
        OAE(N).rtFrqMicCalibrationFilterDB = 20*log10(abs(H));  % magnitude dB of ER10C filter normalized at 1kHz
        
    end




%     if ~isempty(micCalFile)
%         load(micCalfile);
%         OAE(N).micCalibrationFilter = micFilterKernal;   % set the objects filter kernal
%     end
N = N +1;
end
end

