function OAE = splcalibrator(OAE, SEQUENCE);
% Based on code provided by Shawn S. Goodman
obj = OAE(SEQUENCE);
pref = obj.pref;
fs = obj.fs;

% make stimulus
len = .05;
N = round(len* fs);
x = zeros(N,1);

f = [50,20000]; % frequency minimum and maximum (Hz)
stepSize = (f(2)-f(1))/(N-1); % frequency step size (Hz)
F = (f(1):stepSize:f(end))';
rampN = round(N * .05);
phi = 0;
for jj = 1:N
    p(jj,1) = cos(phi);
    phi = phi + ((2*pi) / (1/F(jj) * fs));
end
rampOn = hann(rampN*2);
rampOn = rampOn(1:rampN);
rampOff = hann(rampN*2);
rampOff = rampOff(1:rampN);
p(1:rampN) = p(1:rampN) .* rampOn;
p = flipud(p);
p(1:rampN) = p(1:rampN) .* rampOff;
p = flipud(p);
% increase high-frequency output above 10 kHz
[dummy,indx1] = min(abs(F-10000));
h = hann((N-indx1)*2);
h = h(1:N-indx1);
dBincrease = 20;
multiplier = 10^(dBincrease/20);
h = h * multiplier;
amplitude = ones(N,1);
amplitude(indx1+1:end) = amplitude(indx1+1:end) + h;
p = p .* amplitude;
p = p / (max(abs(p)));
segmt = p * .95;
padLen = 0.001;
padN = round(padLen * samplingRate);
pad = zeros(padN,1);
Input = [pad;segmt;pad];



%%%%%%%%

Input1 = Input ./ max(abs(Input));
Input2 = Input1;
Input2 = Input2 ./ max(abs(Input2));
Input1 = [Input1,zeros(size(Input1))];  % alternate playback on channels
Input2 = [zeros(size(Input2)),Input2];
%%%
temp.stim1 = sp.stim1;
temp.stim2 = sp.stim2;
temp.stim3 = sp.stim3;
temp.stim4 = sp.stim4;
temp.fmin = sp.fmin;
temp.fmax = sp.fmax;
temp.MC_L = sp.MC_L;
temp.MC_R = sp.MC_R;
temp.nReps = sp.nReps;
temp.combiningRule = sp.combiningRule;
temp.recordingID = sp.recordingID;
temp.calType = sp.calType;
sp.calType = 'none';
sp.stim2 = [];
sp.stim3 = [];
sp.stim4 = [];
sp.fmin = 100;
sp.fmax = 18000;
sp.MC_L = 'off';
sp.MC_R = 'off';
sp.nReps = 32;
sp.combiningRule = [1];
sp.recordingID = 'spl1';
disp('Performing in-situ calibration for Ch1. Please wait...')
sp.stim1 = Input1;
sp.makeRecording(sp)
if sp.recordingKilled
    return
end
load(sp.mostRecentSave)
response1 = rp.mu(:,1);
disp('Performing in-situ calibration for Ch2. Please wait...')
sp.stim1 = Input2;
sp.recordingID = 'spl2';
sp.makeRecording(sp)
if sp.recordingKilled
    return
end
load(sp.mostRecentSave)
response2 = rp.mu(:,1);
sp.stim1 = temp.stim1;
sp.stim2 = temp.stim2;
sp.stim3 = temp.stim3;
sp.stim4 = temp.stim4;
sp.fmin = temp.fmin;
sp.fmax = temp.fmax;
sp.MC_L = temp.MC_L;
sp.MC_R = temp.MC_R;
sp.nReps = temp.nReps;
sp.combiningRule = temp.combiningRule;
sp.recordingID = temp.recordingID;
sp.calType = temp.calType;
if strcmp(sp.calType,'fpl')
    isc1 = calculateFPL(tp1,response1);
    isc2 = calculateFPL(tp2,response2);
    plotCorrectionsFPL(isc1,isc2);
else
    isc1 = calculateSPL(sp,Input1(:,1),response1);
    isc2 = calculateSPL(sp,Input2(:,2),response2);
    plotCorrectionsSPL(isc1,isc2);
end
sp.isc_ch1 = isc1;
sp.isc_ch2 = isc2;

