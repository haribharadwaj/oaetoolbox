function obj = teoae_2e_callback(obj, oae)
% teoae_2e_callback
%   Runs double-evoked teoae analysis
%   Inputs:
%       obj - Analysis object
%       oae - OAE Data controller
% MODIFICATIONS
%   6-11-2017 - Bug fix, there was an extra multiple of 2 of the fft. Now
%   removed. JJH.
%general parameters
fs = oae.fs;
T = oae.T;

%analysis parameters
params.nskip = 0; %obj.nskip;
sensitivity = obj.sensitivity;
nfft = obj.nfilt;
nstep = obj.nstep;
gain = obj.gain;
pref = 2e-5;
navg = obj.navg;


% collapase data into 1-buffer
n = size( oae.getData( 1, 1, 1 ), 2 ); 
tmp = zeros( 1, n ); 
tmp = double(tmp);      % averaging buffer
nf = tmp;               % noise floor


condition = 1; invt = -1;
for r = 1:oae.nRepetitions
    p1 = detrend(oae.getData( r, 1, condition ));
    p2 = detrend(oae.getData( r, 2, condition ));
    p3 = detrend(oae.getData( r, 3, condition ));
    
    tmp = tmp+(p1+p2-p3);
    ar(r,:) = (p1+p2)-p3;
    invt = invt*-1;
    nf = nf+(p1+p2-p3)*invt;
end
avg = tmp/oae.nRepetitions;
nf = nf/oae.nRepetitions;



% Not necessary to save this back to controller, as it will to be
% performed each time in this function anyways
% oae = oae.saveAverage(avg);

% Save avgerage to analysis object, and set nBuffers to 1 since
% collapsed
obj.nBuffer = 1;
obj = obj.setProp('nBuffer', 1);
obj.avg(:, obj.nBuffer, obj.nCondition) = avg;

paradigmType = oae.paradigmType;
paradigmLabel = oae.paradigmLabel;

%% run it!
disp('analyzing...');
avg = obj.getCurrentAverage();


% bandpass filter
fc_low = 500; fc_high = 6000;
if ~isempty(obj.fc_l),
    fc_low = obj.fc_l;
end
if ~isempty(obj.fc_h),
    fc_high = obj.fc_h;
end
[b,a]=butter(2,[fc_low fc_high]./(oae.fs/2));        % Bandpass digital filter design
avg = filter(b,a,avg);
nf = filter(b,a,nf);

% calculate fft bin width
bin_width = fs/nfft; Y = []; Ynf = [];
   
Yf = fft(avg, nfft);
Yf = 2*Yf./nfft; % preserve signal scaling
Y = [Y; Yf(1:floor(nfft/2))];

Yfnf = fft(nf, nfft);
Yfnf = 2*Yfnf./nfft; % preserve signal scaling
Ynf = [Ynf; Yfnf(1:floor(nfft/2))];

% frequency vector
X = (0:nfft-1)*(fs/nfft);
X = X(1:floor(nfft/2))';


result.f = X;
result.mag = abs(Y);
result.db = 20*log10(abs(Y));
result.phase = angle(Y);

result.rmserror = abs(Ynf)';    % TODO: add error measure

% for now, no temporal averaging, so no std
result.pa_std = [];
result.db_std = [];

obj.fs = fs;
obj.T  = T;
obj.labels = {'TEOAE'};
obj.labelsOn = [1];

obj = obj.saveAnalysis([result(:).f], [result(:).mag], [result(:).pa_std], [result(:).db], [result(:).db_std], [result(:).phase], [result(:).rmserror]);

end