function OAE = teoae_2e_get(OAE, SEQUENCE)

%% callback function for ui elements (e.g. to update the controls & update the sequence OBJECT
cmd = ['OAE = teoae_2e_set(OAE, SEQUENCE);'];

paradigm = OAE(SEQUENCE).paradigmLabel;
paradigmType = OAE(SEQUENCE).paradigmType;


switch lower(paradigm)
    case {'teoae_2e'}
        
        nfilt = 2048;
        fc_l = 500;
        fc_h = 6000;
        
        uigeom = {1 [1 1] [1 1] [1 1] 1};
        uilist = { { 'style' 'text'      'string'    'TEOAE Analysis Setup'} ...
            {'style'  'text'      'string'    'filter length (nfilt)'} ...
            {'style'  'edit'      'string'    num2str(nfilt), 'tag', 'nfilt', 'callback', cmd} ...
            {'style'  'text'      'string'    'fc_low'} ...
            {'style'  'edit'      'string'    num2str(fc_l), 'tag', 'fc_l', 'callback', cmd} ...
            {'style'  'text'      'string'    'fc_high'} ...
            {'style'  'edit'      'string'    num2str(fc_h), 'tag', 'fc_h', 'callback', cmd} ...
            {'style'  'edit'      'string'    'Discrete Tone', 'value', 1, 'tag', 'analysisType', 'visible', 'off'} ...
            };
        
        
    otherwise
        oae_error('This analyis module is only valid for double-evoked TEOAE');
        return;
end

oae_addparameters('analysis.parameters', uilist, uigeom, 'topborder', 0.01, 'fontsize', 10);

%enable the all buttons in the panel (e.g. analyze)
oae_enable_buttons('analysis');

eval(cmd);
