function [vers block label cmd] = oaemodule_teoae_2e_analysis(fig)
%
%
% module function
% should return 4 variables:
%
% [vers] -  version number
% [tag] - the name system block (e.g. daq, paradigm, analysis, results) that
% this module should be attached to
% [cmd] - initial callback routine
% [label] - The label displayed for this module

vers    = 'TEOAE Analysis 1.0';
label   = 'TEOAE (2E) FFT';
cmd     =  [  'OAE = teoae_2e_get(OAE, SEQUENCE);'];
block     = 'analysis';


end