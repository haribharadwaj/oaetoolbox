function [OAE] = teoae_2e_set(OAE, SEQUENCE)
%
%

handle = findobj('tag', 'analysis.parameters');

type  = oae_getvalue(handle, 'analysisType', 'string');
nfilt = oae_getvalue(handle, 'nfilt');
fc_l  = oae_getvalue(handle, 'fc_l');
fc_h  = oae_getvalue(handle, 'fc_h');

OAE(SEQUENCE).analysisObj.Label            = sprintf('teoae');
OAE(SEQUENCE).analysisObj.analysisCallback = 'teoae_2e_callback';
OAE(SEQUENCE).analysisObj.nfilt            = nfilt;
OAE(SEQUENCE).analysisObj.fc_l             = fc_l;
OAE(SEQUENCE).analysisObj.fc_h             = fc_h;
OAE(SEQUENCE).analysisObj.paradigmType     = OAE(SEQUENCE).paradigmType;

end

