function OAE = fft_get(OAE, SEQUENCE)

%% callback function for ui elements (e.g. to update the controls & update the sequence OBJECT
cmd = ['OAE = fft_set(OAE, SEQUENCE);'];

paradigm = OAE(SEQUENCE).paradigmLabel;
paradigmType = OAE(SEQUENCE).paradigmType;


switch lower(paradigmType) 
    case {'tone', 'soae'}
        
        nfilt = 22050;
        nstep = 0;
        tavg = 0;
        
        uigeom = {1 [1 1] [1 1] [1 1] 2 2};
        uilist = { { 'style' 'text'      'string'    'FFT SETUP'} ...
            {'style'  'text'      'string'    'filter length (nfilt)'} ...
            {'style'  'edit'      'string'    num2str(nfilt), 'tag', 'nfilt', 'callback', cmd} ...
            {'style'  'text'      'string'    'step size (0=whole signal)'} ...
            {'style'  'edit'      'string'    num2str(nstep), 'tag', 'nstep', 'callback', cmd} ...
            {'style'  'text'      'string'    'Temporal averaging (s)'} ...
            {'style'  'edit'      'string'    num2str(tavg), 'tag', 'tavg', 'callback', cmd} ...
            {'style'  'checkbox'  'string'    'Spectral Averaging (overrides temporal avg.)', 'value', 0, 'tag', 'fftavg', 'callback', cmd} ...
            {'style'  'edit'      'string'    'Discrete Tone', 'value', 1, 'tag', 'analysisType', 'visible', 'off'} ...
            };    

        
    otherwise
        oae_error('FFT analysis is only valid for discrete tones');
        return;
end

oae_addparameters('analysis.parameters', uilist, uigeom, 'topborder', 0.01, 'fontsize', 10);

%enable the all buttons in the panel (e.g. analyze)
oae_enable_buttons('analysis');

eval(cmd);
