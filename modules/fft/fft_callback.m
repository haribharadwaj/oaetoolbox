function obj = fft_callback(obj, oae)
% FFT_CALLBACK
%   Runs fft analysis

%general parameters
fs = oae.fs;
T = oae.T;

%analysis parameters
params.nskip = 0; %obj.nskip;
sensitivity = obj.sensitivity;
nfft = obj.nfilt;
nstep = obj.nstep;
gain = obj.gain;
navg = obj.navg;
fftavg = obj.fftavg;


fstart = oae.fstart(:,oae.Buffer, oae.Condition);
if ~isempty(oae.fend),
    fend = oae.fend(:,oae.Buffer, oae.Condition);
else
    fend = [];
end
paradigmType = oae.paradigmType;
paradigmLabel = oae.paradigmLabel;

switch lower(paradigmType),
    case {'tone'}
        switch lower(paradigmLabel)
            case {'discrete dpoae'}
                % add dpoae frequency to the list
                fstart = [fstart; 2*fstart(1)-fstart(2)];
                labels = {'f1' 'f2' 'dpoae'};
                labelsOn = logical([0 0 1]);
            case {'sfoae'}
                % TODO
        end
    case {'soae'}
        labels = {'soae'};
        labelsOn = logical([1]);
    otherwise
        oae_error(sprintf('FFT Analysis cannot be performed on paradigm with type: %s (%s)', lower(paradigmType), lower(paradigmLabel)));
        return;
end


%% run it!
avg = obj.getCurrentAverage();



% temporal averaging
if fftavg,
    navg = nfft;
    % if spectral averaging, split into segments of length navg (time x
    % segments). Don't get mean
    avg = avg(1: floor(length(avg)./navg)*navg);
    avg = reshape(avg, navg, length(avg)/navg);
elseif navg,
    % TODO: should we use partial buffers? If so we need to zero pad any
    % non-even avg
    avg = avg(1: floor(length(avg)./navg)*navg);
    avg = mean(reshape(avg, navg, length(avg)/navg), 2);
end



% calculate fft bin width
bin_width = fs/nfft; Y = [];
if nstep == 0, nstep = length(avg); end
for i=0:nstep:nstep*floor(length(avg)/nstep)-nstep,
    avg = bsxfun(@minus, avg, mean(avg, 1)); % remove DC
    Yf = fft(avg(i+1:i+nstep, :), nfft);
    Yf = 2*Yf./nfft; %preserve signal scaling and mulitply by 2 since fft is two-sided and disperses energy upon both sides, but we only want to show the positive freqs
    Y = [Y Yf(1:nfft/2+1, :)]; 
end
% frequency vector
X = (0:nfft/2)*(fs/nfft);


if strcmp(lower(paradigmType), 'soae')
    X = X';
    result.f = X;
    result.mag = mean(abs(Y), 2);
    result.db = mean(20*log10(abs(Y)),2);
    result.phase = mean(angle(Y),2);
    result.pa_std = nan(size(result.mag));
    result.db_std = nan(size(result.mag));
    result.rmserror = nan(size(result.mag));
else
    for i=1:length(fstart)
        if mod(fstart(i), bin_width), % print warning about fft bin center
            oae_printwarning('Frequency is not centered in FFT bin');
        end
        tmp = abs(X-fstart(i));
        [~,idx] = min(tmp);    %index of closest value
        result(i).f = fstart(i);
        result(i).mag = mean(abs(Y(idx,:)));
        result(i).db = mean(20*log10(abs(Y(idx,:))));
        result(i).phase = mean(angle(Y(idx,:)));
        
        result(i).rmserror = nan(size(result(i).mag)); % TODO: put some useful measure of noise floor here. For now just put NaNs so plotting doesn't crash.
        
        result(i).rmserror = [];    % TODO: add error measure
        if size(Y,2) == 1,
            % for now, no temporal averaging, so no std
            result(i).pa_std = [];
            result(i).db_std = [];
        else
            result(i).pa_std = std(abs(Y(idx,:)));
            result(i).db_std = std(20*log10(abs(Y(idx,:))));
        end
        
    end
end


obj.fs = fs;
obj.T  = T;
obj.labels = labels;
obj.labelsOn = labelsOn;

obj = obj.saveAnalysis([result(:).f], [result(:).mag], [result(:).pa_std], [result(:).db], [result(:).db_std], [result(:).phase].*(180/pi), [result(:).rmserror]);

end