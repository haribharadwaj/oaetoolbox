function [vers block label cmd] = oaemodule_nseoae(fig)
%
%
% module function
% should return 4 variables:
%
% [vers] -  version number
% [tag] - the name system block (e.g. daq, paradigm, analysis, results) that
% this module should be attached to
% [cmd] - initial callback routine
% [label] - The label displayed for this module

vers    = 'fft 1.0';
label   = 'FFT';
cmd     =  [  'OAE = fft_get(OAE, SEQUENCE);'];
block     = 'analysis';


end