function [OAE] = fft_set(OAE, SEQUENCE)
%
%

handle = findobj('tag', 'analysis.parameters');

nfilt = oae_getvalue(handle, 'nfilt');
nstep = oae_getvalue(handle, 'nstep');
tavg = oae_getvalue(handle, 'tavg');
fftavg = oae_getvalue(handle, 'fftavg');
navg = tavg*OAE(SEQUENCE).fs;               % set nstep for temporal averaging

OAE(SEQUENCE).analysisObj.Label             = sprintf('fft');
OAE(SEQUENCE).analysisObj.analysisCallback  = 'fft_callback';
OAE(SEQUENCE).analysisObj.nfilt             = nfilt;
OAE(SEQUENCE).analysisObj.nstep             = nstep;
OAE(SEQUENCE).analysisObj.navg              = navg;
OAE(SEQUENCE).analysisObj.fftavg            = fftavg;
OAE(SEQUENCE).analysisObj.paradigmType      = OAE(SEQUENCE).paradigmType;

end

