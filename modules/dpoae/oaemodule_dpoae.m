% dpoae acquisition plugin
%
%   add DPOAE paradigm sequences
% 
% version 1.0
% OAEToolbox, 2016
% Copyright (C) 2018  Simon Henin and Joshua J. Hajicek
function [vers block label cmd] = oaemodule_dpoae(fig)

vers='dpoae 1.0';
label = 'DPOAE';
cmd=  [  'OAE = dpoae_get(OAE, SEQUENCE);'];
block = 'paradigm';


end